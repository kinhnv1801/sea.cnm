﻿using System.Web;
using System.Web.Mvc;
using SEA.CNM.Api.Attributes;

namespace SEA.CNM.Api
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
