﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEA.CNM.Api.Extension
{
    public static class BoolExtension
    {
        public static string ToGender(this bool gender)
        {
            return gender ? "Nam" : "Nữ";
        }
    }
}