﻿using System.Web.Script.Serialization;

namespace SEA.CNM.Api.Extension
{
    public static class ObjectExtenstion
    {
        public static string ToJson<T>(this T obj)
        {
            var jsonSerialiser = new JavaScriptSerializer();
            return jsonSerialiser.Serialize(obj);
        }
    }
}
