﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace SEA.CNM.Api.Extension
{
    public static class StringExtensions
    {
        public static List<int> ToListInt(this string str, char symbol)
        {
            if (String.IsNullOrEmpty(str))
            {
                return new List<int>();
            }
            return str.Split(symbol).Where(x => !String.IsNullOrEmpty(x)).Select(Int32.Parse).ToList();
        }

        public static List<string> ToListString(this string str, char symbol)
        {
            if (String.IsNullOrEmpty(str))
            {
                return new List<string>();
            }
            return str.Split(symbol).ToList();
        }

        public static List<float> ToListFloat(this string str, char symbol)
        {
            if (String.IsNullOrEmpty(str))
            {
                return new List<float>();
            }
            return str.Split(symbol).Where(x => !String.IsNullOrEmpty(x)).Select(x=>float.Parse(x, CultureInfo.InvariantCulture.NumberFormat)).ToList();
        }

        public static T ToObject<T>(this string json) where T : new()
        {
            if (string.IsNullOrEmpty(json)) return new T();
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static Dictionary<string, object> ToDictionary(this string json)
        {
            if (string.IsNullOrEmpty(json)) return new Dictionary<string, object>();
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }
    }
}
