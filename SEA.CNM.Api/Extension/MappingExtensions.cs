﻿using System;

namespace SEA.CNM.Api.Extension
{
    public static class MappingExtensions
    {
        public static TTo MapTo<TTo>(this object obj)
        {
            var formType = obj.GetType();
            AutoMapper.Mapper.Initialize(cfg => cfg.CreateMap(formType, typeof(TTo)));
            return AutoMapper.Mapper.Map<TTo>(obj);
        }
    }
}
