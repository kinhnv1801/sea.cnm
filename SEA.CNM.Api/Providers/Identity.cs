﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;

namespace SEA.CNM.Api.Providers
{
    public static class Identity
    {

        public static ClaimsIdentity CreateCookie(string name, string id)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, name),
                    //user.Name from my database
                    new Claim(ClaimTypes.NameIdentifier, id),
                    //user.Id from my database
                    new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                        "MyApplication"),
                    //new Claim("FirstName", user.FirstName) //user.FirstName from my database
                };

            return new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Name, ClaimTypes.Role);
        }


        public static ClaimsIdentity CreateOAuthIdentity(string name, string id)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, name),
                    //user.Name from my database
                    new Claim(ClaimTypes.NameIdentifier, id),
                    //user.Id from my database
                    new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                        "MyApplication"),
                    //new Claim("FirstName", user.FirstName) //user.FirstName from my database
                };

            return new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType, ClaimTypes.Name, ClaimTypes.Role);
        }

        public static ClaimsIdentity CreateCookieAuthenticationIdentity(string name, string id)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, name),
                    //user.Name from my database
                    new Claim(ClaimTypes.NameIdentifier, id),
                    //user.Id from my database
                    new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                        "MyApplication"),
                    //new Claim("FirstName", user.FirstName) //user.FirstName from my database
                };

            return new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType, ClaimTypes.Name, ClaimTypes.Role);
        }
    }
}
