﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.System.Artist;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Api.Controllers.Admin.System
{
    [ApiAuthorize]
    public class AdminArtistsController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminArtistsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/artist-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Artists
                    .Where(x => !x.IsDeleted)
                    .Select(x => new
                    {
                        x.ArtistId,
                        x.FullName
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/artists")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]ArtistFilter filter)
        {
            try
            {
                var res = _context.Artists
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.FullName.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.ArtistId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/artists")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = _context.Artists.Where(x => x.ArtistId == id).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/artists")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]ArtistBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var artist = model.MapTo<Artist>();
                artist.IsDeleted = false;
                _context.Artists.Add(artist);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/artists")]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]ArtistBase model)
        {
            try
            {
                var oldArtist = _context.Artists.SingleOrDefault(x => x.ArtistId == id);
                if (oldArtist == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var artist = model.MapTo<Artist>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                artist.IsDeleted = oldArtist.IsDeleted;
                artist.ArtistId = oldArtist.ArtistId;
                _context.Artists.AddOrUpdate(artist);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
        
        [HttpGet]
        [Route("api/admin/artists/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from filmArtist in _context.FilmArtists
                        .Where(x => x.ArtistId == id)
                    join film in _context.Films
                        .Where(x => !x.IsDeleted)
                        on filmArtist.FilmId equals film.FilmId
                    select film).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/artists/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var artist = _context.Artists.SingleOrDefault(x => x.ArtistId == id);
                if (artist == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                artist.IsDeleted = !artist.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/artists")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var artist = _context.Artists.SingleOrDefault(x => x.ArtistId == id);
                if (artist == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Artists.Remove(artist);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
