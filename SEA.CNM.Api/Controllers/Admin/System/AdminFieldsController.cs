﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.System.Field;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Api.Controllers.Admin.System
{
    [ApiAuthorize]
    public class AdminFieldsController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminFieldsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/field-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Fields
                    .Where(x => !x.IsDeleted)
                    .Select(x => new
                    {
                        x.FieldId,
                        x.Name
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/fields")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]FieldFilter filter)
        {
            try
            {
                var res = _context.Fields
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.Name.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));
                
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.FieldId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/fields")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = _context.Fields.Where(x => x.FieldId == id).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/fields")]
        public HttpResponseMessage Post([FromBody]FieldBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var field = model.MapTo<Field>();
                field.IsDeleted = false;
                _context.Fields.Add(field);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/fields")]
        public HttpResponseMessage Put(int id, [FromBody]FieldBase model)
        {
            try
            {
                var oldField = _context.Fields.SingleOrDefault(x => x.FieldId == id);
                if (oldField == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var field = model.MapTo<Field>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                field.IsDeleted = oldField.IsDeleted;
                field.FieldId = oldField.FieldId;
                _context.Fields.AddOrUpdate(field);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/fields/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from filmField in _context.FilmFields
                        .Where(x => x.FieldId == id)
                    join film in _context.Films
                        .Where(x => !x.IsDeleted)
                        on filmField.FilmId equals film.FilmId
                    select film).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/fields/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var field = _context.Fields.SingleOrDefault(x => x.FieldId == id);
                if (field == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                field.IsDeleted = !field.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/fields")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var field = _context.Fields.SingleOrDefault(x => x.FieldId == id);
                if (field == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Fields.Remove(field);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
