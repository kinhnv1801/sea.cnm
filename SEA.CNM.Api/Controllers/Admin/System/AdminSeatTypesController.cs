﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.System.SeatType;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Api.Controllers.Admin.System
{
    [ApiAuthorize]
    public class AdminSeatTypesController : ApiController
    {
        
        private readonly EntitiesContext _context;

        public AdminSeatTypesController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/seat-type-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.SeatTypes
                    .Where(x => !x.IsDeleted)
                    .Select(x => new
                {
                    x.SeatTypeId,
                    x.Name
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/seat-types")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]SeatTypeFilter filter)
        {
            try
            {
                var res = _context.SeatTypes
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.Name.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));
                
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.SeatTypeId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/seat-types")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = _context.SeatTypes.Where(x => x.SeatTypeId == id).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/seat-types")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]SeatTypeBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var seatType = model.MapTo<SeatType>();
                seatType.IsDeleted = false;
                _context.SeatTypes.Add(seatType);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/seat-types")]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]SeatTypeBase model)
        {
            try
            {
                var oldSeatType = _context.SeatTypes.SingleOrDefault(x => x.SeatTypeId == id);
                if (oldSeatType == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var seatType = model.MapTo<SeatType>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                seatType.IsDeleted = oldSeatType.IsDeleted;
                seatType.SeatTypeId = oldSeatType.SeatTypeId;
                _context.SeatTypes.AddOrUpdate(seatType);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/seat-types/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from roomSeat in _context.RoomSeats
                        .Where(x => x.RoomId == id)
                    join room in _context.Rooms
                        .Where(x=>!x.IsDeleted)
                        on roomSeat.RoomId equals room.RoomId
                    select room).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/seat-types/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var seatType = _context.SeatTypes.SingleOrDefault(x => x.SeatTypeId == id);
                if (seatType == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                seatType.IsDeleted = !seatType.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/seat-types")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var seatType = _context.SeatTypes.SingleOrDefault(x => x.SeatTypeId == id);
                if (seatType == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.SeatTypes.Remove(seatType);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
