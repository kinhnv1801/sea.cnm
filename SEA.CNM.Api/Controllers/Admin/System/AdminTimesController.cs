﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.System.Time;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Api.Controllers.Admin.System
{
    [ApiAuthorize]
    public class AdminTimesController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminTimesController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/time-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Times
                    .Where(x => !x.IsDeleted)
                    .ToList()
                    .Select(x => new
                    {
                        x.TimeId,
                        Title =
                            x.BeginAt.AddHours(7).ToString("hh:mm:ss") + " - " +
                            x.EndAt.AddHours(7).ToString("hh:mm:ss")
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/times")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]TimeFilter filter)
        {
            try
            {
                var res = _context.Times
                    .Where(x => x.IsDeleted == filter.IsDeleted );
                
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.TimeId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/times")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = _context.Times.Where(x => x.TimeId == id).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/times")]
        public HttpResponseMessage Post([FromBody]TimeBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var time = model.MapTo<Time>();
                time.IsDeleted = false;
                _context.Times.Add(time);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/times")]
        public HttpResponseMessage Put(int id, [FromBody]TimeBase model)
        {
            try
            {
                var oldField = _context.Times.SingleOrDefault(x => x.TimeId == id);
                if (oldField == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var time = model.MapTo<Time>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                time.IsDeleted = oldField.IsDeleted;
                time.TimeId = oldField.TimeId;
                _context.Times.AddOrUpdate(time);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
        
        [HttpGet]
        [Route("api/admin/times/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from schedules in _context.Schedules
                        .Where(x => x.TimeId == id && !x.IsDeleted)
                    select schedules).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/times/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var time = _context.Times.SingleOrDefault(x => x.TimeId == id);
                if (time == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                time.IsDeleted = !time.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/times")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var time = _context.Times.SingleOrDefault(x => x.TimeId == id);
                if (time == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Times.Remove(time);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
