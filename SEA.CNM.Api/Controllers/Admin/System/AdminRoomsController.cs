﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.System.Room;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Api.Controllers.Admin.System
{
    [ApiAuthorize]
    public class AdminRoomsController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminRoomsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/room-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Rooms
                    .Where(x => !x.IsDeleted)
                    .Select(x => new
                {
                    x.RoomId,
                    x.Code
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/room/seat-type-selects")]
        [HttpGet]
        public HttpResponseMessage GetSeatTypes(int ? roomId)
        {
            try
            {
                var res = (
                    from roomSeat in _context.RoomSeats
                        .Where(x => roomId == null || x.RoomId == roomId)
                    join seatType in _context.SeatTypes
                        on roomSeat.SeatTypeId equals seatType.SeatTypeId
                    select new
                    {
                        seatType.Name,
                        roomSeat.Quantity,
                        seatType.SeatTypeId
                    }).ToList().Select(x=> new
                    {
                        Name = x.Name + " (" + x.Quantity.ToString("D") + " ghế)",
                        x.SeatTypeId
                    });
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/rooms")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]RoomFilter filter)
        {
            try
            {
                var res = _context.Rooms
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.Code.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));
                
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.RoomId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/rooms")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = (
                    from room in _context.Rooms
                        .Where(x => x.RoomId == id)
                    join roomSeat in _context.RoomSeats
                        .Where(x => x.RoomId == id)
                        on room.RoomId equals roomSeat.RoomId into rs
                    from roomSeat in rs.DefaultIfEmpty()
                    select new
                    {
                        Room = room,
                        SeatTypeId = roomSeat == null ? 0 : roomSeat.SeatTypeId,
                        Quantity = roomSeat == null ? 0 : roomSeat.Quantity
                    }).ToList().GroupBy(x => x.Room.RoomId).Select(x => new
                    {
                        x.First().Room.RoomId,
                        x.First().Room.Code,
                        x.First().Room.Large,
                        x.First().Room.Capacity,
                        x.First().Room.Description,
                        x.First().Room.IsDeleted,
                        Seats = x.Where(y=>y.SeatTypeId != 0).Select(y => new
                        {
                            y.SeatTypeId,
                            y.Quantity
                        })
                    }).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/rooms")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]RoomBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var room = model.MapTo<Room>();
                room.IsDeleted = false;
                _context.Rooms.Add(room);
                _context.SaveChanges();
                try
                {
                    foreach (var seat in model.Seats)
                    {
                        _context.RoomSeats.Add(new RoomSeat
                        {
                            Quantity = seat.Quantity,
                            RoomId = room.RoomId,
                            SeatTypeId = seat.SeatTypeId
                        });
                    }
                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch
                {
                    _context.Rooms.Remove(room);
                    _context.SaveChanges();
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
                }
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/rooms")]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]RoomBase model)
        {
            try
            {
                var oldRoom = _context.Rooms.SingleOrDefault(x => x.RoomId == id);
                if (oldRoom == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var oldRoomSeats = _context.RoomSeats.Where(x => x.RoomId == id);
                
                var room = model.MapTo<Room>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                room.IsDeleted = oldRoom.IsDeleted;
                room.RoomId = oldRoom.RoomId;
                _context.Rooms.AddOrUpdate(room);

                foreach (var oldRoomSeat in oldRoomSeats)
                {
                    _context.RoomSeats.Remove(oldRoomSeat);
                }

                foreach (var seat in model.Seats)
                {
                    _context.RoomSeats.Add(new RoomSeat
                    {
                        Quantity = seat.Quantity,
                        RoomId = room.RoomId,
                        SeatTypeId = seat.SeatTypeId
                    });
                }

                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/rooms/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from schedules in _context.Schedules
                        .Where(x => x.RoomId == id && !x.IsDeleted)
                    select schedules).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/rooms/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var room = _context.Rooms.SingleOrDefault(x => x.RoomId == id);
                if (room == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                room.IsDeleted = !room.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/rooms")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var room = _context.Rooms.SingleOrDefault(x => x.RoomId == id);
                if (room == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var oldRoomSeats = _context.RoomSeats.Where(x => x.RoomId == id);

                _context.Rooms.Remove(room);

                foreach (var oldRoomSeat in oldRoomSeats)
                {
                    _context.RoomSeats.Remove(oldRoomSeat);
                }

                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
