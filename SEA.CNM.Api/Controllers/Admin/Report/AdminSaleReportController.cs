﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Domain.Entities;

namespace SEA.CNM.Api.Controllers.Admin.Report
{
    public class AdminSaleReportController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminSaleReportController()
        {
            _context = new EntitiesContext();   
        }


        [Route("api/admin/sale-report")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK,
                "http://localhost/ReportServer/Pages/ReportViewer.aspx?%2fSEA.CNM.Reports%2fBillReport&rs:Command=Render");
        }
    }
}
