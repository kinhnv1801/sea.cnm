﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.Cinema.Film;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Emun;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.Cinema
{
    [ApiAuthorize]
    public class AdminFilmsController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminFilmsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/film-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Films.Where(x=>!x.IsDeleted).Select(x => new
                {
                    x.FilmId,
                    x.Name
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
        
        [Route("api/admin/films")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri] FilmFilter filter)
        {
            try
            {
                if (filter.FieldIds == null) filter.FieldIds = new List<int>();
                if (filter.ActorIds == null) filter.ActorIds = new List<int>();
                if (filter.DirectorIds == null) filter.DirectorIds = new List<int>();
                
                var filmFieldIds = _context.FilmFields.GroupBy(x => x.FilmId)
                    .Where(x => filter.FieldIds.All(y => x.Select(z => z.FieldId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var filmActorIds = _context.FilmArtists
                    .Where(x => x.ArtistType == (int) ArtistType.Actor).GroupBy(x => x.FilmId)
                    .Where(x => filter.ActorIds.All(y => x.Select(z => z.ArtistId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var filmDirectorIds = _context.FilmArtists
                    .Where(x => x.ArtistType == (int)ArtistType.Director).GroupBy(x => x.FilmId)
                    .Where(x => filter.DirectorIds.All(y => x.Select(z => z.ArtistId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var res = _context.Films
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (filter.FieldIds.Count == 0 || filmFieldIds.Contains(x.FilmId)) &&
                        (filter.ActorIds.Count == 0 || filmActorIds.Contains(x.FilmId)) &&
                        (filter.DirectorIds.Count == 0 ||
                         filmDirectorIds.Contains(x.FilmId)) &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.Name.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x => x.FilmId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/films")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = (
                    from film in _context.Films
                        .Where(x => x.FilmId == id)
                    join filmField in _context.FilmFields
                        .Where(x => x.FilmId == id)
                        on film.FilmId equals filmField.FilmId
                    join filmArtist in _context.FilmArtists
                        .Where(x => x.FilmId == id)
                        on film.FilmId equals filmArtist.FilmId
                    select new
                    {
                        film.FilmId,
                        film.Name,
                        film.Avatar,
                        film.CreatedAt,
                        film.Hour,
                        film.Minute,
                        film.LinkReview,
                        film.Review,
                        film.IsDeleted,
                        filmField.FieldId,
                        Artist = new
                        {
                            filmArtist.ArtistId,
                            filmArtist.ArtistType
                        }
                    }).ToList().GroupBy(x => x.FilmId).Select(x => new
                    {
                        x.First().FilmId,
                        x.First().Name,
                        x.First().Avatar,
                        x.First().CreatedAt,
                        x.First().Hour,
                        x.First().Minute,
                        x.First().LinkReview,
                        x.First().Review,
                        x.First().IsDeleted,
                        FieldIds = x.Select(y => y.FieldId).GroupBy(y => y).Select(y => y.Key).ToList(),
                        ActorIds = x.Select(y => y.Artist)
                            .Where(y => y.ArtistType == (int) ArtistType.Actor)
                            .Select(y => y.ArtistId).GroupBy(y => y).Select(y => y.Key).ToList(),
                        DirectorIds = x.Select(y => y.Artist)
                            .Where(y => y.ArtistType == (int) ArtistType.Director)
                            .Select(y => y.ArtistId).GroupBy(y => y).Select(y => y.Key).ToList()
                    }).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/films")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]FilmBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var film = model.MapTo<Film>();
                film.IsDeleted = false;
                _context.Films.Add(film);
                _context.SaveChanges();
                try
                {
                    foreach (var fieldId in model.FieldIds)
                    {
                        _context.FilmFields.Add(new FilmField
                        {
                            FieldId = fieldId,
                            FilmId = film.FilmId
                        });
                    }

                    foreach (var directorId in model.DirectorIds)
                    {
                        _context.FilmArtists.Add(new FilmArtist
                        {
                            ArtistId = directorId,
                            ArtistType = (int) ArtistType.Director,
                            FilmId = film.FilmId
                        });
                    }
                    
                    foreach (var actorId in model.ActorIds)
                    {
                        _context.FilmArtists.Add(new FilmArtist
                        {
                            ArtistId = actorId,
                            ArtistType = (int) ArtistType.Actor,
                            FilmId = film.FilmId
                        });
                    }

                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch
                {
                    _context.Films.Remove(film);
                    _context.SaveChanges();
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
                }
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/films")]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]FilmBase model)
        {
            try
            {
                var oldFilm = _context.Films.SingleOrDefault(x => x.FilmId == id);
                if (oldFilm == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var oldFilmFields = _context.FilmFields.Where(x => x.FilmId == oldFilm.FilmId).ToList();

                var oldFilmArtists = _context.FilmArtists.Where(x => x.FilmId == oldFilm.FilmId).ToList();
                
                var film = model.MapTo<Film>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                film.IsDeleted = oldFilm.IsDeleted;
                film.FilmId = oldFilm.FilmId;
                _context.Films.AddOrUpdate(film);

                foreach (var oldFilmField in oldFilmFields)
                {
                    _context.FilmFields.Remove(oldFilmField);
                }

                foreach (var oldFilmArtist in oldFilmArtists)
                {
                    _context.FilmArtists.Remove(oldFilmArtist);
                }

                foreach (var fieldId in model.FieldIds)
                {
                    _context.FilmFields.Add(new FilmField
                    {
                        FieldId = fieldId,
                        FilmId = film.FilmId
                    });
                }

                foreach (var directorId in model.DirectorIds)
                {
                    _context.FilmArtists.Add(new FilmArtist
                    {
                        ArtistId = directorId,
                        ArtistType = (int)ArtistType.Director,
                        FilmId = film.FilmId
                    });
                }

                foreach (var actorId in model.ActorIds)
                {
                    _context.FilmArtists.Add(new FilmArtist
                    {
                        ArtistId = actorId,
                        ArtistType = (int)ArtistType.Actor,
                        FilmId = film.FilmId
                    });
                }

                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/films/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from schedules in _context.Schedules
                        .Where(x => x.FilmId == id && !x.IsDeleted)
                    select schedules).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/films/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var film = _context.Films.SingleOrDefault(x => x.FilmId == id);
                if (film == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                film.IsDeleted = !film.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/films")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var film = _context.Films.SingleOrDefault(x => x.FilmId == id);
                if (film == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var filmFields = _context.FilmFields.Where(x => x.FilmId == film.FilmId).ToList();

                var filmArtists = _context.FilmArtists.ToList();

                foreach (var oldFilmField in filmFields)
                {
                    _context.FilmFields.Remove(oldFilmField);
                }

                foreach (var oldFilmArtist in filmArtists)
                {
                    _context.FilmArtists.Remove(oldFilmArtist);
                }

                _context.Films.Remove(film);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
