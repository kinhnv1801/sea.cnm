﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.Cinema.Ticket;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.Cinema
{
    [ApiAuthorize]
    public class AdminTicketsController : ApiController
    {
        class TicketInfo
        {
            public int TicketId { get; set; }

            public string TicketCode { get; set; }

            public int Quantity { get; set; }
        }

        class ScheduleInfo
        {
            public int ScheduleId { get; set; }

            public int TimeId { get; set; }

            public int RoomId { get; set; }

            public int FilmId { get; set; }

            public DateTime Date { get; set; }

            public DateTime BeginAt { get; set; }

            public DateTime EndAt { get; set; }

            public string FilmName { get; set; }

            public List<TicketInfo> Tickets { get; set; } 
        }

        class DateInfo
        {
            public DateTime Date { get; set; }

            public List<ScheduleInfo> Schedules { get; set; }
        }

        private readonly EntitiesContext _context;

        public AdminTicketsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/ticket-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Tickets.ToList().Select(x => new
                {
                    x.TicketId,
                    Code = "T" + x.TicketId.ToString("D5")
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/tickets")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]TicketFilter filter)
        {
            try
            {
                var res = (
                    from schedule in _context.Schedules
                        .Where(x =>
                            (filter.Date == null ||
                             EntityFunctions.TruncateTime(x.Date) == EntityFunctions.TruncateTime(filter.Date)) &&
                            (filter.FilmId == null || x.FilmId == filter.FilmId) &&
                            (filter.RoomId == null || x.RoomId == filter.RoomId) &&
                            (filter.TimeId == null || x.TimeId == filter.TimeId))
                    join ticket in _context.Tickets
                        .Where(x =>
                            x.IsDeleted == filter.IsDeleted &&
                            (filter.PublishedAt == null ||
                             EntityFunctions.TruncateTime(x.PublishedAt) == EntityFunctions.TruncateTime(filter.PublishedAt)) &&
                            (filter.SeatTypeId == null || x.SeatTypeId == filter.SeatTypeId))
                        on schedule.ScheduleId equals ticket.ScheduleId
                    join film in _context.Films
                        on schedule.FilmId equals film.FilmId
                    select new
                    {
                        schedule.Date,
                        schedule.TimeId,
                        schedule.RoomId,
                        ticket.TicketId,
                        film.Avatar,
                        schedule.FilmId,
                        ticket.SeatTypeId,
                        ticket.Price,
                        ticket.Quantity,
                        ticket.IsDeleted,
                        ticket.PublishedAt
                    });

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x => x.TicketId).ToPaginatedList(filter.Page, filter.Take)
                    .ToList().Select(x => new
                    {
                        Code = "T" + x.TicketId.ToString("D5"),
                        x.Date,
                        x.TimeId,
                        x.RoomId,
                        x.TicketId,
                        x.Avatar,
                        x.FilmId,
                        x.SeatTypeId,
                        x.Price,
                        x.Quantity,
                        x.IsDeleted,
                        x.PublishedAt
                    }),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }


        [Route("api/admin/ticket-calendar")]
        [HttpGet]
        // ReSharper disable once FunctionComplexityOverflow
        public HttpResponseMessage Get([FromUri]TicketCalendarFilter filter)
        {
            try
            {
                if (filter.BeginDate == null)
                    filter.BeginDate = DateTime.Today;
                var endDate = ((DateTime)filter.BeginDate).AddDays(6);
                var dates = new List<DateTime>
                {
                    ((DateTime) filter.BeginDate),
                    ((DateTime) filter.BeginDate).AddDays(1),
                    ((DateTime) filter.BeginDate).AddDays(2),
                    ((DateTime) filter.BeginDate).AddDays(3),
                    ((DateTime) filter.BeginDate).AddDays(4),
                    ((DateTime) filter.BeginDate).AddDays(5),
                    ((DateTime) filter.BeginDate).AddDays(6)
                };

                var rooms = _context.Rooms
                    .Where(x =>
                        !x.IsDeleted &&
                        (filter.RoomId == null || filter.RoomId == x.RoomId))
                    .ToList();

                var res = rooms.Select(x => new
                {
                    x.RoomId,
                    x.Code,
                    Dates = dates.Select(y => new DateInfo
                    {
                        Date = y,
                        Schedules = new List<ScheduleInfo>()
                    }).ToList()
                }).ToList();

                var schedules = (
                    from schedule in _context.Schedules
                        .Where(x =>
                            !x.IsDeleted &&
                            EntityFunctions.TruncateTime(x.Date) >= filter.BeginDate &&
                            EntityFunctions.TruncateTime(x.Date) <= endDate)
                    join film in _context.Films
                        .Where(x => !x.IsDeleted)
                        on schedule.FilmId equals film.FilmId
                    join time in _context.Times
                        .Where(x => !x.IsDeleted)
                        on schedule.TimeId equals time.TimeId
                    join ticket in _context.Tickets
                        .Where(x => !x.IsDeleted)
                        on schedule.ScheduleId equals ticket.ScheduleId into ticketTemp
                    from ticket in ticketTemp.DefaultIfEmpty()
                    select new
                    {
                        ScheduleId = schedule.ScheduleId,
                        RoomId = schedule.RoomId,
                        TimeId = schedule.TimeId,
                        FilmId = film.FilmId,
                        Date = schedule.Date,
                        FilmName = film.Name,
                        BeginAt = time.BeginAt,
                        EndAt = time.EndAt,
                        TicketId = ticket == null ? 0 : ticket.TicketId,
                        Quantity = ticket == null ? 0 : ticket.Quantity
                    }).ToList().GroupBy(x => x.ScheduleId)
                    .Select(x => new ScheduleInfo
                    {
                        BeginAt = x.First().BeginAt,
                        ScheduleId = x.First().ScheduleId,
                        Date = x.First().Date,
                        EndAt = x.First().EndAt,
                        FilmId = x.First().FilmId,
                        FilmName = x.First().FilmName,
                        RoomId = x.First().RoomId,
                        TimeId = x.First().TimeId,
                        Tickets = x.Where(y => y.TicketId != 0).Select(y => new TicketInfo
                        {
                            Quantity = (int) y.Quantity,
                            TicketCode = "T" + y.TicketId.ToString("D5"),
                            TicketId = y.TicketId
                        }).ToList()
                    });

                // ReSharper disable once PossibleMultipleEnumeration
                foreach (var room in res)
                {
                    foreach (var date in room.Dates)
                    {
                        date.Schedules = schedules
                            .Where(x => x.Date.ToString("yy-MM-dd") == date.Date.ToString("yy-MM-dd") &&
                                        x.RoomId == room.RoomId).ToList();
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, res);

            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/tickets/price")]
        [HttpGet]
        public HttpResponseMessage GetPrice(int ticketId, int quantity)
        {
            try
            {
                var res = _context.Tickets
                    .Where(x => x.SeatTypeId == ticketId).Select(x => x.Price)
                    .SingleOrDefault() * quantity;
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/tickets")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = (
                    from ticket in _context.Tickets
                        .Where(x => x.TicketId == id)
                    join schedule in _context.Schedules
                        on ticket.ScheduleId equals schedule.ScheduleId
                    join film in _context.Films
                        on schedule.FilmId equals film.FilmId
                    select new
                    {
                        schedule.Date,
                        schedule.TimeId,
                        schedule.RoomId,
                        ticket.TicketId,
                        film.Avatar,
                        schedule.FilmId,
                        ticket.SeatTypeId,
                        ticket.Price,
                        ticket.Quantity,
                        ticket.PublishedAt,
                        ticket.IsDeleted
                    }).ToList().Select(x => new
                    {
                        Code = "T" + x.TicketId.ToString("D5"),
                        x.Date,
                        x.TimeId,
                        x.RoomId,
                        x.TicketId,
                        x.Avatar,
                        x.FilmId,
                        x.SeatTypeId,
                        x.Price,
                        x.Quantity,
                        x.PublishedAt,
                        x.IsDeleted
                    }).SingleOrDefault();

                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/tickets")]
        public HttpResponseMessage Post([FromBody]TicketBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                var date = (DateTime)model.Date;
                date = date.AddHours(7);

                var schedule = _context.Schedules
                    .Where(x =>
                        x.RoomId == model.RoomId &&
                        x.TimeId == model.TimeId &&
                        (x.Date.Day == date.Day && x.Date.Month == date.Month && x.Date.Year == date.Year) &&
                        x.FilmId == model.FilmId).SingleOrDefault();
                if (schedule == null)
                {
                    ModelState.AddModelError("", "Không tìm thấy suất chiếu");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var ticket = new Ticket
                {
                    IsDeleted = false,
                    Price = model.Price,
                    PublishedAt = model.PublishedAt,
                    Quantity = model.Quantity,
                    ScheduleId = schedule.ScheduleId,
                    SeatTypeId = model.SeatTypeId
                };

                _context.Tickets.Add(ticket);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/tickets")]
        public HttpResponseMessage Put(int id, [FromBody]TicketBase model)
        {
            try
            {
                var oldTicket = _context.Tickets.SingleOrDefault(x => x.TicketId == id);
                if (oldTicket == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var ticket = model.MapTo<Ticket>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                var date = (DateTime)model.Date;
                date = date.AddHours(7);

                var schedule = _context.Schedules
                    .Where(x =>
                        x.RoomId == model.RoomId &&
                        x.TimeId == model.TimeId &&
                        (x.Date.Day == date.Day && x.Date.Month == date.Month && x.Date.Year == date.Year) &&
                        x.FilmId == model.FilmId).SingleOrDefault();
                if (schedule == null)
                {
                    ModelState.AddModelError("", "Không tìm thấy suất chiếu");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                ticket.ScheduleId = schedule.ScheduleId;
                ticket.IsDeleted = oldTicket.IsDeleted;
                ticket.TicketId = oldTicket.TicketId;
                _context.Tickets.AddOrUpdate(ticket);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/tickets/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from billTicket in _context.BillTickets
                        .Where(x => x.TicketId == id)
                    join bill in _context.Bills
                        .Where(x => !x.IsDeleted)
                        on billTicket.BillId equals bill.BillId
                    select bill).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/tickets/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var ticket = _context.Tickets.SingleOrDefault(x => x.TicketId == id);
                if (ticket == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                ticket.IsDeleted = !ticket.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/tickets")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var ticket = _context.Tickets.SingleOrDefault(x => x.TicketId == id);
                if (ticket == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Tickets.Remove(ticket);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
