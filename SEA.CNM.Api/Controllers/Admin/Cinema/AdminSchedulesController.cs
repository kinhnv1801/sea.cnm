﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.Cinema.Schedule;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.Cinema
{
    [ApiAuthorize]
    public class AdminSchedulesController : ApiController
    {
        class ScheduleTime
        {
            public int ScheduleId { get; set; }

            public int TimeId { get; set; }

            public DateTime BeginAt { get; set; }

            public DateTime EndAt { get; set; }

            public int FilmId { get; set; }

            public string FilmName { get; set; }
        }

        private readonly EntitiesContext _context;

        public AdminSchedulesController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/schedule/time-selects")]
        [HttpGet]
        public HttpResponseMessage GetTimes(int? roomId)
        {
            try
            {
                var res = (
                    from schedule in _context.Schedules
                        .Where(x => roomId == null || x.RoomId == roomId)
                    join time in _context.Times
                        on schedule.TimeId equals time.TimeId
                    select new
                    {
                        time.TimeId,
                        time.BeginAt,
                        time.EndAt
                    }).ToList().Select(x => new
                    {
                        x.TimeId,
                        Title = x.BeginAt.AddHours(7).ToString("hh:mm:ss") + " - " + x.EndAt.AddHours(7).ToString("hh:mm:ss")
                    });
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/schedule/date-selects")]
        [HttpGet]
        public HttpResponseMessage GetDates(int? roomId, int? timeId)
        {
            try
            {
                var res = _context.Schedules
                    .Where(x =>
                        (roomId == null || x.RoomId == roomId) &&
                        (timeId == null || x.TimeId == timeId))
                    .ToList().Select(x => new
                    {
                        Title = x.Date.ToString("dd-MM-yyyy"),
                        Value = x.Date
                    });
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/schedule/film-selects")]
        [HttpGet]
        public HttpResponseMessage GetFilms(int? roomId, int? timeId, DateTime? date)
        {
            try
            {
                var dateTime = new DateTime();
                if (date != null)
                    dateTime = (DateTime)date;

                var res = (
                    from schedule in _context.Schedules
                        .Where(x =>
                        (roomId == null || x.RoomId == roomId) &&
                        (timeId == null || x.TimeId == timeId) &&
                        (date == null || (x.Date.Day == dateTime.Day && x.Date.Month == dateTime.Month && x.Date.Year == dateTime.Year)))
                    join film in _context.Films
                        on schedule.FilmId equals film.FilmId
                    select new
                    {
                        film.Name,
                        film.FilmId
                    });
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/schedules")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]ScheduleFilter filter)
        {
            try
            {
                var res = (
                    from schedule in _context.Schedules
                        .Where(x =>
                            x.IsDeleted == filter.IsDeleted &&
#pragma warning disable 618
 (filter.Date == null || EntityFunctions.TruncateTime(x.Date) == filter.Date) &&
#pragma warning restore 618
 (filter.FilmId == null || x.FilmId == filter.FilmId) &&
                            (filter.RoomId == null || x.RoomId == filter.RoomId) &&
                            (filter.TimeId == null || x.TimeId == filter.TimeId))
                    join film in _context.Films
                        on schedule.FilmId equals film.FilmId
                    select new
                    {
                        schedule.Date,
                        schedule.TimeId,
                        schedule.RoomId,
                        schedule.ScheduleId,
                        film.Avatar,
                        schedule.FilmId
                    });

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x => x.ScheduleId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }


        [Route("api/admin/schedule-calendar")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]ScheduleCalendarFilter filter)
        {
            try
            {
                if (filter.BeginDate == null)
                    filter.BeginDate = DateTime.Today;
                var endDate = ((DateTime)filter.BeginDate).AddDays(6);
                var dates = new List<DateTime>
                {
                    ((DateTime) filter.BeginDate),
                    ((DateTime) filter.BeginDate).AddDays(1),
                    ((DateTime) filter.BeginDate).AddDays(2),
                    ((DateTime) filter.BeginDate).AddDays(3),
                    ((DateTime) filter.BeginDate).AddDays(4),
                    ((DateTime) filter.BeginDate).AddDays(5),
                    ((DateTime) filter.BeginDate).AddDays(6)
                };

                var rooms = _context.Rooms
                    .Where(x =>
                        !x.IsDeleted &&
                        (filter.RoomId == null || filter.RoomId == x.RoomId))
                    .ToList();

                var times = _context.Times.Where(x => !x.IsDeleted).ToList();

                var core = rooms.Select(x => new
                {
                    x.RoomId,
                    x.Code,
                    Dates = dates.Select(y => new
                    {
                        Date = y,
                        Times = times.Select(z => new ScheduleTime
                        {
                            TimeId = z.TimeId,
                            BeginAt = z.BeginAt.AddHours(7),
                            EndAt = z.EndAt.AddHours(7),
                            FilmId = 0,
                            FilmName = ""
                        }).ToList()

                    }).ToList()
                }).ToList();

                var schedules = (
                    from schedule in _context.Schedules
                        .Where(x =>
                            !x.IsDeleted &&
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.Date) >= filter.BeginDate &&
#pragma warning restore 618
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.Date) <= endDate
#pragma warning restore 618
)
                    join film in _context.Films
                        .Where(x => !x.IsDeleted)
                        on schedule.FilmId equals film.FilmId
                    select new
                    {
                        schedule.ScheduleId,
                        schedule.Date,
                        schedule.RoomId,
                        schedule.TimeId,
                        film.Name,
                        film.FilmId
                    }).ToList();

                foreach (var room in core)
                {
                    foreach (var date in room.Dates)
                    {
                        for (var k = 0; k < date.Times.Count; k++)
                        {
                            foreach (var schedule in schedules)
                            {
                                if (date.Date.ToString("yy-MM-dd") == schedule.Date.ToString("yy-MM-dd") &&
                                    room.RoomId == schedule.RoomId &&
                                    date.Times[k].TimeId == schedule.TimeId)
                                {
                                    var beginAt = date.Times[k].BeginAt;
                                    var endAt = date.Times[k].EndAt;

                                    date.Times[k].FilmName = schedule.Name;
                                    date.Times[k].FilmId = schedule.FilmId;
                                    date.Times[k].ScheduleId = schedule.ScheduleId;
                                    var removeTime = date.Times
                                        .Where(x =>
                                            (x.BeginAt > beginAt && x.BeginAt < endAt) ||
                                            (x.EndAt > beginAt && x.EndAt < endAt) ||

                                            (beginAt > x.BeginAt && beginAt < x.EndAt) ||
                                            (endAt > x.BeginAt && endAt < x.EndAt))
                                        .ToList();
                                    foreach (var time in removeTime)
                                    {
                                        date.Times.Remove(time);
                                    }
                                }
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, core);

            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/schedules")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = _context.Schedules.Where(x => x.ScheduleId == id).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/schedules")]
        public HttpResponseMessage Post([FromBody]ScheduleBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                if (model.Date != null)
                    model.Date = ((DateTime)model.Date).AddHours(7);
                var schedule = model.MapTo<Schedule>();
                schedule.IsDeleted = false;
                _context.Schedules.Add(schedule);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/schedules")]
        public HttpResponseMessage Put(int id, [FromBody]ScheduleBase model)
        {
            try
            {
                var oldSchedule = _context.Schedules.SingleOrDefault(x => x.ScheduleId == id);
                if (oldSchedule == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var schedule = model.MapTo<Schedule>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                if (model.Date != null) 
                    model.Date = ((DateTime)model.Date).AddHours(7);
                schedule.IsDeleted = oldSchedule.IsDeleted;
                schedule.ScheduleId = oldSchedule.ScheduleId;
                _context.Schedules.AddOrUpdate(schedule);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/schedules/changed-status")]
        public HttpResponseMessage CheckStatus(int id)
        {
            try
            {
                if ((
                    from schedules in _context.Tickets
                        .Where(x => x.ScheduleId == id && !x.IsDeleted)
                    select schedules).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/schedules/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var schedule = _context.Schedules.SingleOrDefault(x => x.ScheduleId == id);
                if (schedule == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                schedule.IsDeleted = !schedule.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/schedules")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var schedule = _context.Schedules.SingleOrDefault(x => x.ScheduleId == id);
                if (schedule == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Schedules.Remove(schedule);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
