﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.Cinema.Bill;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.Cinema
{
    [ApiAuthorize]
    public class AdminBillsController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminBillsController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/bills")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]BillFilter filter)
        {
            try
            {
                if (filter.TicketIds == null) filter.TicketIds = new List<int>();
                
                var billIds = _context.BillTickets.GroupBy(x => x.BillId)
                    .Where(x => filter.TicketIds.All(y => x.Select(z => z.TicketId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var res = (
                    from bill in _context.Bills
                        .Where(x =>
                            x.IsDeleted == filter.IsDeleted &&
                            (filter.TicketIds.Count == 0 || billIds.Contains(x.BillId)))
                    join user in _context.Users
                        .Where(x =>
                            (string.IsNullOrEmpty(filter.SearchText) ||
                             x.FullName.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())))
                        on bill.UserId equals user.UserId into u
                    from user in u.DefaultIfEmpty()
                    select new
                    {
                        bill,
                        user
                    });

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x => x.bill.BillId).Select(x => new
                    {
                        x.bill.BillId,
                        x.bill.Price,
                        x.bill.CreatedAt,
                        UserName = x.user == null ? null : x.user.UserName,
                        FullName = x.user == null ? null : x.user.FullName
                    }).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/bills/price")]
        [HttpGet]
        public HttpResponseMessage GetPrice(string ticketsJson)
        {
            try
            {
                var tickets = ticketsJson.ToObject<List<BillTicketBase>>();
                return Request.CreateResponse(HttpStatusCode.OK, GetPrice(tickets));
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/bills")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var res = (
                    from bill in _context.Bills
                        .Where(x => x.BillId == id)
                    join billTicket in _context.BillTickets
                        on bill.BillId equals billTicket.BillId
                    select new
                    {
                        bill.BillId,
                        bill.Price,
                        bill.UserId,
                        billTicket.TicketId,
                        billTicket.Quantity
                    }).ToList().GroupBy(x=>x.BillId)
                    .Select(x=> new
                    {
                        BillId = x.Key,
                        x.First().Price,
                        x.First().UserId,
                        Tickets = x.Select(y=> new
                        {
                            y.Quantity,
                            y.TicketId
                        }).ToList()
                    }).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/bills")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]BillBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var bill = model.MapTo<Bill>();
                bill.Code = Guid.NewGuid().ToString();
                bill.Price = GetPrice(model.Tickets);
                bill.CreatedAt = DateTime.Now;
                bill.IsDeleted = false;
                _context.Bills.Add(bill);
                _context.SaveChanges();
                try
                {
                    foreach (var ticket in model.Tickets)
                    {
                        _context.BillTickets.Add(new BillTicket
                        {
                            BillId = bill.BillId,
                            Quantity = ticket.Quantity,
                            TicketId = ticket.TicketId
                        });
                    }
                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch
                {
                    _context.Bills.Remove(bill);
                    _context.SaveChanges();
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
                }
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/bills")]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]BillBase model)
        {
            try
            {
                var oldBill = _context.Bills.SingleOrDefault(x => x.BillId == id);
                if (oldBill == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var oldBillTickets = _context.BillTickets.Where(x => x.BillId == oldBill.BillId).ToList();
                
                var bill = model.MapTo<Bill>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                bill.Code = oldBill.Code;
                bill.CreatedAt = oldBill.CreatedAt;
                bill.Price = GetPrice(model.Tickets);
                bill.IsDeleted = oldBill.IsDeleted;
                bill.BillId = oldBill.BillId;
                _context.Bills.AddOrUpdate(bill);

                foreach (var oldBillTicket in oldBillTickets)
                {
                    _context.BillTickets.Remove(oldBillTicket);
                }

                foreach (var ticket in model.Tickets)
                {
                    _context.BillTickets.Add(new BillTicket
                    {
                        BillId = bill.BillId,
                        Quantity = ticket.Quantity,
                        TicketId = ticket.TicketId
                    });
                }
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/bills/status")]
        public HttpResponseMessage ChangeStatus(int id)
        {
            try
            {
                var bill = _context.Bills.SingleOrDefault(x => x.BillId == id);
                if (bill == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                bill.IsDeleted = !bill.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/bills")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var bill = _context.Bills.SingleOrDefault(x => x.BillId == id);
                if (bill == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);
                var billTickets = _context.BillTickets.Where(x => x.BillId == bill.BillId).ToList();

                _context.Bills.Remove(bill);

                foreach (var billTicket in billTickets)
                {
                    _context.BillTickets.Remove(billTicket);
                }

                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        private decimal GetPrice(List<BillTicketBase> tickets)
        {
            decimal? res = 0;

            var ticketIds = tickets.Select(y => y.TicketId);

            var core = _context.Tickets
                .Where(x => ticketIds.Contains(x.TicketId)).ToList();
            foreach (var ticket in tickets)
            {
                var singleOrDefault = core.SingleOrDefault(x => x.TicketId == ticket.TicketId);
                if (singleOrDefault != null)
                    res += ticket.Quantity * singleOrDefault.Price;
            }
            return (res ?? 0);
        }
    }
}
