﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.User.User;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.AccountEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.User
{
    [ApiAuthorize]
    public class AdminUsersController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminUsersController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/user-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Users.ToList().Select(x => new
                {
                    x.UserName,
                    x.UserId
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/user/profile")]
        [HttpGet]
        public HttpResponseMessage GetProfile()
        {
            try
            {
                var userId = User.Identity.GetUserId();

                var profile = (
                    from user in _context.Users
                        .Where(x => x.UserId == userId && !x.IsDeleted)
                    join userRole in _context.UserRoles
                        on user.UserId equals userRole.UserId
                    join role in _context.Roles
                        .Where(x => !x.IsDeleted)
                        on userRole.RoleId equals role.RoleId
                    select new
                    {
                        user.FullName,
                        user.Avatar,
                        user.UserId,
                        role.Apis
                    }).ToList()
                    .GroupBy(x => x.UserId)
                    .Select(x => new
                    {
                        x.First().FullName,
                        x.First().Avatar,
                        Apis = x.Select(y => y.Apis.ToObject<List<string>>()).GroupBy(y => y).Select(y => y.Key)
                    }).ToList().SingleOrDefault();
                if(profile == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

                return Request.CreateResponse(HttpStatusCode.OK, profile);

            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/users")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]UserFilter filter)
        {
            try
            {
                var res = (
                    from user in _context.Users
                        .Where(x =>
                            x.IsDeleted == filter.IsDeleted &&
                            (string.IsNullOrEmpty(filter.SearchText) ||
                             x.UserName.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())))
                    join userRole in _context.UserRoles
                        on user.UserId equals userRole.UserId
                    join role in _context.Roles
                        on userRole.RoleId equals role.RoleId
                    select new
                    {
                        user.UserName,
                        user.FullName,
                        user.UserId,
                        role.Name
                    }).GroupBy(x => x.UserId).Select(x => new
                    {
                        x.FirstOrDefault().FullName,
                        x.FirstOrDefault().UserName,
                        x.FirstOrDefault().UserId,
                        RoleNames = x.Select(y => y.Name)
                    });

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x => x.UserId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/users")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.UserId == id)
                    .ToList()
                    .Select(x => new
                    {
                        x.UserId,
                        x.UserName,
                        x.FullName,
                        x.Gender,
                        x.DateOfBirth,
                        x.Phone,
                        x.Email,
                        x.Avatar,
                        x.Address,
                        RoleIds = _context.UserRoles.Where(y => y.UserId == x.UserId).Select(y => y.RoleId).ToList(),
                        x.IsDeleted
                    }).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/users")]
        public HttpResponseMessage Post([FromBody]UserPost model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                if (model.Confirm != model.Password)
                {
                    ModelState.AddModelError("Confirm", "Mật khẩu và xác nhận mật khẩu khác nhau");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var user = model.MapTo<Domain.Entities.AccountEntities.User>();
                user.UserId = Guid.NewGuid().ToString();
                user.IsDeleted = false;
                _context.Users.Add(user);
                _context.SaveChanges();
                foreach (var roleId in model.RoleIds)
                {
                    _context.UserRoles.Add(new UserRole
                    {
                        RoleId = roleId,
                        UserId = user.UserId
                    });
                }
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/users")]
        public HttpResponseMessage Put(string id, [FromBody]UserBase model)
        {
            try
            {
                var oldUser = _context.Users.SingleOrDefault(x => x.UserId == id);
                if (oldUser == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var oldUserRoles = _context.UserRoles
                    .Where(x => x.UserId == oldUser.UserId).ToList();

                var user = model.MapTo<Domain.Entities.AccountEntities.User>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                user.IsDeleted = oldUser.IsDeleted;
                user.UserId = oldUser.UserId;
                user.UserName = oldUser.UserName;
                user.Password = oldUser.Password;

                foreach (var oldUserRole in oldUserRoles)
                {
                    _context.UserRoles.Remove(oldUserRole);
                } 
                
                foreach (var roleId in model.RoleIds)
                {
                    _context.UserRoles.Add(new UserRole
                    {
                        RoleId = roleId,
                        UserId = user.UserId
                    });
                }

                _context.Users.AddOrUpdate(user);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/users/changed-status")]
        public HttpResponseMessage CheckStatus(string id)
        {
            try
            {
                if ((
                    from bill in _context.Bills
                        .Where(x => x.UserId == id)
                    select bill).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/users/status")]
        public HttpResponseMessage ChangeStatus(string id)
        {
            try
            {
                var user = _context.Users.SingleOrDefault(x => x.UserId == id);
                if (user == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                user.IsDeleted = !user.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/users")]
        public HttpResponseMessage Delete(string id)
        {
            try
            {
                var user = _context.Users.SingleOrDefault(x => x.UserId == id);
                if (user == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var userRoles = _context.UserRoles
                    .Where(x => x.UserId == user.UserId).ToList();

                foreach (var userRole in userRoles)
                {
                    _context.UserRoles.Remove(userRole);
                } 
                
                _context.Users.Remove(user);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
