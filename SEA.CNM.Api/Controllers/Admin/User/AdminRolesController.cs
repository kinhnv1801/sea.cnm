﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Admin.User.Role;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.AccountEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers.Admin.User
{
    [ApiAuthorize]
    public class AdminRolesController : ApiController
    {
        private readonly EntitiesContext _context;

        public AdminRolesController()
        {
            _context = new EntitiesContext();
        }

        [Route("api/admin/role-selects")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var res = _context.Roles.Select(x => new
                {
                    x.RoleId,
                    x.Name
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/api-selects")]
        [HttpGet]
        public HttpResponseMessage GetApis()
        {
            try
            {
                var core = new List<string>();
                
                var controllers =
                    from assembly in AppDomain.CurrentDomain.GetAssemblies()
                    from type in assembly.GetTypes()
                    where type.IsSubclassOf(typeof(ApiController))
                    select type;

                foreach (var controller in controllers)
                {
                    var apis = controller.GetMethods()
                        .Where(x => x.GetCustomAttributes(typeof (RouteAttribute), true).Length > 0)
                        .Select(x => x.CustomAttributes)
                        .Select(x =>
                            (x.FirstOrDefault(y => y.AttributeType.FullName == "System.Web.Http.HttpGetAttribute") !=
                             null
                                ? "(GET)"
                                : x.FirstOrDefault(y => y.AttributeType.FullName == "System.Web.Http.HttpPostAttribute") !=
                                  null
                                    ? "(POST)"
                                    : x.FirstOrDefault(
                                        y => y.AttributeType.FullName == "System.Web.Http.HttpPutAttribute") != null
                                        ? "(PUT)"
                                        : x.FirstOrDefault(
                                            y => y.AttributeType.FullName == "System.Web.Http.HttpDeleteAttribute") !=
                                          null
                                            ? "(DELETE)"
                                            : "(GET)") + " " +
                            x.FirstOrDefault(y => y.AttributeType.FullName == "System.Web.Http.RouteAttribute")
                                .ConstructorArguments.Select(z => z.Value).FirstOrDefault()
                        ).ToList();

                    core.AddRange(apis);
                }
                var res = core.Select(x => new
                {
                    Title = x,
                    Value = x
                }).ToList().GroupBy(x => x.Value).Select(x => new
                {
                    x.First().Value,
                    x.First().Title
                }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch 
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/roles")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]RoleFilter filter)
        {
            try
            {
                var res = _context.Roles
                    .Where(x =>
                        x.IsDeleted == filter.IsDeleted &&
                        (string.IsNullOrEmpty(filter.SearchText) ||
                         x.Name.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())));
                
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Items = res.OrderBy(x=>x.RoleId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, res.Count())
                });
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [Route("api/admin/roles")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            try
            {
                var res = _context.Roles.Where(x => x.RoleId == id)
                    .ToList()
                    .Select(x => new
                    {
                        x.RoleId,
                        x.Name,
                        x.Description,
                        x.RoleType,
                        Apis = x.Apis.ToObject<List<string>>(),
                        x.IsDeleted,
                    }).SingleOrDefault();
                if (res == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPost]
        [Route("api/admin/roles")]
        public HttpResponseMessage Post([FromBody]RoleBase model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                var role = model.MapTo<Role>();
                role.RoleId = Guid.NewGuid().ToString();
                role.IsDeleted = false;
                role.Apis = model.Apis.ToJson();
                _context.Roles.Add(role);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/roles")]
        public HttpResponseMessage Put(string id, [FromBody]RoleBase model)
        {
            try
            {
                var oldRole = _context.Roles.SingleOrDefault(x => x.RoleId == id);
                if (oldRole == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                var role = model.MapTo<Role>();
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                role.IsDeleted = oldRole.IsDeleted;
                role.Apis = model.Apis.ToJson();
                role.RoleId = oldRole.RoleId;
                _context.Roles.AddOrUpdate(role);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpGet]
        [Route("api/admin/roles/changed-status")]
        public HttpResponseMessage CheckStatus(string id)
        {
            try
            {
                if ((
                    from userRole in _context.UserRoles
                        .Where(x => x.RoleId == id)
                    join user in _context.Users
                        .Where(x => !x.IsDeleted)
                        on userRole.UserId equals user.UserId
                    select user).Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, false);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpPut]
        [Route("api/admin/roles/status")]
        public HttpResponseMessage ChangeStatus(string id)
        {
            try
            {
                var role = _context.Roles.SingleOrDefault(x => x.RoleId == id);
                if (role == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                role.IsDeleted = !role.IsDeleted;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [HttpDelete]
        [Route("api/admin/roles")]
        public HttpResponseMessage Delete(string id)
        {
            try
            {
                var role = _context.Roles.SingleOrDefault(x => x.RoleId == id);
                if (role == null)
                    return Request.CreateErrorResponse(HttpStatusCode.OK, SysError.NOT_FOUND);

                _context.Roles.Remove(role);
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
