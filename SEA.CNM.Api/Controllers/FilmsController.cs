﻿using SEA.CNM.Domain.Dto.Film;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Dto.NganLuong;
using SEA.CNM.Domain.Emun;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers
{
    public class FilmsController : ApiController
    {
        private readonly EntitiesContext _context;

        private readonly NganLuongService _nganLuongService;

        public FilmsController()
        {
            _context = new EntitiesContext();
            _nganLuongService = new NganLuongService();
        }

        /// <summary>
        /// Lấy danh sách trong ô chọn diễn viên và đạo diễn
        /// </summary>
        /// <returns></returns>
        [Route("api/artist-selects")]
        [HttpGet]
        public HttpResponseMessage GetArtistSelects()
        {
            try
            {
                var res = _context.Artists.Where(x => !x.IsDeleted)
                    .Select(x => new
                    {
                        x.ArtistId,
                        x.FullName
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Lấy danh sách trong ô chọn lĩnh vực
        /// </summary>
        /// <returns></returns>
        [Route("api/field-selects")]
        [HttpGet]
        public HttpResponseMessage GetFieldSelects()
        {
            try
            {
                var res = _context.Fields.Where(x => !x.IsDeleted)
                    .Select(x => new
                    {
                        x.FieldId,
                        x.Name
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Lấy danh sách trong ô chọn thời gian chiếu
        /// </summary>
        /// <returns></returns>
        [Route("api/time-selects")]
        [HttpGet]
        public HttpResponseMessage GetTimeSelects()
        {
            try
            {
                var res = _context.Times
                    .Where(x => !x.IsDeleted)
                    .ToList()
                    .Select(x => new
                    {
                        x.TimeId,
                        Title =
                            x.BeginAt.AddHours(7).ToString("hh:mm:ss") + " - " +
                            x.EndAt.AddHours(7).ToString("hh:mm:ss")
                    }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Lấy danh sách phân trang của phim đã có vé bán và dưa đến ngày chiếu của vé đó
        /// Cần có 2 giá trị Take và Page để chạy API
        /// Nếu kg nhập giá trị trong filter thì giá trị kg được sủ dụng
        /// </summary>
        /// <param name="filter">
        /// SearchText kiểu chuỗi ký tự
        /// 
        /// TimeIds kiểu danh sách số nguyên
        /// 
        /// FieldIds kiểu danh sách số nguyên
        /// 
        /// DirectorIds kiểu danh sách số nguyên
        /// 
        /// ActorIds kiểu danh sách số nguyên
        /// 
        /// ScheduleDate kiểu datetime
        /// </param>
        /// <returns></returns>
        [Route("api/films")]
        [HttpGet]
        // ReSharper disable once FunctionComplexityOverflow
        public HttpResponseMessage Get([FromUri]FilmFilter filter)
        {
            try
            {
                var nowDate = DateTime.Now;

                if (filter.TimeIds == null) filter.TimeIds = new List<int>();
                if (filter.FieldIds == null) filter.FieldIds = new List<int>();
                if (filter.ActorIds == null) filter.ActorIds = new List<int>();
                if (filter.DirectorIds == null) filter.DirectorIds = new List<int>();

                var filmFieldIds = _context.FilmFields.GroupBy(x => x.FilmId)
                    .Where(x => filter.FieldIds.All(y => x.Select(z => z.FieldId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var filmActorIds = _context.FilmArtists
                    .Where(x => x.ArtistType == (int)ArtistType.Actor).GroupBy(x => x.FilmId)
                    .Where(x => filter.ActorIds.All(y => x.Select(z => z.ArtistId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var filmDirectorIds = _context.FilmArtists
                    .Where(x => x.ArtistType == (int)ArtistType.Director).GroupBy(x => x.FilmId)
                    .Where(x => filter.DirectorIds.All(y => x.Select(z => z.ArtistId).Contains(y)))
                    .Select(x => x.Key).ToList();

                var core = (
                    from film in _context.Films
                        .Where(x =>
                            (filter.FieldIds.Count == 0 || filmFieldIds.Contains(x.FilmId)) &&
                            (filter.ActorIds.Count == 0 || filmActorIds.Contains(x.FilmId)) &&
                            (filter.DirectorIds.Count == 0 ||
                             filmDirectorIds.Contains(x.FilmId)) &&
                            (string.IsNullOrEmpty(filter.SearchText) ||
                             x.Name.Trim().ToLower().Contains(filter.SearchText.Trim().ToLower())))
                    join schedule in _context.Schedules
                        .Where(x =>
                            (x.Date == filter.ScheduleDate || filter.ScheduleDate == null) &&
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.Date) > nowDate &&
#pragma warning restore 618
 !x.IsDeleted)
                        on film.FilmId equals schedule.FilmId
                    join ticket in _context.Tickets
                        .Where(x =>
                            !x.IsDeleted &&
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.PublishedAt) < nowDate)
#pragma warning restore 618
 on schedule.ScheduleId equals ticket.ScheduleId
                    join time in _context.Times
                        .Where(x =>
                            !x.IsDeleted &&
                            (filter.TimeIds.Contains(x.TimeId) || filter.TimeIds.Count == 0))
                        on schedule.TimeId equals time.TimeId
                    join filmField in _context.FilmFields
                        on film.FilmId equals filmField.FilmId into filmFieldTemp
                    from filmField in filmFieldTemp.DefaultIfEmpty()
                    join filmArtist in _context.FilmArtists
                        on film.FilmId equals filmArtist.FilmId into filmArtistTemp
                    from filmArtist in filmArtistTemp.DefaultIfEmpty()
                    select new
                    {
                        film.FilmId,
                        film.Avatar,
                        film.LinkReview,
                        FilmName = film.Name,
                        film.Hour,
                        film.Minute,
                        ticket.PublishedAt,
                        FieldId = filmField == null ? 0 : filmField.FieldId,
                        ArtistId = filmArtist == null ? 0 : filmArtist.ArtistId,
                        ArtistType = filmArtist == null ? 0 : filmArtist.ArtistType
                    }).GroupBy(x => x.FilmId)
                    .Select(x => new
                    {
                        x.FirstOrDefault().FilmId,
                        x.FirstOrDefault().FilmName,
                        x.FirstOrDefault().Avatar,
                        x.FirstOrDefault().LinkReview,
                        x.FirstOrDefault().Hour,
                        x.FirstOrDefault().Minute,
                        FieldIds = x.Where(y => y.FieldId != 0).GroupBy(y => y.FieldId).Select(y => y.Key),
                        ActorIds = x.Where(y => y.FieldId != 0 && y.ArtistType == (int)ArtistType.Actor)
                            .GroupBy(y => y.ArtistId).Select(y => y.Key),
                        DirectorIds = x.Where(y => y.FieldId != 0 && y.ArtistType == (int)ArtistType.Director)
                            .GroupBy(y => y.ArtistId).Select(y => y.Key)
                    });

                var res = new
                {
                    Items = core.OrderBy(x => x.FilmId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, core.Count())
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
        
        /// <summary>
        /// Lấy thông tin Film
        /// </summary>
        /// <param name="id">kiểu số nguyên</param>
        /// <returns></returns>
        [Route("api/films")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var nowDate = DateTime.Now;

            var res = (
                from film in _context.Films
                    .Where(x => x.FilmId == id)
                join schedule in _context.Schedules
                    .Where(x =>
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.Date) > nowDate &&
#pragma warning restore 618
 !x.IsDeleted)
                    on film.FilmId equals schedule.FilmId
                join room in _context.Rooms
                    .Where(x => !x.IsDeleted)
                    on schedule.RoomId equals room.RoomId
                join ticket in _context.Tickets
                    .Where(x =>
                        !x.IsDeleted &&
#pragma warning disable 618
 EntityFunctions.TruncateTime(x.PublishedAt) < nowDate)
#pragma warning restore 618
 on schedule.ScheduleId equals ticket.ScheduleId
                join seatType in _context.SeatTypes
                    .Where(x => !x.IsDeleted)
                    on ticket.SeatTypeId equals seatType.SeatTypeId
                join time in _context.Times
                    .Where(x => !x.IsDeleted)
                    on schedule.TimeId equals time.TimeId
                join filmFieldName in
                    (
                        from filmField in _context.FilmFields
                        join field in _context.Fields
                            .Where(x => !x.IsDeleted)
                            on filmField.FieldId equals field.FieldId
                        select new
                        {
                            field.Name,
                            filmField.FilmId
                        }
                        )
                    on film.FilmId equals filmFieldName.FilmId into filmFieldTemp
                from filmFieldName in filmFieldTemp.DefaultIfEmpty()
                join filmArtistName in
                    (
                        from filmArist in _context.FilmArtists
                        join artist in _context.Artists
                            .Where(x => !x.IsDeleted)
                                on filmArist.ArtistId equals artist.ArtistId
                        select new
                        {
                            artist.FullName,
                            filmArist.FilmId,
                            filmArist.ArtistType
                        })
                    on film.FilmId equals filmArtistName.FilmId into filmArtistTemp
                from filmArtistName in filmArtistTemp.DefaultIfEmpty()
                select new
                {
                    film.FilmId,
                    film.Review,
                    film.Avatar,
                    film.LinkReview,
                    FilmName = film.Name,
                    film.Hour,
                    film.Minute,
                    schedule.Date,
                    FieldName = filmFieldName == null ? null : filmFieldName.Name,
                    ArtistId = filmArtistName == null ? null : filmArtistName.FullName,
                    ArtistType = filmArtistName == null ? 0 : filmArtistName.ArtistType,
                    ticket.TicketId,
                    ticket.Price,
                    ticket.Quantity,
                    RoomCode = room.Code,
                    SeatTypeName = seatType.Name
                }).ToList().GroupBy(x => x.FilmId)
                .Select(x => new
                {
                    x.First().FilmId,
                    x.First().Review,
                    x.First().FilmName,
                    x.First().Avatar,
                    x.First().LinkReview,
                    x.First().Hour,
                    x.First().Minute,
                    FieldNames = x.Where(y => y.FieldName != null).GroupBy(y => y.FieldName).Select(y => y.Key),
                    ActorNames = x.Where(y => y.FieldName != null && y.ArtistType == (int)ArtistType.Actor)
                        .GroupBy(y => y.ArtistId).Select(y => y.Key),
                    DirectorNames = x.Where(y => y.FieldName != null && y.ArtistType == (int)ArtistType.Director)
                        .GroupBy(y => y.ArtistId).Select(y => y.Key),
                    Tickets = x.Select(y => new
                    {
                        y.TicketId,
                        TicketCode = "T" + y.TicketId.ToString("D5"),
                        y.Price,
                        y.Quantity,
                        y.Date,
                        y.RoomCode,
                        y.SeatTypeName
                    }).GroupBy(y => y).Select(y => y.Key)
                }).SingleOrDefault();
            if (res == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [ApiAuthorize]
        [Route("api/ngan-luong-url")]
        [HttpGet]
        public HttpResponseMessage GetNganLuongUrl([FromUri]string modelString)
        {
            try
            {
                var model = modelString.ToObject<NganLuongUrlInput>();

                decimal price = 0;

                var ticketIds = model.Tickets.Select(x => x.TicketId);

                var tickets = _context.Tickets
                    .Where(x => ticketIds.Contains(x.TicketId));

                foreach (var ticket in tickets)
                {
                    var ticket1 = ticket;
                    var quantity = model.Tickets.Where(x => x.TicketId == ticket1.TicketId)
                        .Select(x => x.Quantity).SingleOrDefault();

                    price += (decimal)(quantity * ticket.Price);
                }

                var checkOutUrl = _nganLuongService.BuildCheckoutUrl(new BuildCheckoutUrlInput
                {
                    Price = 2000,//price,
                    AffiliateCode = "",
                    Quantity = model.Tickets.Count,
                    BuyerInfo = "",
                    Currency = "vnd",
                    Discount = 0,
                    FeeCal = 0,
                    FeeShipping = 0,
                    OrderCode = Guid.NewGuid().ToString(),
                    OrderDescription = model.Tickets.Select(x => new
                    {
                        TicketCode = "T" + x.TicketId.ToString("D5"),
                        x.Quantity
                    }).ToJson(),
                    ReturnUrl = model.ReturnUrl,
                    Tax = 0,
                    TransactionInfo = ""
                });

                return Request.CreateResponse(HttpStatusCode.OK, checkOutUrl);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        [ApiAuthorize]
        [Route("api/bill")]
        [HttpPost]
        public HttpResponseMessage CreateBill([FromBody]BillPost model)
        {
            try
            {
                var userId = User.Identity.GetUserId();

                var checkNganLuonng = _nganLuongService.VerifyPayment(model);
                if (!checkNganLuonng)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

                if (_context.Bills.Any(x => x.Code == model.OrderCode))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);

                var bill = new Bill
                {
                    IsDeleted = false,
                    Price = Decimal.Parse(model.Price),
                    Code = model.OrderCode,
                    CreatedAt = DateTime.Now,
                    UserId = userId
                };
                
                _context.Bills.Add(bill);
                _context.SaveChanges();

                var tickets = model.Tickets.ToObject<List<TicketInfo>>();

                try
                {
                    foreach (var ticket in tickets)
                    {
                        var t = _context.Tickets.SingleOrDefault(x => x.TicketId == ticket.TicketId);
                        if (t != null)
                        {
                            t.Quantity = t.Quantity - ticket.Quantity;
                        }

                        _context.BillTickets.Add(new BillTicket
                        {
                            BillId = bill.BillId,
                            Quantity = ticket.Quantity,
                            TicketId = ticket.TicketId
                        });
                    }
                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    _context.Bills.Remove(bill);
                    _context.SaveChanges();
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
                }
            }
            catch 
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Tạo hoa đơn không cần kiểm tra ngân lượng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ApiAuthorize]
        [Route("api/bill-for-mobile")]
        [HttpPost]
        public HttpResponseMessage CreateBill([FromBody]BillPostForMobile model)
        {
            try
            {
                var userId = User.Identity.GetUserId();

                var bill = new Bill
                {
                    IsDeleted = false,
                    Price = model.Price,
                    Code = Guid.NewGuid().ToString(),
                    CreatedAt = DateTime.Now,
                    UserId = userId
                };

                _context.Bills.Add(bill);
                _context.SaveChanges();

                var tickets = model.Tickets.ToObject<List<TicketInfo>>();

                try
                {
                    foreach (var ticket in tickets)
                    {
                        var t = _context.Tickets.SingleOrDefault(x => x.TicketId == ticket.TicketId);
                        if (t != null)
                        {
                            t.Quantity = t.Quantity - ticket.Quantity;
                        }

                        _context.BillTickets.Add(new BillTicket
                        {
                            BillId = bill.BillId,
                            Quantity = ticket.Quantity,
                            TicketId = ticket.TicketId
                        });
                    }
                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    _context.Bills.Remove(bill);
                    _context.SaveChanges();
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
                }
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
