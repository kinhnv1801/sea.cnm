﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SEA.CNM.Domain.Dto.Bill;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.Common;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.Extensions;

namespace SEA.CNM.Api.Controllers
{
    [ApiAuthorize]
    public class BillsController : ApiController
    {
        private readonly EntitiesContext _context;

        public BillsController()
        {
            _context = new EntitiesContext();
        }

        /// <summary>
        /// Lấy dánh sách phim trong ô chọn
        /// </summary>
        /// <returns></returns>
        [Route("api/film-selects")]
        [HttpGet]
        public HttpResponseMessage GetFilms()
        {
            try
            {
                var res = _context.Films.Where(x => !x.IsDeleted)
                    .Select(x => new
                    {
                        x.FilmId,
                        x.Name
                    }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Lấy danh sách phân trang của hóa đơn
        /// </summary>
        /// <param name="filter">
        /// FilmIds kiểu danh sách nguyên
        /// 
        /// FromDate kiểu datetime chấp nhân null
        /// 
        /// ToDate kiểu datetime chấp nhận null
        /// 
        /// Page kiểu nguyên bắt buộc
        /// 
        /// Take kiểu nguyên bắt buộc
        /// 
        /// </param>
        /// <returns></returns>
        [Route("api/bills")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]BillFilter filter)
        {
            try
            {
                if (filter.FilmIds == null) filter.FilmIds = new List<int>();

                var userId = User.Identity.GetUserId();

                var core = (
                    from bill in _context.Bills
                        .Where(x => 
                            !x.IsDeleted && x.UserId == userId &&
                            (x.CreatedAt < filter.ToDate || filter.ToDate == null) &&
                            (x.CreatedAt > filter.FromDate || filter.ToDate == null))
                    join billTicket in _context.BillTickets
                        on bill.BillId equals billTicket.BillId
                    join ticket in _context.Tickets
                        .Where(x => !x.IsDeleted)
                        on billTicket.TicketId equals ticket.TicketId
                    join schedule in _context.Schedules
                        .Where(x => !x.IsDeleted)
                        on ticket.ScheduleId equals schedule.ScheduleId
                    join film in _context.Films
                        .Where(x =>
                            !x.IsDeleted &&
                            (filter.FilmIds.Contains(x.FilmId) || filter.FilmIds.Count == 0))
                        on schedule.FilmId equals film.FilmId
                    join time in _context.Times
                        .Where(x => !x.IsDeleted)
                        on schedule.TimeId equals time.TimeId
                    join room in _context.Rooms
                        .Where(x => !x.IsDeleted)
                        on schedule.RoomId equals room.RoomId
                    join seatType in _context.SeatTypes
                        .Where(x => !x.IsDeleted)
                        on ticket.SeatTypeId equals seatType.SeatTypeId
                    select new
                    {
                        bill.BillId,
                        bill.CreatedAt,
                        FilmName = film.Name,
                        ScheduleDate = schedule.Date,
                        BeginWatch = time.BeginAt,
                        EndWatch = time.EndAt,
                        RoomCode = room.Code,
                        SeatTypeName = seatType.Name,
                        Quantity = billTicket.Quantity,
                        Price = ticket.Price,
                        TotalPrice = bill.Price
                    }).GroupBy(x => x.BillId)
                    .Select(x => new
                    {
                        x.FirstOrDefault().BillId,
                        x.FirstOrDefault().CreatedAt,
                        x.FirstOrDefault().TotalPrice,
                        TicketInfos = x.Select(y => new
                        {
                            y.FilmName,
                            y.ScheduleDate,
                            y.BeginWatch,
                            y.EndWatch,
                            y.RoomCode,
                            y.SeatTypeName,
                            y.Quantity,
                            y.Price
                        }).GroupBy(y=>y).Select(y=>y.Key)
                    });

                var res = new
                {
                    Items = core.OrderBy(x => x.BillId).ToPaginatedList(filter.Page, filter.Take),
                    PageInfo = new PageInfo(filter.Page, filter.Take, core.Count())
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
    }
}
