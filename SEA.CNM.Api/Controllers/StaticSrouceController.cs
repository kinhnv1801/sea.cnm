﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SEA.CNM.Api.Controllers
{
    public class StaticSrouceController : ApiController
    {
        [Route("api/static/genders")]
        [HttpGet]
        public HttpResponseMessage GetGenders()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new List<object>
            {
                new
                {
                    Value = true,
                    Title = "Nam"
                },
                new
                {
                    Value = false,
                    Title = "Nữ"
                }
            });
        }

        [Route("api/static/stauts")]
        [HttpGet]
        public HttpResponseMessage GetStatus()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new List<object>
            {
                new
                {
                    Value = true,
                    Title = "Đang chạy"
                },
                new
                {
                    Value = false,
                    Title = "Dừng kích hoạt"
                }
            });
        }

        [Route("api/static/role-types")]
        [HttpGet]
        public HttpResponseMessage GetRoleTypes()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new List<object>
            {
                new
                {
                    Value = true,
                    Title = "Quyền mặc định"
                },
                new
                {
                    Value = false,
                    Title = "Quyền chọn"
                }
            });
        }
    }
}
