﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using SEA.CNM.Api.Attributes;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Dto.User;
using SEA.CNM.Domain.Entities;
using SEA.CNM.Domain.Entities.AccountEntities;

namespace SEA.CNM.Api.Controllers
{
    [ApiAuthorize]
    public class UserController : ApiController
    {
        private readonly EntitiesContext _context;

        public UserController()
        {
            _context = new EntitiesContext();
        }

        /// <summary>
        /// Đăng ký tài khoản
        /// </summary>
        /// <param name="model">
        /// UserName chuỗi kiểu ký tự
        /// 
        /// Password chuỗi kiểu ký tự
        /// 
        /// ConfirmPassword chuỗi kiểu ký tự
        /// 
        /// FullName chuỗi kiểu ký tự
        /// 
        /// Phone chuỗi kiểu ký tự
        /// 
        /// Email chuỗi kiểu ký tự
        /// </param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("api/user")]
        [HttpPost]
        public HttpResponseMessage Post(UserPost model)
        {
            //try
            //{
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Mật khẩu và xác nhận mật khẩu khác nhau");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var user = model.MapTo<User>();

                user.UserId = Guid.NewGuid().ToString();
                user.IsDeleted = false;
                var roleIds = _context.Roles
                    .Where(x => x.RoleType && !x.IsDeleted)
                    .Select(x=>x.RoleId);
                _context.Users.Add(user);
                foreach (var roleId in roleIds)
                {
                    _context.UserRoles.Add(new UserRole
                    {
                        RoleId = roleId,
                        UserId = user.UserId
                    });
                }
                _context.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            //}
            //catch (Exception)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            //}
        }
        
        [Route("api/user/profile")]
        [HttpGet]
        public HttpResponseMessage Profile()
        {
            try
            {
                var userId = User.Identity.GetUserId();

                var profile = (
                    from user in _context.Users
                        .Where(x => x.UserId == userId && !x.IsDeleted)
                    join userRole in _context.UserRoles
                        on user.UserId equals userRole.UserId
                    join role in _context.Roles
                        .Where(x => !x.IsDeleted)
                        on userRole.RoleId equals role.RoleId
                    select new
                    {
                        user.Address,
                        user.Avatar,
                        user.DateOfBirth,
                        user.Email,
                        user.FullName,
                        user.Gender,
                        user.Phone,
                        user.UserId,
                        user.UserName,
                        role.Apis
                    }).ToList()
                    .GroupBy(x => x.UserId)
                    .Select(x => new
                    {
                        x.First().Address,
                        x.First().Avatar,
                        x.First().DateOfBirth,
                        x.First().Email,
                        x.First().FullName,
                        x.First().Gender,
                        x.First().Phone,
                        x.First().UserId,
                        x.First().UserName,
                        Apis = x.Select(y => y.Apis.ToObject<List<string>>()).GroupBy(y => y).Select(y => y.Key)
                    }).ToList().SingleOrDefault();
                if (profile == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

                return Request.CreateResponse(HttpStatusCode.OK, profile);
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Sửa mật khẩu
        /// </summary>
        /// <param name="userId">kiểu ký chuỗi tự</param>
        /// <param name="model">
        /// OldPassword kiểu chuỗi ký tự
        /// 
        /// Password kiểu chuỗi ký tự
        /// 
        /// ConfirmPassword kiểu chuỗi ký tự
        /// </param>
        /// <returns></returns>
        [Route("api/user/password")]
        [HttpPut]
        public HttpResponseMessage EditPassword(string userId, EditPasswordModel model)
        {
            try
            {
                var user = _context.Users.SingleOrDefault(x => x.UserId == userId);
                if (user == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Mật khẩu và xác nhận mật khẩu khác nhau");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                if (model.OldPassword != user.Password)
                {
                    ModelState.AddModelError("OldPassword", "Mật khẩu cũ sai");
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                user.Password = model.Password;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }

        /// <summary>
        /// Sửa thông tin tài khoản
        /// </summary>
        /// <param name="userId">kiểu chuỗi ký tự</param>
        /// <param name="model">
        /// 
        /// FullName kiểu chuỗi ký tự
        /// 
        /// Gender kiểu bool
        /// 
        /// DateOfBirth kiểu datetime
        /// 
        /// Phone kiểu chuỗi ký tự
        /// 
        /// Email kiểu chuỗi ký tự
        /// 
        /// Avatar kiểu chuỗi ký tự
        /// Với Avatar, anh đẩy ảnh lên web server trước. sau đó đưa đường dẫn qua web service
        /// 
        /// Address kiểu chuỗi ký tự
        /// </param>
        /// <returns></returns>
        [Route("api/user/profile")]
        [HttpPut]
        public HttpResponseMessage EditProfile(string userId, EditProfileModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                
                var user = _context.Users.SingleOrDefault(x => x.UserId == userId);
                if (user == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, SysError.NOT_FOUND);

                user.FullName = model.FullName;
                user.Gender = model.Gender;
                user.DateOfBirth = model.DateOfBirth;
                user.Phone = model.Phone;
                user.Email = model.Email;
                user.Avatar = model.Avatar;
                user.Address = model.Address;
                _context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, SysError.SERVICE_ERROR);
            }
        }
        

        /// <summary>
        /// Đăng xuất
        /// </summary>
        /// <returns></returns>
        [Route("api/user/logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }
        
        #endregion
    }
}
