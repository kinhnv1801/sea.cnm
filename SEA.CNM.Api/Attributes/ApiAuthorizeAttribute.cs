﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;
using SEA.CNM.Api.Extension;
using SEA.CNM.Domain.Entities;

namespace SEA.CNM.Api.Attributes
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var context = new EntitiesContext();

            var res = base.IsAuthorized(actionContext);

            if (!res)
                return false;

            var userId = HttpContext.Current.User.Identity.GetUserId();

            var core = (
                from userRole in context.UserRoles
                    .Where(x => x.UserId == userId)
                join role in context.Roles
                    .Where(x => !x.IsDeleted)
                    on userRole.RoleId equals role.RoleId
                select role.Apis).ToList().Select(x => x.ToObject<List<string>>());

            var apis = new List<string>();
            apis = core.Aggregate(apis, (current, api) => current.Concat(api).ToList());

            var method = actionContext.Request.Method;
            var uri = HttpContext.Current.Request.Url.AbsolutePath.Substring(1);
            
            return apis.Any(x => x.Contains("(" + method + ")") && x.Contains(uri));
        }

    }
}