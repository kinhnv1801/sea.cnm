﻿using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using SEA.CNM.Domain.Dto.NganLuong;

namespace SEA.CNM.Api
{
    public class NganLuongService
    {
        private readonly string _merchantSiteCode;
        private readonly string _receiver;
        private readonly string _securePass;
        private readonly string _nganLuongUrl;

        public NganLuongService()
        {
            _merchantSiteCode = "45009";
            _receiver = "nguyenvankinh181196@gmail.com";
            _securePass = "d6f12bd2eeff2104deee8f11ad177abc";
            _nganLuongUrl = "https://www.nganluong.vn/checkout.php";
        }

        public string CreateMd5Hash1(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            foreach (byte t in hashBytes)
            {
                sb.Append(t.ToString("x2"));
            }
            return sb.ToString();
        }

        public string GetMd5Hash(String input)
        {
            var x = new MD5CryptoServiceProvider();
            var bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            var s = new StringBuilder();
            foreach (var b in bs)
                s.Append(b.ToString("x2").ToLower());
            var md5String = s.ToString();
            return md5String;
        }
        
        public string BuildCheckoutUrl(BuildCheckoutUrlInput model)
        {
            var strReturnUrl = model.ReturnUrl.ToLower();
            var securityCode = "";
            securityCode += "" + _merchantSiteCode;
            securityCode += " " + strReturnUrl;
            securityCode += " " + _receiver;
            securityCode += " " + model.TransactionInfo;
            securityCode += " " + model.OrderCode;
            securityCode += " " + model.Price;
            securityCode += " " + model.Currency;
            securityCode += " " + model.Quantity;
            securityCode += " " + model.Tax;
            securityCode += " " + model.Discount;
            securityCode += " " + model.FeeCal;
            securityCode += " " + model.FeeShipping;
            securityCode += " " + model.OrderDescription;
            securityCode += " " + model.BuyerInfo;
            securityCode += " " + model.AffiliateCode;
            securityCode += " " + _securePass;

            var md5 = GetMd5Hash(securityCode);

            var urlEncode = HttpUtility.UrlEncode(strReturnUrl);
            if (urlEncode == null) return "#";
            var ht = new Hashtable
            {
                {"merchant_site_code", _merchantSiteCode},
                {"return_url", urlEncode.ToLower()},
                {"receiver", HttpUtility.UrlEncode(_receiver)},
                {"transaction_info", model.TransactionInfo},
                {"order_code", model.OrderCode},
                {"price", model.Price},
                {"currency", model.Currency},
                {"quantity", model.Quantity},
                {"tax", model.Tax},
                {"discount", model.Discount},
                {"fee_cal", model.FeeCal},
                {"fee_shipping", model.FeeShipping},
                {"order_description", HttpUtility.UrlEncode(model.OrderDescription)},
                {"buyer_info", HttpUtility.UrlEncode(model.BuyerInfo)},
                {"affiliate_code", model.AffiliateCode},
                {"secure_code", md5}
            };

            var redirectUrl = _nganLuongUrl;

            if (redirectUrl.IndexOf("?", StringComparison.Ordinal) == -1)
            {
                redirectUrl += "?";
            }
            else if (redirectUrl.Substring(redirectUrl.Length - 1, 1) != "?" && redirectUrl.IndexOf("&", StringComparison.Ordinal) == -1)
            {
                redirectUrl += "&";
            }

            var url = "";

            var en = ht.GetEnumerator();

            while (en.MoveNext())
            {
                if (url == "")
                    url += en.Key + "=" + en.Value;
                else
                    url += "&" + en.Key + "=" + en.Value;
            }

            var rdu = redirectUrl + url;

            return rdu;
        }


        public bool VerifyPayment(VerifyPaymentInput model)
        {
            var str = "";
            str += " " + HttpUtility.HtmlDecode(model.TransactionInfo);
            str += " " + model.OrderCode;
            str += " " + model.Price;
            str += " " + model.PaymentId;
            str += " " + model.PaymentType;
            str += " " + HttpUtility.HtmlDecode(model.ErrorText);
            str += " " + _merchantSiteCode;
            str += " " + _securePass;

            var verifySecureCode = GetMd5Hash(str);

            return verifySecureCode == model.SecureCode;
        }


        public string Base64Encode(string data)
        {
            try
            {
                var encDataByte = Encoding.UTF8.GetBytes(data);
                var encodedData = Convert.ToBase64String(encDataByte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Encode" + e.Message);
            }
        }

        public string Base64Decode(string data)
        {
            try
            {
                var encoder = new UTF8Encoding();
                var utf8Decode = encoder.GetDecoder();

                var todecodeByte = Convert.FromBase64String(data);
                var charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                var decodedChar = new char[charCount];
                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
                var result = new String(decodedChar);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }
        }

        public string Base64Encode1(string source)
        {
            var b = Encoding.ASCII.GetBytes(source);

            return Convert.ToBase64String(b);
        }

        //public string Nl_checkoder(int total, string orderCode, string paymentId)
        //{

        //    string param = "";
        //    param = "<ORDERS><TOTAL>1</TOTAL><ORDER><ORDER_CODE>" + orderCode + "</ORDER_CODE><PAYMENT_ID>" + paymentId + "</PAYMENT_ID></ORDER></ORDERS>";
        //    string paramdata = this.Base64Encode(param);

        //    string checksum = this.GetMd5Hash(this.MerchantSiteCode + this.SecurePass);

        //    APInlcheckorder.NGANLUONG_API nlcheck = new APInlcheckorder.NGANLUONG_API();

        //    string result = nlcheck.checkOrder_v2(this.MerchantSiteCode, paramdata, checksum);

        //    //string result = nlcheck.checkOrder_v2("21971", "PE9SREVSUz48VE9UQUw+MTwvVE9UQUw+PE9SREVSPjxPUkRFUl9DT0RFPjAwODI2NzwvT1JERVJfQ09ERT48UEFZTUVOVF9JRD48L1BBWU1FTlRfSUQ+PC9PUkRFUj48L09SREVSUz4=", "4a1039aa996976ddfcbaf48db3a13d7d");

        //    return this.Base64Decode(result);
        //}
    }
}

