﻿'use strict';

angular.module('app.sections.ticket', ['ngRoute'])
    .config([
        '$routeProvider', function ($routeProvider) {
            $routeProvider
                .when('/tickets', {
                    templateUrl: '/App/sections/ticket/ticket.html',
                    controller: 'Ticket'
                });
        }
    ]).controller('Ticket',
    [
        '$scope', '$host',
        function ($scope, $host) {

            Number.prototype.formatMoney = function (c, d, t) {
                var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            document.title = 'Lịch sử mua vé';

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/film-selects'
                })(function (res) {
                    $scope.filmSrouce = {
                        Items: res.data,
                        Value: 'FilmId',
                        Title: 'Name',
                        Placeholder: 'Chọn phim'
                    };
                });
            })();

            (function main() {
                $scope.loadSuccess = false;
                $scope.filter = {
                    Page: 1,
                    Take: 10
                }

                $scope.loadPage = function (page) {
                    $scope.loadSuccess = false;
                    if (page == undefined) {
                        $scope.filter.Page = 1;
                    } else {
                        $scope.filter.Page = page;
                    }

                    $host({
                        method: 'GET',
                        params: $scope.filter,
                        url: 'api/bills'
                    })(function (res) {
                        $scope.items = res.data.Items;
                        $scope.pageInfo = res.data.PageInfo;
                        $scope.loadSuccess = true;
                    });
                }

                $scope.loadPage();
            })();
        }
    ]);