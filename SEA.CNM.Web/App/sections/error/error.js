﻿'use strict';

angular.module('app.sections.error', ['ngRoute'])
    .config([
        '$routeProvider', function ($routeProvider) {
            $routeProvider
                .when('/404', {
                    templateUrl: '/App/sections/error/404/error.404.html',
                    controller: 'Error404'
                }).when('/500', {
                    templateUrl: '/App/sections/error/500/error.500.html',
                    controller: 'Error500'
                }).when('/403', {
                    templateUrl: '/App/sections/error/404/error.404.html',
                    controller: 'Error403'
                });
        }
    ]);