﻿'use strict';

angular.module('app.sections',
    [
        'ngRoute',
        'app.sections.home',
        'app.sections.film',
        'app.sections.ticket',
        'app.sections.user',
        'app.sections.error'
    ])
    .config([
        '$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({ redirectTo: '/' });
        }
    ]);