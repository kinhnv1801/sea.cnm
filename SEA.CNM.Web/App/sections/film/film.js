﻿'use strict';

angular.module('app.sections.film', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
            $routeProvider
                .when('/films', {
                    templateUrl: '/App/sections/film/index/film.index.html',
                    controller: 'FilmIndex'
                }).when('/films/:id', {
                    templateUrl: '/App/sections/film/item/film.item.html',
                    controller: 'FilmItem'
                });
        }
    ]);