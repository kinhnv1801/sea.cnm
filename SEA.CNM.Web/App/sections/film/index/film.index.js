﻿angular
    .module('app.sections.film')
    .controller('FilmIndex', [
        '$scope', '$host',
        function ($scope, $host) {
            document.title = 'Danh sách phim';
            (function makeSrouce() {
                $scope.loadSuccess = false;
                $host({
                    method: 'GET',
                    url: 'api/artist-selects'
                })(function (res) {
                    $scope.actorSrouce = {
                        Items: res.data,
                        Value: 'ArtistId',
                        Title: 'FullName',
                        Multiple: true,
                        Placeholder: 'Chọn diễn viên'
                    };

                    $scope.directorSrouce = {
                        Items: res.data,
                        Value: 'ArtistId',
                        Title: 'FullName',
                        Multiple: true,
                        Placeholder: 'Chọn đạo diễn'
                    };
                });

                $host({
                    method: 'GET',
                    url: 'api/field-selects'
                })(function (res) {
                    $scope.fieldSrouce = {
                        Items: res.data,
                        Value: 'FieldId',
                        Title: 'Name',
                        Multiple: true,
                        Placeholder: 'Chọn lĩnh vực'
                    };
                });

                $host({
                    method: 'GET',
                    url: 'api/time-selects'
                })(function (res) {
                    $scope.timeSrouce = {
                        Items: res.data,
                        Title: 'Title',
                        Value: 'TimeId',
                        Multiple: true,
                        Placeholder: 'Chọn giờ chiếu'
                    };
                });
            })();

            (function main() {
                $scope.filter = {
                    SearchText: '',
                    Page: 1,
                    Take: 10
                }

                $scope.loadPage = function (page) {
                    $scope.loadSuccess = false;
                    if (page == undefined) {
                        $scope.filter.Page = 1;
                    } else {
                        $scope.filter.Page = page;
                    }

                    $host({
                        method: 'GET',
                        params: $scope.filter,
                        url: 'api/films'
                    })(function (res) {
                        $scope.items = res.data.Items;
                        $scope.pageInfo = res.data.PageInfo;
                        $scope.loadSuccess = true;
                    });
                }

                $scope.loadPage();

            })();
        }
    ]);