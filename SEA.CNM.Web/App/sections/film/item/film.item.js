﻿angular
    .module('app.sections.film')
    .controller('FilmItem', [
        '$scope', '$host', '$routeParams', '$cookies', '$location',
        function ($scope, $host, $routeParams, $cookies, $location) {
            document.title = 'Chi tiết phim';

            Number.prototype.formatMoney = function(c, d, t) {
                var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            (function main() {
                var filmId = $routeParams.id;

                (function createBill() {
                    var card = $cookies.get('card');
                    if (card != undefined) {
                        if ($routeParams.secure_code != undefined) {
                            var billPost = {
                                TransactionInfo: $routeParams.transaction_info,
                                OrderCode: $routeParams.order_code,
                                Price: $routeParams.price,
                                PaymentId: $routeParams.payment_id,
                                PaymentType: $routeParams.payment_type,
                                ErrorText: $routeParams.error_text,
                                SecureCode: $routeParams.secure_code,
                                Tickets: card
                            }
                            console.log(billPost);
                            $host({
                                method: 'POST',
                                url: 'api/bill',
                                data: JSON.stringify(billPost)
                            })(function(res) {
                                alert('Bạn đã đặt vé thành công, Cảm ơn bạn đã sử dụng dịch vụ');
                                $cookies.remove('card');
                                window.history.pushState("", "", $location.absUrl().split('?')[0]);
                            });
                        }
                    }
                })();

                function updateCard() {
                    var card = $cookies.get('card');
                    if (card != undefined) {
                        $scope.card = JSON.parse(card);

                        for (var i = 0; i < $scope.card.length; i++) {
                            for (var j = 0; j < $scope.item.Tickets.length; j++) {
                                if ($scope.card[i].TicketId === $scope.item.Tickets[j].TicketId) {
                                    $scope.item.Tickets[j].Quantity = $scope.item.Tickets[j].Quantity - $scope.card[i].Quantity;
                                }
                            }
                        }
                    }

                };

                $host({
                    method: 'GET',
                    url: 'api/films',
                    params: {
                        id: filmId
                    }
                })(function(res) {
                    $scope.item = res.data;
                    updateCard();
                });

                (function cardFunc() {
                    $scope.card = [];

                    $scope.addTicketInCard = function (ticketId) {
                        $scope.item.Tickets = $scope.item.Tickets.map(function (e) {
                            if (e.TicketId === ticketId) {
                                if (e.Quantity <= 0) {
                                    alert('Hết vé ' + e.TicketCode);
                                } else {
                                    e.Quantity = e.Quantity - 1;
                                    if ($scope.card.filter(function (e) {
                                        return e.TicketId === ticketId;
                                    }).length === 0) {
                                        $scope.card.push({
                                            TicketId: ticketId,
                                            Quantity: 1
                                        });
                                    } else {
                                        $scope.card = $scope.card.map(function (e) {
                                            if (e.TicketId === ticketId) {
                                                return {
                                                    TicketId: ticketId,
                                                    Quantity: e.Quantity + 1
                                                }
                                            }
                                            return e;
                                        });
                                    }

                                    $cookies.put('card', JSON.stringify($scope.card));
                                }
                            }
                            return e;
                        });
                    }

                    $scope.removeTicketInCard = function (ticketId) {
                        if ($scope.card.filter(function (e) {
                            return e.TicketId === ticketId;
                        }).length === 0) {
                            alert('Không có vé này trong giỏ hàng');
                            return 0;
                        } else if ($scope.card.filter(function (e) {
                            return e.TicketId === ticketId && e.Quantity === 1;
                        }).length === 1) {
                            $scope.card = $scope.card.filter(function (e) {
                                return e.TicketId !== ticketId;
                            });
                        } else {
                            $scope.card = $scope.card.map(function (e) {
                                if (e.TicketId === ticketId) {
                                    return {
                                        TicketId: ticketId,
                                        Quantity: e.Quantity - 1
                                    }
                                }
                                return e;
                            });
                        }

                        $cookies.put('card', JSON.stringify($scope.card));

                        $scope.item.Tickets = $scope.item.Tickets.map(function (e) {
                            if (e.TicketId === ticketId) {
                                e.Quantity = e.Quantity + 1;
                            }
                            return e;
                        });
                    }

                    $scope.getSum = function (ticketId, quantity) {
                        var price = $scope.item.Tickets.filter(function (e) {
                            return e.TicketId === ticketId;
                        })[0].Price;

                        return quantity * price;
                    }

                    $scope.getSumAll = function () {
                        var totalPrice = 0;
                        for (var i = 0; i < $scope.card.length; i++) {
                            totalPrice += $scope.getSum($scope.card[i].TicketId, $scope.card[i].Quantity);
                        }
                        return totalPrice;
                    }

                    $scope.getTicketCode = function (ticketId) {
                        var ticketCode = $scope.item.Tickets.filter(function (e) {
                            return e.TicketId === ticketId;
                        })[0].TicketCode;

                        return ticketCode;
                    }

                })();

                $scope.checkOut = function () {
                    if ($cookies.get('token') == undefined) {
                        $location.search('returnPath', $location.path());
                        $location.path('/login');
                        $location.replace();
                    } else {
                        var returnUrl = $location.absUrl();
                        var params = {
                            Tickets: $scope.card,
                            ReturnUrl: returnUrl
                        };

                        $host({
                            method: 'GET',
                            url: 'api/ngan-luong-url',
                            params: {
                                modelString: JSON.stringify(params)
                            }
                        })(function (res) {
                            window.location.href = res.data;
                        });
                    }
                }
            })();
        }
    ]);