﻿'use strict';

angular
    .module('app.sections.user')
    .controller('UserLogin', [
        '$scope', '$location', '$token', '$routeParams', '$cookies',
        function ($scope, $location, $token, $routeParams, $cookies) {
            document.title = 'Đăng nhập';
            if ($cookies.get('token') != undefined) {
                $location.path('/');
                $location.replace();
            }

            $scope.login = function () {
                $token($scope.userName, $scope.password)(function (res) {
                    if ($routeParams.returnPath != undefined) {
                        $location.path($routeParams.returnPath);
                        $location.search('returnPath', null);
                    } else {
                        $location.path('/');
                    }
                    $location.replace();
                });
            }
        }
    ]);