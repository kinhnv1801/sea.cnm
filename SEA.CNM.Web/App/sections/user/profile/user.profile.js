﻿'use strict';

angular
    .module('app.sections.user')
    .controller('UserProfile', [
        '$scope', '$host', '$cookies', '$location',
        function ($scope, $host, $cookies, $location) {
            (function makeLayout() {
                document.title = 'Thông tin cá nhân';
                $scope.profilePage = true;
                $scope.editProfilePage = false;
                $scope.editPasswordPage = false;
            })();

            (function main() {
                if ($cookies.get('token') == undefined) {
                    $location.path('/login');
                    $location.search('returnPath', '/profile');
                    $location.replace();
                }

                $scope.editProfile = function () {
                    $scope.profilePage = false;
                    $scope.editProfilePage = true;
                    $scope.editPasswordPage = false;
                    $host({
                        method: 'GET',
                        url: 'api/user/profile'
                    })(function (res) {
                        $scope.item = res.data;
                        $scope.item.Gender = $scope.item.Gender ? "true" : "false";
                        $scope.item.DateOfBirth = new Date($scope.item.DateOfBirth);
                    }, function (res) {

                    });
                }

                $scope.editPassword = function () {
                    $scope.profilePage = false;
                    $scope.editProfilePage = false;
                    $scope.editPasswordPage = true;
                }

                $host({
                    method: 'GET',
                    url: 'api/user/profile'
                })(function (res) {
                    $scope.item = res.data;
                    $scope.item.Gender = $scope.item.Gender ? "Nam" : "Nữ";
                    $scope.item.DateOfBirth = new Date($scope.item.DateOfBirth);
                }, function (res) {

                });

                $scope.save = function () {
                    if ($scope.editProfilePage) {
                        $host({
                            method: 'PUT',
                            url: 'api/user/profile',
                            params: {
                                userId: $scope.item.UserId
                            },
                            data: $scope.item
                        })(function (res) {
                            $host({
                                method: 'GET',
                                url: 'api/user/profile'
                            })(function (res) {
                                $scope.item = res.data;
                                $scope.item.Gender = $scope.item.Gender ? "Nam" : "Nữ";
                                $scope.item.DateOfBirth = new Date($scope.item.DateOfBirth);
                            }, function (res) {

                            });
                            $scope.profilePage = true;
                            $scope.editProfilePage = false;
                            $scope.editPasswordPage = false;
                        }, function (res) {

                        });
                    } else if ($scope.editPasswordPage) {
                        $host({
                            method: 'PUT',
                            url: 'api/user/password',
                            params: {
                                userId: $scope.item.UserId
                            },
                            data: $scope.item
                        })(function (res) {
                            $scope.profilePage = true;
                            $scope.editProfilePage = false;
                            $scope.editPasswordPage = false;
                        }, function (res) {

                        });
                    }
                }

                $scope.item = {
                    Gender: "true"
                }
            })();
        }
    ]);