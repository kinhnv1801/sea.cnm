﻿'use strict';

angular.module('app.sections.user',
    [
        'ngRoute',
        'ngCookies'
    ])
    .config([
        '$routeProvider', function($routeProvider) {
            $routeProvider
                .when('/login', {
                    templateUrl: '/App/sections/user/login/user.login.html',
                    controller: 'UserLogin'
                }).when('/profile', {
                    templateUrl: '/App/sections/user/profile/user.profile.html',
                    controller: 'UserProfile'
                }).when('/register', {
                    templateUrl: '/App/sections/user/register/user.register.html',
                    controller: 'UserRegister'
                });
        }
    ]);