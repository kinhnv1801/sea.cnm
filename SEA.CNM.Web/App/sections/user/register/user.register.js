﻿'use strict';

angular
    .module('app.sections.user')
    .controller('UserRegister', [
        '$scope', '$host', '$location', '$token', '$cookies', '$routeParams',
        function ($scope, $host, $location, $token, $cookies, $routeParams) {
            (function makeLayout() {
                document.title = 'Đăng ký';
            })();

            (function main() {
                if ($cookies.get('token') == undefined) {
                    $location.path('/login');
                    $location.search('returnPath', '/profile');
                    $location.replace();
                }

                $scope.register = function() {
                    $host({
                        method: 'POST',
                        url: 'api/user',
                        data: $scope.item
                    })(function (res) {
                        $token($scope.item.userName, $scope.item.password)(function (res) {
                            if ($routeParams.returnPath != undefined) {
                                $location.path($routeParams.returnPath);
                                $location.search('returnPath', null);
                            } else {
                                $location.path('/');
                            }
                            $location.replace();
                        });
                    }, function(res) {

                    });
                }
            })();

        }
    ]);