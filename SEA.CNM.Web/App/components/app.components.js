﻿'use strict';

angular
    .module('app.components',
    [
        'angular-click-outside',
        'ngCookies'
    ]);