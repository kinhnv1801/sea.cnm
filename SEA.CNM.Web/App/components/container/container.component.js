﻿'use strict';

angular
    .module('app.components')
    .component('container',
    {
        transclude: true,
        templateUrl: '/App/components/container/container.template.html',
        controller: [
            '$scope',
            function ($scope) {

            }
        ]
    });