﻿'use strict';

angular
    .module('app.components')
    .component('reviewSlider',
    {
        templateUrl: '/App/components/review-slider/review-slider.template.html',
        controller: [
            '$scope',
            function ($scope) {
                $(window).load(function () {

                    $("#flexiselDemo1").flexisel({
                        visibleItems: 6,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: false,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 2
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 3
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 3
                            }
                        }
                    });
                });
            }
        ]
    });