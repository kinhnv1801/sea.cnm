﻿'use strict';

angular
    .module('app.components')
    .component('menu',
    {
        templateUrl: '/App/components/menu/menu.template.html',
        controller: [
            '$scope', '$location', '$cookies', '$rootScope', '$host',
            function ($scope, $location, $cookies, $rootScope, $host) {
                var ctrl = this;

                ctrl.path = $location.path();
                
                $rootScope.$on("$routeChangeSuccess", function() {
                    ctrl.status = {
                        Home: $location.path() === '/' ? 'active' : '',
                        Films: $location.path().indexOf('/films') !== -1 ? 'active' : '',
                        Tickets: $location.path() === '/tickets' ? 'active' : '',
                        Profile: $location.path() === '/profile' ? 'active' : '',
                        Login: $location.path() === '/login' ? 'active' : '',
                        Register: $location.path() === '/register' ? 'active' : ''
                    };

                    $("html, body").animate({ scrollTop: 0 }, "slow");

                    if ($cookies.get('token') != undefined) {
                        $host({
                            method: 'GET',
                            url: 'api/user/profile'
                        })(function(res) {
                            ctrl.profile = res.data;
                            for (var k = 0; k < ctrl.profile.Apis.length; k++) {
                                for (var l = 0; l < ctrl.profile.Apis[k].length; l++) {
                                    if (ctrl.profile.Apis[k][l].indexOf('admin') !== -1) {
                                        ctrl.adminPage = true;
                                    }
                                }
                            }
                        });
                    } else {
                        ctrl.adminPage = false;
                    }
                });

                ctrl.hasCookie = function() {
                    return $cookies.get('token') != undefined;
                }

                ctrl.logout = function() {
                    $cookies.remove('token');
                    if ($location.path() === '/profile' || 
                        $location.path() === '/tickets') {
                        $location.path('/');
                        $location.replace();
                    }
                }
            }
        ]
    });