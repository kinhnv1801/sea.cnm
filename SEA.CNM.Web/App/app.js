'use strict';

angular
    .module('app', [
        'app.components',
        'app.sections',
        'app.filters',
        'ngCookies'
    ]);

angular
    .module('app')
    .factory('$host',
    [
        '$http', '$cookies', '$location',
        function ($http, $cookies, $location) {
            var webApplication = 'http://localhost:52005/';
            var webServer = 'http://localhost:8001/';
            var hostPart = webApplication;
            return function (object) {
                var token = $cookies.get('token');
                return function(successFunc, errorFunc) {
                    object.url = hostPart + object.url;
                    if (token != undefined && token != null) {
                        token = token.replace("\"", "");
                        token = token.replace("\"", "");
                        object.headers = {
                            'Authorization': 'Bearer ' + token
                        }
                    };
                    $http(object).then(function(res) {
                        if (successFunc != undefined) {
                            successFunc(res);
                        }
                    }, function(res) {
                        if (errorFunc != undefined) {
                            errorFunc(res);
                        } else {
                            $location.path('/' + res.status);
                            $location.replace();
                        }
                    });
                }
            }
        }
    ])
    .factory('$token',
    [
        '$http', '$cookies',
        function ($http, $cookies) {
            var webApplication = 'http://localhost:52005/';
            var webServer = 'http://localhost:8001/';
            var hostPart = webApplication;
            return function (userName, password) {
                return function(successFunc, errorFunc) {
                    $http({
                        method: 'POST',
                        url: hostPart + 'token',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {
                            grant_type: 'password',
                            username: userName,
                            password: password
                        }
                    }).then(function (res) {
                        var token = res.data.access_token;
                        $cookies.put('token', token);
                        if (successFunc != undefined)
                            successFunc(res);
                    }, function (res) {
                        if (errorFunc != undefined)
                            errorFunc(res);
                    });
                }
            }
        }
    ]);
