﻿CKEDITOR.plugins.add('timestamp', {
    init: function (editor) {
        // tạo sự kiện
        editor.addCommand('insertTimestamp', {
            exec: function (editor) {
                var now = new Date();
                editor.insertHtml('The current date and time is: <em>' + now.toString() + '</em>');
            }
        });
        // tạo nút
        editor.ui.addButton('Timestamp', { //Tên nút
            label: 'Insert Timestamp', //Hiển thị khi hover vào nút
            command: 'insertTimestamp', //Sự kiên khi ấn
            toolbar: 'insert', //trong toolbar name
            icon: this.path + 'icons/timestamp.png' // ảnh đại diện
        });
    }
});