﻿CKEDITOR.plugins.add('myimage', {
    init: function (editor) {
        editor.addCommand('myimage', new CKEDITOR.dialogCommand('myimage'));
        // tạo nút
        editor.ui.addButton('MyImage', { //Tên nút
            label: 'My Image', //Hiển thị khi hover vào nút\
            command: 'myimage',
            toolbar: 'insert', //trong toolbar name
            icon: this.path + 'icons/myimage.png' // ảnh đại diện
        });

        CKEDITOR.dialog.add('myimage', function (editor) {
            var data = [
                {
                    url: '/App_Data/Image/image1.jpg'
                }, {
                    url: '/App_Data/Image/image2.jpg'
                }, {
                    url: '/App_Data/Image/image3.jpg'
                }, {
                    url: '/App_Data/Image/image4.jpg'
                }, {
                    url: '/App_Data/Image/image5.jpg'
                }
            ];

            var build =
                '<div>' +
                    '<div>' +
                            '<a class="cke_dialog_ui_button cke_dialog_ui_button_ok" style="margin-right: 5px"><span style="color: #fff;font-weight: bold;" class="cke_dialog_ui_button">Upload</span></a>' +
                            '<a class="cke_dialog_ui_button"><span style="font-weight: bold;color: #484848;" class="cke_dialog_ui_button">Xóa</span></a>' +
                    '</div>' +
                    '<div>' +
                        '<table class="table-hover table-bordered" style="margin-left: 0px;">' +
                        '<thead>' +
                            '<tr>' +
                                '<td>Ảnh</td>' +
                                '<td>Thông tin</td>' +
                                '<td>Chọn</td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                            '<tr>' +
                            '<td><a><img src="/Data/Image/image1.jpg" style="height: 40px;margin: 5px;"/></a></td>' +
                            '<td>' +
                                '<ul style="margin: 5px;">' +
                                    '<li>Tên: image1</li>' +
                                    '<li>Ngày: 30/1/2017</li>' +
                                '</ul>' +
                            '</td>' +
                            '<td>' +
                                '<input type="checkbox" />' +
                            '</td>' +
                            '</tr>' +
                        '</tbody>' +
                        '</table>' +
                    '</div>' +
                '</div>';
            return {
                title: 'Quản lý ảnh',
                minWidth: 800,
                minHeight: 300,
                onOk: function() {
                    // "this" is now a CKEDITOR.dialog object.
                    var document = this.getElement().getDocument();
                    // document = CKEDITOR.dom.document
                    var element = document.getById('myDiv');
                    if (element)
                        alert(element.getHtml());
                },
                contents: [
                    {
                        id: 'tab1',
                        label: '',
                        title: '',
                        elements: [
                            {
                                type: 'html',
                                html: build
                            }
                        ]
                    }
                ],
                buttons: [CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton]
            };
        });
    }
})