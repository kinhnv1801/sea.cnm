﻿'use strict';

angular.module('app-admin.components',
[
    'angular-click-outside',
    'ckeditor'
]).value('UcInfo', {
    UcName: ''
});