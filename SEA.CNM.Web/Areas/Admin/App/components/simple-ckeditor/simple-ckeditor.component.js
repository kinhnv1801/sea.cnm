﻿'use strict';

angular
    .module('app-admin.components')
    .component('simpleCkeditor', {
        template: '<div ckeditor="$ctrl.options" ng-model="$ctrl.ngModel" ready="$ctrl.onReady()"></div>',
        bindings: {
            ngModel: '='
        },
        controller: [
            function() {
                var ctrl = this;

                ctrl.options = {
                    language: 'vi',
                    allowedContent: true,
                    entities: false,
                    toolbarGroups: [
                        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
                        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'paragraph'] }
                    ]
                };
            }
        ]
    });