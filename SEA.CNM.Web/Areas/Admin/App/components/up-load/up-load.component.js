﻿'use strict';

angular
    .module('app-admin.components')
    .component('upLoad', {
        template: '<input type="hidden" data-ckfinder ng-model="$ctrl.ngModel">\
                    <a class="btn btn-default" ng-click="$ctrl.upLoad()"><i class="fa fa-upload"></i></a>',
        bindings: {
            ngModel: '='
        },
        controller: [
            function() {
                var ctrl = this;

                ctrl.upLoad = function() {
                    var finder = new CKFinder();
                    ctrl.uploadFile = function(url) {
                        ctrl.ngModel = url;
                        $('input[data-ckfinder]').click();
                    };
                    finder.selectActionFunction = ctrl.uploadFile;
                    finder.popup();
                }
            }
        ]
    });