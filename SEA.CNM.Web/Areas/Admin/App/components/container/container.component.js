﻿'use strict';

angular.module('app-admin.components').component('container', {
    transclude: true,
    templateUrl: '/Areas/Admin/App/components/container/container.template.html',
    controller:
    [
        'UcInfo',
        function (ucInfo, $rootScope) {
            var ctrl = this;
            ctrl.UcName = ucInfo.UcName;
        }
    ]
});