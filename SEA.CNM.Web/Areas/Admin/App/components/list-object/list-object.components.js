﻿'use strict';

angular
    .module('app-admin.components')
    .component('listObject', {
        templateUrl: '/Areas/Admin/App/components/list-object/list-object.template.html',
        bindings: {
            config: '<',
            ngModel: '=',
            ngAdd: '&',
            ngRemove: '&'
        },
        controller: [
            function() {
                var ctrl = this;

                ctrl.initial = undefined;

                ctrl.Add = function() {
                    if (ctrl.ngModel == undefined || ctrl.ngModel.length == undefined) {
                        ctrl.ngModel = [];
                    }
                    var row = {};
                    for (var i = 0; i < ctrl.config.Cols.length; i++) {
                        row[ctrl.config.Cols[i].Value] = ctrl[ctrl.config.Cols[i].Value];
                        ctrl[ctrl.config.Cols[i].Value] = angular.copy(ctrl.initial);
                    }
                    ctrl.ngModel.push(row);
                    setTimeout(function () { ctrl.ngAdd() }, 100);
                };
                ctrl.Remove = function(row) {
                    ctrl.ngModel = ctrl.ngModel.filter(function(e) {
                        return e !== row;
                    });
                    setTimeout(function () { ctrl.ngRemove() }, 100);
                };

                ctrl.displayTitle = function (value, srouce) {
                    var selectItem = "Không xác định";
                    if (srouce.Multiple != undefined && srouce.Multiple === true) {
                        if (value != undefined && value !== {} && value.length !== 0) {
                            return srouce.Items.filter(function (e) {
                                return value.indexOf(e[srouce.Value]) !== -1;
                            }).map(function (e) {
                                return e[srouce.Title];
                            }).join(', ');
                        } else {
                            return selectItem;
                        }
                    } else {
                        if (value != undefined && value !== {}) {
                            return srouce.Items.filter(function (e) {
                                return e[srouce.Value] === value;
                            })[0][srouce.Title];
                        } else {
                            return selectItem;
                        }
                    }
                }
            }
        ]
    });