﻿'use strict';

angular.module('app-admin.components').component('pageBtn', {
    templateUrl: '/Areas/Admin/App/components/page-btn/page-btn.template.html',
    bindings: {
        ngModel: '=',
        pageInfo: '=',
        loadPage:'='
    },
    controller: [
        function () {
            var ctrl = this;

            ctrl.range = function () {
                if (ctrl.pageInfo != undefined) {
                    var input = [];
                    for (var i = ctrl.pageInfo.FirstDisplayPage; i <= ctrl.pageInfo.LastDisplayPage; i++) {
                        input.push(i);
                    }
                    return input;
                } else {
                    return [];
                }
            };

        }
    ]
});