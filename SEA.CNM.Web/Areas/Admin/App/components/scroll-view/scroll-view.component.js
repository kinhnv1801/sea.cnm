﻿'use strict';

angular.module('app-admin.components').component('scrollView', {
    templateUrl: '/Areas/Admin/App/components/scroll-view/scroll-view.template.html',
    controller:
    [
        '$host', '$rootScope',
        function ($host, $rootScope) {
            var ctrl = this;
            ctrl.sidebarMenu = [
                {
                    title: 'Báo cáo',
                    icon: 'fa fa-home',
                    isShown: true,
                    elements: [{
                        controller: 'home',
                        title: 'Báo cáo chung',
                        isCurrentPage: true,
                        isDisplayed: true
                    }]
                }, {
                    title: 'Rạp chiếu',
                    icon: 'fa fa-film',
                    elements: [{
                        controller: 'film',
                        title: 'Phim',
                        api: '(GET) api/admin/films'
                    }, {
                        controller: 'schedule',
                        title: 'Suất chiếu',
                        api: '(GET) api/admin/schedules'
                    }, {
                        controller: 'ticket',
                        title: 'Vé',
                        api: '(GET) api/admin/tickets'
                    }, {
                        controller: 'bill',
                        title: 'Hóa đơn',
                        api: '(GET) api/admin/bills'
                    }]
                }, {
                    title: 'Tài khoản',
                    icon: 'fa fa-users',
                    elements: [{
                        controller: 'role',
                        title: 'Quyền',
                        api: '(GET) api/admin/roles'
                    }, {
                        controller: 'user',
                        title: 'Tài khoản',
                        api: '(GET) api/admin/users'
                    }]
                }, {
                    title: 'Cơ bản',
                    icon: 'fa fa-cogs',
                    elements: [{
                        controller: 'artist',
                        title: 'Nghệ sĩ',
                        api: '(GET) api/admin/artists'
                    }, {
                        controller: 'seat-type',
                        title: 'Loại ghế',
                        api: '(GET) api/admin/seat-types'
                    }, {
                        controller: 'room',
                        title: 'Phòng',
                        api: '(GET) api/admin/rooms'
                    }, {
                        controller: 'field',
                        title: 'Lĩnh vực',
                        api: '(GET) api/admin/fields'
                    }, {
                        controller: 'time',
                        title: 'Giời gian chiếu',
                        api: '(GET) api/admin/times'
                    }]
                }
            ];

            function loadMenu(controller) {
                var setSideMenu = false;

                for (var i = 0; i < ctrl.sidebarMenu.length; i++) {
                    for (var j = 0; j < ctrl.sidebarMenu[i].elements.length; j++) {
                        if (ctrl.sidebarMenu[i].elements[j].controller === controller) {
                            ctrl.sidebarMenu[i].elements[j].isCurrentPage = true;
                            if (setSideMenu === false) {
                                ctrl.sidebarMenu[i].isShown = true;
                                setSideMenu = true;
                            }
                        } else {
                            if (setSideMenu === false)
                                ctrl.sidebarMenu[i].isShown = false;
                            ctrl.sidebarMenu[i].elements[j].isCurrentPage = false;
                        }
                    }
                    setSideMenu = false;
                }
            }

            $rootScope.$on("$routeChangeSuccess", function () {
                loadMenu(document.URL.split('/')[5]);
            });

            ctrl.showOrHide = function (title) {
                for (var k = 0; k < ctrl.sidebarMenu.length; k++) {
                    if (ctrl.sidebarMenu[k].title === title) {
                        ctrl.sidebarMenu[k].isShown = !ctrl.sidebarMenu[k].isShown;
                    } else {
                        ctrl.sidebarMenu[k].isShown = false;
                    }
                }
            }

            ctrl.showParent = function (elements) {
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].isDisplayed === true) {
                        return true;
                    }
                }
                return false;
            }


            $host({
                method: 'GET',
                url: 'api/admin/user/profile'
            })(function (res) {
                ctrl.profile = res.data;
                for (var k = 0; k < ctrl.profile.Apis.length; k++) {
                    for (var l = 0; l < ctrl.profile.Apis[k].length; l++) {
                        
                        for (var i = 0; i < ctrl.sidebarMenu.length; i++) {
                            for (var j = 0; j < ctrl.sidebarMenu[i].elements.length; j++) {

                                if (ctrl.sidebarMenu[i].elements[j].api != undefined) {
                                    if (ctrl.profile.Apis[k][l] === ctrl.sidebarMenu[i].elements[j].api) {
                                        ctrl.sidebarMenu[i].elements[j].isDisplayed = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    ]
});