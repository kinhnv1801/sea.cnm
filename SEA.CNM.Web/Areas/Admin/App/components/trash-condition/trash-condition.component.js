﻿'use strict';

angular.module('app-admin.components').component('trashCondition', {
    template: '<a class="btn btn-default" ng-click="$ctrl.toggle()"><i class="{{$ctrl.iconClass}}"></i></a>',
    bindings: {
        ngModel: '=',
        ngClick: '&'
    },
    controller:
    [
        function() {
            var ctrl = this;

            if (ctrl.ngModel) {
                ctrl.iconClass = 'fa fa-list';
            } else {
                ctrl.iconClass = 'fa fa-trash';
            }

            ctrl.toggle = function() {
                ctrl.ngModel = !ctrl.ngModel;
                if (ctrl.ngModel) {
                    ctrl.iconClass = 'fa fa-list';
                } else {
                    ctrl.iconClass = 'fa fa-trash';
                }
            }
        }
    ]
});