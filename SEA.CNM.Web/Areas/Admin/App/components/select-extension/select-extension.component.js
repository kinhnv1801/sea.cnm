﻿'use strict';

angular.module('app-admin.components').component('selectExtension', {
    templateUrl: '/Areas/Admin/App/components/select-extension/select-extension.template.html',
    bindings: {
        srouce: '<',
        multiple: '=',
        ngModel: '=',
        ngChange: '&',
        searchBox: '='
    },
    controller:
    [
        function ($scope) {
            var ctrl = this;

            if (ctrl.init == undefined) {
                ctrl.init = angular.copy(ctrl.ngModel);
            }
            
            ctrl.showSelect = false;

            ctrl.toggleSelect = function() {
                ctrl.showSelect = !ctrl.showSelect;
            }

            ctrl.selectOrUnselect = function (value) {
                if (ctrl.ngModel == undefined) {
                    ctrl.ngModel = ctrl.multiple ? [] : {};
                }
                if (ctrl.multiple === true) {
                    if (ctrl.ngModel.indexOf(value) !== -1) {
                        ctrl.ngModel = ctrl.ngModel.filter(function (e) {
                            return e !== value;
                        });
                    } else {
                        ctrl.ngModel.push(value);
                    }
                } else {
                    if (ctrl.ngModel == value) {
                        ctrl.ngModel = angular.copy(ctrl.init);
                    } else {
                        ctrl.ngModel = value;
                    }
                    ctrl.showSelect = !ctrl.showSelect;
                }
                setTimeout(function () { ctrl.ngChange(); }, 100);
            }

            ctrl.hideSelect = function() {
                ctrl.showSelect = false;
            }

            ctrl.displayTitle = function () {
                if (ctrl.srouce != undefined) {
                    var selectItem = ctrl.srouce.Placeholder,
                        item = ' lựa chọn';
                    if (!ctrl.multiple) {
                        if (ctrl.ngModel != undefined && ctrl.ngModel !== {}) {
                            if (ctrl.srouce.Items == undefined ||
                                ctrl.srouce.Items.length == undefined ||
                                ctrl.srouce.Items.length === 0 ||
                                ctrl.srouce.Value == undefined ||
                                ctrl.srouce.Title == undefined)
                                return selectItem;
                            var res = ctrl.srouce.Items.filter(function (e) {
                                return e[ctrl.srouce.Value] === ctrl.ngModel;
                            })[0];

                            if (res == undefined || res[ctrl.srouce.Title] == undefined)
                                return selectItem;
                            return res[ctrl.srouce.Title];
                        } else {
                            return selectItem;
                        }
                    } else {
                        if (ctrl.ngModel != undefined && ctrl.ngModel !== {} && ctrl.ngModel.length !== 0 && ctrl.ngModel.length !== undefined) {
                            return ctrl.ngModel.length + item;
                        } else {
                            return selectItem;
                        }
                    }
                }
            }
        }
    ]
});