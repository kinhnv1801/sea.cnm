﻿'use strict';

angular.module('app-admin', [
    'app-admin.components',
    'app-admin.directives',
    'app-admin.filters',
    'app-admin.services',
    'app-admin.sections',
    'ngCookies'
]);

angular.module('app-admin').factory('$host',
[
    '$http', '$cookies', '$location',
    function ($http, $cookies, $location) {
        var webApplication = 'http://localhost:52005/';
        var webServer = 'http://localhost:8001/';

        var hostPart = webApplication;
        return function (object) {
            var token = $cookies.get('token');
            return function (successFunc, errorFunc) {
                object.url = hostPart + object.url;
                if (token != undefined && token != null) {
                    token = token.replace("\"", "");
                    token = token.replace("\"", "");
                    object.headers = {
                        'Authorization': 'Bearer ' + token
                    }
                };
                $http(object).then(function(res) {
                    if (successFunc != undefined) {
                        successFunc(res);
                    }
                }, function(res) {
                    if (errorFunc != undefined) {
                        errorFunc(res, function (response) {
                            switch (response.status) {
                                case 401:
                                    {
                                        alert("Bạn không có quyền đổi trạng thái");
                                        break;
                                    }
                                case 404:
                                    {
                                        alert("Bảng ghi không được tìm thấy");
                                        break;
                                    }
                                case 500:
                                    {
                                        alert("Có lỗi xẩy ra bởi hệ thống khi thực hiện chức năng");
                                        break;
                                    }
                            }
                        });
                    } else {
                        $location.path('/' + res.status);
                        $location.replace();
                    }
                });
            }
        }
    }
]);
