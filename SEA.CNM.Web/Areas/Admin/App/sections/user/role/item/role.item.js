﻿'use strict';

angular
    .module('app-admin.sections.role')
    .controller('RoleItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý quyền';
                document.title = 'Quản lý quyền';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm quyền';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa quyền';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết quyền';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/api-selects'
                })(function (res) {
                    $scope.apiSrouce = {
                        Items: res.data,
                        Value: 'Value',
                        Title: 'Title',
                        Placeholder: 'Chọn api',
                        Multiple: true
                    };
                }, function (res) { });
                
                $host({
                    method: 'GET',
                    url: 'api/static/role-types'
                })(function (res) {
                    $scope.roleTypeSrouce = {
                        Items: res.data,
                        Value: 'Value',
                        Title: 'Title',
                        Placeholder: 'Chọn loại quyền'
                    };
                }, function (res) { });
            })();
            
            (function main() {
                if ($scope.createPage) {
                    $scope.item = {};

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/roles',
                            data: $scope.item
                        })(function (res) {
                            $location.path('/role/index');
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                } else if ($scope.editPage || $scope.detailsPage) {
                    var roleId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/roles',
                        params: {
                            id: roleId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/roles',
                            params: {
                                id: roleId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/role/details/' + roleId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/roles',
                                params: {
                                    id: roleId
                                }
                            })(function (res) {
                                $location.path('/role/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                };
            })();
        }
    ]);