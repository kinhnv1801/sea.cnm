﻿'use strict';

angular.module('app-admin.sections.role', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/role/index', {
                templateUrl: '/Areas/Admin/App/sections/user/role/index/role.index.html',
                controller: 'RoleIndex'
            }).when('/role/create', {
                templateUrl: '/Areas/Admin/App/sections/user/role/item/role.item.html',
                controller: 'RoleItem'
            }).when('/role/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/user/role/item/role.item.html',
                controller: 'RoleItem'
            }).when('/role/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/user/role/item/role.item.html',
                controller: 'RoleItem'
            });
    }
    ]);