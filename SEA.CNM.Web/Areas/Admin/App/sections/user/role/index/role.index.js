﻿'use strict';

angular
    .module('app-admin.sections.role')
    .controller('RoleIndex',
    [
        '$scope', '$host', 'UcInfo',
        function ($scope, $host, ucInfo) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý quyền';
                document.title = 'Quản lý quyền';
                $scope.action = 'Danh sách quyền';
                $scope.actionIcon = 'fa fa-list';
            })();

            (function main() {
                $scope.filter = {
                    SearchText: '',
                    IsDeleted: false,
                    Page: 1,
                    Take: 10
                }

                $scope.loadPage = function (page) {
                    if (page == undefined) {
                        $scope.filter.Page = 1;
                    } else {
                        $scope.filter.Page = page;
                    }

                    $host({
                        method: 'GET',
                        params: $scope.filter,
                        url: 'api/admin/roles'
                    })(function (res) {
                        $scope.items = res.data.Items;
                        $scope.pageInfo = res.data.PageInfo;
                    });
                }

                $scope.loadPage();

                $scope.changeStatus = function (id) {
                    $host({
                        method: 'GET',
                        url: 'api/admin/roles/changed-status',
                        params: {
                            id: id
                        }
                    })(function (res) {
                        if (res.data === false) {
                            alert('Bạn không thể đổi trạng thái bảng ghi');
                        } else {
                            if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                                $host({
                                    method: 'PUT',
                                    params: {
                                        id: id
                                    },
                                    url: 'api/admin/roles/status'
                                })(function (res) {
                                    $scope.loadPage();
                                }, function (res, exec1) {
                                    exec1(res);
                                });
                            }
                        }
                    }, function (res, exec1) {
                        exec1(res);
                    });
                }
            })();
        }
    ]);