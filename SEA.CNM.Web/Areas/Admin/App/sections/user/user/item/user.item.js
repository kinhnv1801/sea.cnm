﻿'use strict';

angular
    .module('app-admin.sections.user')
    .controller('UserItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeSrouce() {
                ucInfo.UcName = 'Quản lý tài khoản';
                document.title = 'Quản lý tài khoản';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm tài khoản';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa tài khoản';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết tài khoản';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/role-selects'
                })(function (res) {
                    $scope.roleSrouce = {
                        Items: res.data,
                        Value: 'RoleId',
                        Title: 'Name',
                        Multiple: true,
                        Placeholder: 'Chọn quyền'
                    };
                }, function (res) {
                });

                $host({
                    method: 'GET',
                    url: 'api/static/genders'
                })(function (res) {
                    $scope.genderSrouce = {
                        Items: res.data,
                        Value: 'Value',
                        Title: 'Title',
                        Placeholder: 'Chọn giới tính'
                    };
                }, function (res) {
                });

            })();

            (function main() {
                if ($scope.createPage) {
                    $scope.item = {};

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/users',
                            data: $scope.item
                        })(function (res) {
                            $location.path('/user/index');
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                } else if ($scope.editPage || $scope.detailsPage) {
                    var userId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/users',
                        params: {
                            id: userId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                        $scope.item.DateOfBirth = new Date($scope.item.DateOfBirth);
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/users',
                            params: {
                                id: userId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/user/details/' + userId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/users',
                                params: {
                                    id: userId
                                }
                            })(function (res) {
                                $location.path('/user/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                };
            })();

        }
    ]);