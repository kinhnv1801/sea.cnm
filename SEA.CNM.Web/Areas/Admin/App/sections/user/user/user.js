﻿'use strict';

angular.module('app-admin.sections.user', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/user/index', {
                templateUrl: '/Areas/Admin/App/sections/user/user/index/user.index.html',
                controller: 'UserIndex'
            }).when('/user/create', {
                templateUrl: '/Areas/Admin/App/sections/user/user/item/user.item.html',
                controller: 'UserItem'
            }).when('/user/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/user/user/item/user.item.html',
                controller: 'UserItem'
            }).when('/user/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/user/user/item/user.item.html',
                controller: 'UserItem'
            });
    }
    ]);