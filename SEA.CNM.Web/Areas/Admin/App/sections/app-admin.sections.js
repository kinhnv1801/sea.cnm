﻿'use strict';

angular.module('app-admin.sections',
    [
        'ngRoute',
        'app-admin.sections.home',
        'app-admin.sections.artist',
        'app-admin.sections.field',
        'app-admin.sections.room',
        'app-admin.sections.seat-type',
        'app-admin.sections.time',
        'app-admin.sections.role',
        'app-admin.sections.user',
        'app-admin.sections.bill',
        'app-admin.sections.film',
        'app-admin.sections.schedule',
        'app-admin.sections.ticket',
        'app-admin.sections.error'
    ])
    .config([
        '$locationProvider', '$routeProvider',
        function ($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({ redirectTo: '/home/index' });
        }
    ]);