﻿'use strict';

angular.module('app-admin.sections.home', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
            $routeProvider.when('/home/index', {
                templateUrl: '/Areas/Admin/App/sections/report/home/home.html',
                controller: 'Home'
            });
        }
    ])
    .controller('Home',
    [
        '$scope',
        function ($scope) {
            $scope.saleReport = "http://localhost/ReportServer/Pages/ReportViewer.aspx?%2fSEA.CNM.Reports%2fBillReport&rs:Command=Render";
        }
    ]);