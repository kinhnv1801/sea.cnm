﻿'use strict';

angular.module('app-admin.sections.error', ['ngRoute'])
    .config([
        '$routeProvider', function ($routeProvider) {
            $routeProvider
                .when('/404', {
                    templateUrl: '/Areas/Admin/App/sections/error/404/error.404.html',
                    controller: 'Error404'
                }).when('/401', {
                    templateUrl: '/Areas/Admin/App/sections/error/401/error.401.html',
                    controller: 'Error401'
                }).when('/400', {
                    templateUrl: '/Areas/Admin/App/sections/error/400/error.400.html',
                    controller: 'Error400'
                }).when('/403', {
                    templateUrl: '/Areas/Admin/App/sections/error/403/error.403.html',
                    controller: 'Error403'
                });
        }
    ]);