﻿'use strict';

angular
    .module('app-admin.sections.ticket')
    .controller('TicketItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý vé';
                document.title = 'Quản lý vé';

                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm vé';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa vé';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết vé';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {

                $scope.loadTimeAndSeatType = function () {
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $scope.item.RoomId
                        },
                        url: 'api/admin/schedule/time-selects'
                    })(function (res) {
                        $scope.timeSrouce = {
                            Items: res.data,
                            Title: 'Title',
                            Value: 'TimeId',
                            Placeholder: 'Chọn giờ chiếu'
                        };
                    }, function (res) { });

                    $host({
                        method: 'GET',
                        params: {
                            roomId: $scope.item.RoomId
                        },
                        url: 'api/admin/room/seat-type-selects'
                    })(function (res) {
                        $scope.seatTypeSrouce = {
                            Items: res.data,
                            Title: 'Name',
                            Value: 'SeatTypeId',
                            Placeholder: 'Chọn loại ghế'
                        };
                    }, function (res) { });
                }

                $scope.loadDate = function () {
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $scope.item.RoomId,
                            timeId: $scope.item.TimeId
                        },
                        url: 'api/admin/schedule/date-selects'
                    })(function (res) {
                        $scope.dateSrouce = {
                            Items: res.data,
                            Title: 'Title',
                            Value: 'Value',
                            Placeholder: 'Chọn ngày chiếu'
                        };
                    }, function (res) { });
                }

                $scope.loadFilm = function () {
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $scope.item.RoomId,
                            timeId: $scope.item.TimeId,
                            date: $scope.item.Date
                        },
                        url: 'api/admin/schedule/film-selects'
                    })(function (res) {
                        $scope.filmSrouce = {
                            Items: res.data,
                            Title: 'Name',
                            Value: 'FilmId',
                            Placeholder: 'Chọn phim'
                        };
                    }, function (res) { });
                }

                $host({
                    method: 'GET',
                    url: 'api/admin/room-selects'
                })(function (res) {
                    $scope.roomSrouce = {
                        Items: res.data,
                        Title: 'Code',
                        Value: 'RoomId',
                        Placeholder: 'Chọn phòng'
                    };
                }, function (res) { });

                if (($scope.item != undefined &&
                    $scope.item.RoomId != undefined) || 
                    $routeParams.roomId != undefined) {
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $routeParams.roomId != undefined ? $routeParams.roomId : $scope.item.RoomId
                        },
                        url: 'api/admin/schedule/time-selects'
                    })(function (res) {
                        $scope.timeSrouce = {
                            Items: res.data,
                            Title: 'Title',
                            Value: 'TimeId',
                            Placeholder: 'Chọn giờ chiếu'
                        };
                    }, function (res) { });

                    $host({
                        method: 'GET',
                        params: {
                            roomId: $routeParams.roomId != undefined ? $routeParams.roomId : $scope.item.RoomId
                        },
                        url: 'api/admin/room/seat-type-selects'
                    })(function (res) {
                        $scope.seatTypeSrouce = {
                            Items: res.data,
                            Title: 'Name',
                            Value: 'SeatTypeId',
                            Placeholder: 'Chọn loại ghế'
                        };
                    }, function (res) { });
                } else {
                    $scope.timeSrouce = {
                        Items: [],
                        Title: 'Title',
                        Value: 'TimeId',
                        Placeholder: 'Chọn giờ chiếu'
                    };

                    $scope.seatTypeSrouce = {
                        Items: [],
                        Title: 'Name',
                        Value: 'SeatTypeId',
                        Placeholder: 'Chọn loại ghế'
                    };
                }

                if (($scope.item != undefined &&
                    $scope.item.RoomId != undefined &&
                    $scope.item.TimeId != undefined) || 
                    ($routeParams.roomId != undefined &&
                    $routeParams.roomId != undefined)) {
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $routeParams.roomId != undefined ? $routeParams.roomId : $scope.item.RoomId,
                            timeId: $routeParams.timeId != undefined ? $routeParams.timeId : $scope.item.TimeId
                        },
                        url: 'api/admin/schedule/date-selects'
                    })(function (res) {
                        $scope.dateSrouce = {
                            Items: res.data,
                            Title: 'Title',
                            Value: 'Value',
                            Placeholder: 'Chọn ngày chiếu'
                        };
                    }, function (res) { });
                } else {
                    $scope.dateSrouce = {
                        Items: [],
                        Title: 'Title',
                        Value: 'Value',
                        Placeholder: 'Chọn ngày chiếu'
                    };
                }

                if (($scope.item != undefined &&
                    $scope.item.RoomId != undefined &&
                    $scope.item.TimeId != undefined &&
                    $scope.item.Date != undefined) ||
                    ($routeParams.roomId != undefined &&
                    $routeParams.roomId != undefined &&
                    $routeParams.date != undefined)) {
                    
                    $host({
                        method: 'GET',
                        params: {
                            roomId: $routeParams.roomId != undefined ? $routeParams.roomId : $scope.item.RoomId,
                            timeId: $routeParams.timeId != undefined ? $routeParams.timeId : $scope.item.TimeId,
                            date: $routeParams.date != undefined ? new Date($routeParams.date) : $scope.item.Date
                        },
                        url: 'api/admin/schedule/film-selects'
                    })(function (res) {
                        $scope.filmSrouce = {
                            Items: res.data,
                            Title: 'Name',
                            Value: 'FilmId',
                            Placeholder: 'Chọn phim'
                        };
                    }, function (res) { });
                } else {
                    $scope.filmSrouce = {
                        Items: [],
                        Title: 'Name',
                        Value: 'FilmId',
                        Placeholder: 'Chọn phim'
                    };
                }
            })();

            (function main() {
                if ($routeParams.page === 'calendar') {
                    $scope.page = 'calendar';
                }
                if ($scope.detailsPage || $scope.editPage) {
                    var ticketId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/tickets',
                        params: {
                            id: ticketId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                        $scope.item.Date = new Date(res.data.Date);
                        $scope.item.PublishedAt = new Date(res.data.PublishedAt);
                        $scope.loadTimeAndSeatType();
                        $scope.loadDate();
                        $scope.loadFilm();
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/tickets',
                            params: {
                                id: ticketId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/ticket/details/' + ticketId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/tickets',
                                params: {
                                    id: ticketId
                                }
                            })(function (res) {
                                $location.path('/ticket/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                } else if ($scope.createPage) {
                    if ($routeParams.timeId != undefined) {
                        $scope.page = 'calendar';
                    }
                    $scope.item = {
                        TimeId: parseInt($routeParams.timeId),
                        FilmId: parseInt($routeParams.filmId),
                        RoomId: parseInt($routeParams.roomId),
                        Date: new Date($routeParams.date)
                    };

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/tickets',
                            data: $scope.item
                        })(function (res) {
                            if ($scope.page === 'calendar') {
                                $location.url('/ticket/calendar');
                            } else {
                                $location.path('/ticket/index');
                            }
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }
                }
            })();
        }
    ]);