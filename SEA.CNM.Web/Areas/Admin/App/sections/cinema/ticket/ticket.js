﻿'use strict';

angular.module('app-admin.sections.ticket', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/ticket/index', {
                templateUrl: '/Areas/Admin/App/sections/cinema/ticket/index/ticket.index.html',
                controller: 'TicketIndex'
            }).when('/ticket/calendar', {
                templateUrl: '/Areas/Admin/App/sections/cinema/ticket/index/ticket.index.html',
                controller: 'TicketIndex'
            }).when('/ticket/create', {
                templateUrl: '/Areas/Admin/App/sections/cinema/ticket/item/ticket.item.html',
                controller: 'TicketItem'
            }).when('/ticket/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/ticket/item/ticket.item.html',
                controller: 'TicketItem'
            }).when('/ticket/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/ticket/item/ticket.item.html',
                controller: 'TicketItem'
            });
    }
    ]);