﻿'use strict';

angular
    .module('app-admin.sections.ticket')
    .controller('TicketIndex',
    [
        '$scope', '$host', 'UcInfo',
        function ($scope, $host, ucInfo) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý vé';
                document.title = 'Quản lý vé';
                if (window.location.href.indexOf('index') !== -1) {
                    $scope.indexPage = true;
                    $scope.action = 'Danh sách vé';
                    $scope.actionIcon = 'fa fa-list';
                } else if (window.location.href.indexOf('calendar') !== -1) {
                    $scope.calendarPage = true;
                    $scope.action = 'Bảng vé';
                    $scope.actionIcon = 'fa fa-calendar';
                }
            })();

            (function makeSrouce() {
                (function (makeSrouceIndex, makeSrouceCalendar) {
                    if ($scope.indexPage && makeSrouceIndex != undefined) {
                        makeSrouceIndex();
                    } else if ($scope.calendarPage && makeSrouceCalendar != undefined) {
                        makeSrouceCalendar();
                    }
                })(
                    function () {
                        $host({
                            method: 'GET',
                            url: 'api/admin/film-selects'
                        })(function (res) {
                            $scope.filmSrouce = {
                                Items: res.data,
                                Title: 'Name',
                                Value: 'FilmId',
                                Placeholder: 'Chọn phim'
                            };
                        }, function (res) { });

                        $host({
                            method: 'GET',
                            url: 'api/admin/room-selects'
                        })(function (res) {
                            $scope.roomSrouce = {
                                Items: res.data,
                                Title: 'Code',
                                Value: 'RoomId',
                                Placeholder: 'Chọn phòng'
                            };
                        }, function (res) { });

                        $host({
                            method: 'GET',
                            url: 'api/admin/time-selects'
                        })(function (res) {
                            $scope.timeSrouce = {
                                Items: res.data,
                                Title: 'Title',
                                Value: 'TimeId',
                                Placeholder: 'Chọn giờ chiếu'
                            };
                        }, function (res) { });


                        $host({
                            method: 'GET',
                            url: 'api/admin/seat-type-selects'
                        })(function (res) {
                            $scope.seatTypeSrouce = {
                                Items: res.data,
                                Title: 'Name',
                                Value: 'SeatTypeId',
                                Placeholder: 'Chọn loại ghế'
                            };
                        }, function (res) { });
                    },
                    function () {
                        $host({
                            method: 'GET',
                            url: 'api/admin/room-selects'
                        })(function (res) {
                            $scope.roomSrouce = {
                                Items: res.data,
                                Title: 'Code',
                                Value: 'RoomId',
                                Placeholder: 'Chọn phòng'
                            };
                        }, function (res) {
                        });
                    });
            })();

            (function main() {
                (function (mainIndex, mainCalendar) {

                    if ($scope.indexPage && mainIndex != undefined) {
                        mainIndex();
                    } else if ($scope.calendarPage && mainCalendar != undefined) {
                        mainCalendar();
                    }

                    $scope.changeStatus = function (id) {
                        $host({
                            method: 'GET',
                            url: 'api/admin/tickets/changed-status',
                            params: {
                                id: id
                            }
                        })(function (res) {
                            if (res.data === false) {
                                alert('Bạn không thể đổi trạng thái bảng ghi');
                            } else {
                                if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                                    $host({
                                        method: 'PUT',
                                        params: {
                                            id: id
                                        },
                                        url: 'api/admin/tickets/status'
                                    })(function (res) {
                                        $scope.loadPage();
                                    }, function (res, exec1) {
                                        exec1(res);
                                    });
                                }
                            }
                        }, function (res, exec1) {
                            exec1(res);
                        });
                    }
                })(
                    function () {
                        $scope.filter = {
                            IsDeleted: false,
                            Page: 1,
                            Take: 10
                        }

                        $scope.loadPage = function (page) {
                            if (page == undefined) {
                                $scope.filter.Page = 1;
                            } else {
                                $scope.filter.Page = page;
                            }

                            $host({
                                method: 'GET',
                                params: $scope.filter,
                                url: 'api/admin/tickets'
                            })(function (res) {
                                $scope.items = res.data.Items;
                                $scope.pageInfo = res.data.PageInfo;
                            });
                        }

                        $scope.loadPage();
                    },
                    function () {
                        $scope.filter = {
                            BeginDate: ""
                        }

                        $scope.loadPage = function (page) {
                            $host({
                                method: 'GET',
                                params: $scope.filter,
                                url: 'api/admin/ticket-calendar'
                            })(function (res) {
                                $scope.items = res.data;
                            });
                        }

                        $scope.loadPage();
                    });
            })();

            (function main() {
                
            })();
        }
    ]);