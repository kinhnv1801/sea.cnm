﻿'use strict';

angular
    .module('app-admin.sections.schedule')
    .controller('ScheduleItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý suất chiếu';
                document.title = 'Quản lý suất chiếu';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm suất chiếu';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa suất chiếu';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết suất chiếu';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/film-selects'
                })(function (res) {
                    $scope.filmSrouce = {
                        Items: res.data,
                        Title: 'Name',
                        Value: 'FilmId',
                        Multiple: false,
                        Placeholder: 'Chọn phim'
                    };
                }, function (res) {
                });

                $host({
                    method: 'GET',
                    url: 'api/admin/room-selects'
                })(function (res) {
                    $scope.roomSrouce = {
                        Items: res.data,
                        Title: 'Code',
                        Value: 'RoomId',
                        Multiple: false,
                        Placeholder: 'Chọn phòng'
                    };
                }, function (res) {
                });

                $host({
                    method: 'GET',
                    url: 'api/admin/time-selects'
                })(function (res) {
                    $scope.timeSrouce = {
                        Items: res.data,
                        Title: 'Title',
                        Value: 'TimeId',
                        Multiple: false,
                        Placeholder: 'Chọn giờ chiếu'
                    };
                }, function (res) {
                });
            })();

            (function main() {
                if ($scope.detailsPage || $scope.editPage) {
                    var scheduleId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/schedules',
                        params: {
                            id: scheduleId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                        $scope.item.Date = new Date(res.data.Date);
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/schedules',
                            params: {
                                id: scheduleId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/schedule/details/' + scheduleId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/schedules',
                                params: {
                                    id: scheduleId
                                }
                            })(function (res) {
                                $location.path('/schedule/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                } else if ($scope.createPage) {
                    $scope.item = {
                        Date: new Date($routeParams.date),
                        TimeId: parseInt($routeParams.timeId),
                        RoomId: parseInt($routeParams.roomId)
                    };

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/schedules',
                            params: {
                                id: scheduleId
                            },
                            data: $scope.item
                        })(function (res) {
                            if ($routeParams.page === 'calendar') {
                                $location.path('/schedule/calendar');
                            } else {
                                $location.path('/schedule/index');
                            }
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }
                }
            })();
        }
    ]);