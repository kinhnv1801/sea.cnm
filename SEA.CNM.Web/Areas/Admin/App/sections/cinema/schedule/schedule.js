﻿'use strict';

angular.module('app-admin.sections.schedule', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/schedule/index', {
                templateUrl: '/Areas/Admin/App/sections/cinema/schedule/index/schedule.index.html',
                controller: 'ScheduleIndex'
            }).when('/schedule/calendar', {
                templateUrl: '/Areas/Admin/App/sections/cinema/schedule/index/schedule.index.html',
                controller: 'ScheduleIndex'
            }).when('/schedule/create', {
                templateUrl: '/Areas/Admin/App/sections/cinema/schedule/item/schedule.item.html',
                controller: 'ScheduleItem'
            }).when('/schedule/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/schedule/item/schedule.item.html',
                controller: 'ScheduleItem'
            }).when('/schedule/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/schedule/item/schedule.item.html',
                controller: 'ScheduleItem'
            });
    }
    ]);