﻿'use strict';

angular.module('app-admin.sections.bill', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/bill/index', {
                templateUrl: '/Areas/Admin/App/sections/cinema/bill/index/bill.index.html',
                controller: 'BillIndex'
            }).when('/bill/create', {
                templateUrl: '/Areas/Admin/App/sections/cinema/bill/item/bill.item.html',
                controller: 'BillItem'
            }).when('/bill/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/bill/item/bill.item.html',
                controller: 'BillItem'
            }).when('/bill/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/bill/item/bill.item.html',
                controller: 'BillItem'
            });
    }
    ]);