﻿'use strict';

angular
    .module('app-admin.sections.bill')
    .controller('BillIndex',
    [
        '$scope', '$host', 'UcInfo',
        function ($scope, $host, ucInfo) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý hóa đơn';
                document.title = 'Quản lý hóa đơn';
                $scope.action = 'Danh sách hóa đơn';
                $scope.actionIcon = 'fa fa-list';
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/ticket-selects'
                })(function (res) {
                    $scope.ticketSrouce = {
                        Items: res.data,
                        Title: 'Code',
                        Value: 'TicketId',
                        Multiple: true,
                        Placeholder: 'Chọn mã vé'
                    };
                }, function (res) { });
            })();

            (function main() {
                $scope.filter = {
                    SearchText: '',
                    IsDeleted: false,
                    Page: 1,
                    Take: 10
                };

                $scope.loadPage = function (page) {
                    if (page == undefined) {
                        $scope.filter.Page = 1;
                    } else {
                        $scope.filter.Page = page;
                    }

                    $host({
                        method: 'GET',
                        params: $scope.filter,
                        url: 'api/admin/bills'
                    })(function (res) {
                        $scope.items = res.data.Items;
                        $scope.pageInfo = res.data.PageInfo;
                    });
                }

                $scope.loadPage();

                $scope.changeStatus = function (id) {
                    $host({
                        method: 'GET',
                        url: 'api/admin/bills/changed-status',
                        params: {
                            id: id
                        }
                    })(function (res) {
                        if (res.data === false) {
                            alert('Bạn không thể đổi trạng thái bảng ghi');
                        } else {
                            if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                                $host({
                                    method: 'PUT',
                                    params: {
                                        id: id
                                    },
                                    url: 'api/admin/bills/status'
                                })(function (res) {
                                    $scope.loadPage();
                                }, function (res, exec1) {
                                    exec1(res);
                                });
                            }
                        }
                    }, function (res, exec1) {
                        exec1(res);
                    });
                }
            })();
        }
    ]);