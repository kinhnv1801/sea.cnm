﻿'use strict';

angular
    .module('app-admin.sections.bill')
    .controller('BillItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý hóa đơn';
                document.title = 'Quản lý hóa đơn';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm hóa đơn';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa hóa đơn';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết hóa đơn';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/ticket-selects'
                })(function(res) {
                    $scope.ticketSrouce = {
                        Items: res.data,
                        Value: 'TicketId',
                        Title: 'Code',
                        Placeholder: 'Chọn mã vé'
                    };
                    $scope.ticketConfig = {
                        Cols: [
                            {
                                Value: 'TicketId',
                                Title: 'Mã vé',
                                Type: 'Select',
                                Srouce: $scope.ticketSrouce,
                                OnChange: function() {
                                }
                            }, {
                                Value: 'Quantity',
                                Title: 'Số lượng',
                                Type: 'Number',
                                Placeholder: 'Nhập số vé'
                            }
                        ]
                    }
                }, function(res) {});

                $host({
                    method: 'GET',
                    url: 'api/admin/user-selects'
                })(function(res) {
                    $scope.userSrouce = {
                        Items: res.data,
                        Value: 'UserId',
                        Title: 'UserName',
                        Placeholder: 'Chọn tài khoản'
                    };
                }, function(res) {});
            })();

            (function main() {
                $scope.getPrice = function() {
                    $host({
                        method: 'GET',
                        url: 'api/admin/bills/price',
                        params: {
                            ticketsJson: JSON.stringify($scope.item.Tickets)
                        }
                    })(function(res) {
                        $scope.item.Price = res.data;
                    }, function(res) {

                    });
                };

                if ($scope.detailsPage || $scope.editPage) {
                    var billId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/bills',
                        params: {
                            id: billId
                        }
                    })(function(res) {
                        $scope.item = res.data;
                        $scope.item.CreatedAt = new Date(res.data.CreatedAt);
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/bills',
                            params: {
                                id: billId
                            },
                            data: $scope.item
                        })(function(res) {
                            $location.path('/bill/details/' + billId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/bills',
                                params: {
                                    id: billId
                                }
                            })(function(res) {
                                $location.path('/bill/index');
                                $location.replace();
                            }, function(res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                } else if ($scope.createPage) {
                    $scope.item = {};

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/bills',
                            params: {
                                id: billId
                            },
                            data: $scope.item
                        })(function(res) {
                            $location.path('/bill/index');
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }
                }
            })();
        }
    ]);