﻿'use strict';

angular.module('app-admin.sections.film', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/film/index', {
                templateUrl: '/Areas/Admin/App/sections/cinema/film/index/film.index.html',
                controller: 'FilmIndex'
            }).when('/film/create', {
                templateUrl: '/Areas/Admin/App/sections/cinema/film/item/film.item.html',
                controller: 'FilmItem'
            }).when('/film/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/film/item/film.item.html',
                controller: 'FilmItem'
            }).when('/film/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/cinema/film/item/film.item.html',
                controller: 'FilmItem'
            });
    }
    ]);