﻿'use strict';

angular
    .module('app-admin.sections.film')
    .controller('FilmItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý phim';
                document.title = 'Quản lý phim';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm phim';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa phim';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết phim';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/artist-selects'
                })(function (res) {
                    $scope.artistSrouce = {
                        Items: res.data,
                        Title: 'FullName',
                        Value: 'ArtistId',
                        Multiple: true,
                        Placeholder: 'Chọn nghệ sĩ'
                    };
                }, function (res) {
                });

                $host({
                    method: 'GET',
                    url: 'api/admin/field-selects'
                })(function (res) {
                    $scope.fieldSrouce = {
                        Items: res.data,
                        Value: 'FieldId',
                        Title: 'Name',
                        Multiple: true,
                        Placeholder: 'Chọn lĩnh vực'
                    };
                }, function (res) {
                });
            })();

            (function main() {
                if ($scope.detailsPage || $scope.editPage) {
                    var fieldId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/films',
                        params: {
                            id: fieldId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                        $scope.item.CreatedAt = new Date(res.data.CreatedAt);
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/films',
                            params: {
                                id: fieldId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/film/details/' + fieldId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/films',
                                params: {
                                    id: fieldId
                                }
                            })(function (res) {
                                $location.path('/film/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                } else if ($scope.createPage) {
                    $scope.item = {};

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/films',
                            params: {
                                id: fieldId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/film/index');
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }
                }
            })();
        }
    ]);