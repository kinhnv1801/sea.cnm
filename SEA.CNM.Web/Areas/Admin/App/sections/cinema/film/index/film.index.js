﻿'use strict';

angular
    .module('app-admin.sections.film')
    .controller('FilmIndex',
    [
        '$scope', '$host', 'UcInfo',
        function ($scope, $host, ucInfo) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý phim';
                document.title = 'Quản lý phim';
                $scope.action = 'Danh sách phim';
                $scope.actionIcon = 'fa fa-list';
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/artist-selects'
                })(function(res) {
                    $scope.actorSrouce = {
                        Items: res.data,
                        Title: 'FullName',
                        Value: 'ArtistId',
                        Multiple: true,
                        Placeholder: 'Chọn diễn viên'
                    };

                    $scope.directorSrouce = {
                        Items: res.data,
                        Title: 'FullName',
                        Value: 'ArtistId',
                        Multiple: true,
                        Placeholder: 'Chọn đạo diễn'
                    };
                }, function(res) {});

                $host({
                    method: 'GET',
                    url: 'api/admin/field-selects'
                })(function(res) {
                    $scope.fieldSrouce = {
                        Items: res.data,
                        Value: 'FieldId',
                        Title: 'Name',
                        Multiple: true,
                        Placeholder: 'Chọn lĩnh vực'
                    };
                }, function(res) {});
            })();

            (function main() {
                $scope.filter = {
                    SearchText: '',
                    IsDeleted: false,
                    Page: 1,
                    Take: 10
                }

                $scope.loadPage = function (page) {
                    if (page == undefined) {
                        $scope.filter.Page = 1;
                    } else {
                        $scope.filter.Page = page;
                    }

                    $host({
                        method: 'GET',
                        params: $scope.filter,
                        url: 'api/admin/films'
                    })(function (res) {
                        $scope.items = res.data.Items;
                        $scope.pageInfo = res.data.PageInfo;
                    });
                }

                $scope.loadPage();

                $scope.changeStatus = function (id) {
                    $host({
                        method: 'GET',
                        url: 'api/admin/films/changed-status',
                        params: {
                            id: id
                        }
                    })(function (res) {
                        if (res.data === false) {
                            alert('Bạn không thể đổi trạng thái bảng ghi');
                        } else {
                            if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                                $host({
                                    method: 'PUT',
                                    params: {
                                        id: id
                                    },
                                    url: 'api/admin/films/status'
                                })(function (res) {
                                    $scope.loadPage();
                                }, function (res, exec1) {
                                    exec1(res);
                                });
                            }
                        }
                    }, function (res, exec1) {
                        exec1(res);
                    });
                }
            })();
        }
    ]);