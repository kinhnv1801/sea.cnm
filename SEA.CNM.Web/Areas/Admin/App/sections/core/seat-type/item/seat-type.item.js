﻿angular.module('app-admin.sections.seat-type').controller('SeatTypeItem',
[
    '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
    function ($scope, $http, ucInfo, $host, $routeParams, $location) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý loại ghế';
            document.title = 'Quản lý loại ghế';

            if (window.location.href.indexOf('create') !== -1) {
                $scope.createPage = true;
                $scope.action = 'Thêm loại ghế';
                $scope.actionIcon = 'fa fa-plus';
            } else if (window.location.href.indexOf('edit') !== -1) {
                $scope.editPage = true;
                $scope.action = 'Sửa loại ghế';
                $scope.actionIcon = 'fa fa-edit';
            } else if (window.location.href.indexOf('details') !== -1) {
                $scope.detailsPage = true;
                $scope.action = 'Xem chi tiết loại ghế';
                $scope.actionIcon = 'fa fa-eye';
            }
        })();

        (function main() {
            if ($scope.detailsPage || $scope.editPage) {
                var seatTypeId = $routeParams.id;
                $host({
                    method: 'GET',
                    url: 'api/admin/seat-types',
                    params: {
                        id: seatTypeId
                    }
                })(function (res) {
                    $scope.item = res.data;
                });

                $scope.save = function () {
                    $host({
                        method: 'PUT',
                        url: 'api/admin/seat-types',
                        params: {
                            id: seatTypeId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/seat-type/details/' + seatTypeId);
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }

                $scope.delete = function () {
                    if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                        $host({
                            method: 'DELETE',
                            url: 'api/admin/seat-types',
                            params: {
                                id: seatTypeId
                            }
                        })(function (res) {
                            $location.path('/seat-type/index');
                            $location.replace();
                        }, function (res, exec1) {
                            exec1(res);
                        });
                    }
                }
            } else if ($scope.createPage) {
                $scope.item = {};

                $scope.save = function () {
                    $host({
                        method: 'POST',
                        url: 'api/admin/seat-types',
                        data: $scope.item
                    })(function (res) {
                        $location.path('/seat-type/index');
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }
            }
        })();
    }
]);