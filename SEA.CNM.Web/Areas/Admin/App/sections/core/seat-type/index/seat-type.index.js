﻿'use strict';

angular.module('app-admin.sections.seat-type').controller('SeatTypeIndex',
[
    '$scope', '$host', 'UcInfo',
    function ($scope, $host, ucInfo) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý loại ghế';
            document.title = 'Quản lý loại ghế';
            $scope.action = 'Danh sách loại ghế';
            $scope.filter = {
                SearchText: '',
                IsDeleted: false,
                Page: 1,
                Take: 10
            }
        })();

        (function main() {
            $scope.filter = {
                SearchText: '',
                IsDeleted: false,
                SeatTypeIds: [],
                Page: 1,
                Take: 10
            }

            $scope.loadPage = function (page) {
                if (page == undefined) {
                    $scope.filter.Page = 1;
                } else {
                    $scope.filter.Page = page;
                }

                $host({
                    method: 'GET',
                    params: $scope.filter,
                    url: 'api/admin/seat-types'
                })(function (res) {
                    $scope.items = res.data.Items;
                    $scope.pageInfo = res.data.PageInfo;
                });
            }

            $scope.loadPage();

            $scope.changeStatus = function (id) {
                $host({
                    method: 'GET',
                    url: 'api/admin/seat-types/changed-status',
                    params: {
                        id: id
                    }
                })(function (res) {
                    if (res.data === false) {
                        alert('Bạn không thể đổi trạng thái bảng ghi');
                    } else {
                        if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                            $host({
                                method: 'PUT',
                                params: {
                                    id: id
                                },
                                url: 'api/admin/seat-types/status'
                            })(function (res) {
                                $scope.loadPage();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                }, function (res, exec1) {
                    exec1(res);
                });
            }
        })();
    }
]);