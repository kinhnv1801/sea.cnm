﻿'use strict';

angular.module('app-admin.sections.seat-type', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/seat-type/index', {
                templateUrl: '/Areas/Admin/App/sections/core/seat-type/index/seat-type.index.html',
                controller: 'SeatTypeIndex'
            }).when('/seat-type/create', {
                templateUrl: '/Areas/Admin/App/sections/core/seat-type/item/seat-type.item.html',
                controller: 'SeatTypeItem'
            }).when('/seat-type/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/seat-type/item/seat-type.item.html',
                controller: 'SeatTypeItem'
            }).when('/seat-type/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/seat-type/item/seat-type.item.html',
                controller: 'SeatTypeItem'
            });
    }
    ]);