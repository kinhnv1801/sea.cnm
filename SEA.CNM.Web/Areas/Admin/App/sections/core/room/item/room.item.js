﻿'use strict';

angular
    .module('app-admin.sections.room')
    .controller('RoomItem',
    [
        '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
        function ($scope, $http, ucInfo, $host, $routeParams, $location) {
            (function makeLayout() {
                ucInfo.UcName = 'Quản lý phòng';
                document.title = 'Quản lý phòng';

                // Cài đặt giao diện
                if (window.location.href.indexOf('create') !== -1) {
                    $scope.createPage = true;
                    $scope.action = 'Thêm phòng';
                    $scope.actionIcon = 'fa fa-plus';
                } else if (window.location.href.indexOf('edit') !== -1) {
                    $scope.editPage = true;
                    $scope.action = 'Sửa phòng';
                    $scope.actionIcon = 'fa fa-edit';
                } else if (window.location.href.indexOf('details') !== -1) {
                    $scope.detailsPage = true;
                    $scope.action = 'Xem chi tiết phòng';
                    $scope.actionIcon = 'fa fa-eye';
                }
            })();

            (function makeSrouce() {
                $host({
                    method: 'GET',
                    url: 'api/admin/seat-type-selects'
                })(function(res) {
                    $scope.seatTypeSrouce = {
                        Items: res.data,
                        Value: 'SeatTypeId',
                        Title: 'Name',
                        Placeholder: 'Chọn loại ghế'
                    };
                    $scope.seatConfig = {
                        Cols: [
                            {
                                Value: 'SeatTypeId',
                                Title: 'Loại ghế',
                                Type: 'Select',
                                Srouce: $scope.seatTypeSrouce,
                                OnChange: function() {
                                }
                            }, {
                                Value: 'Quantity',
                                Title: 'Số lượng',
                                Type: 'Number',
                                Placeholder: 'Nhập số lương ghế'
                            }
                        ]
                    }
                }, function(res) {});
            })();

            (function main() {
                if ($scope.createPage) {
                    $scope.item = {};

                    $scope.save = function () {
                        $host({
                            method: 'POST',
                            url: 'api/admin/rooms',
                            data: $scope.item
                        })(function (res) {
                            $location.path('/room/index');
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                } else if ($scope.editPage || $scope.detailsPage) {
                    var roomId = $routeParams.id;
                    $host({
                        method: 'GET',
                        url: 'api/admin/rooms',
                        params: {
                            id: roomId
                        }
                    })(function (res) {
                        $scope.item = res.data;
                    });

                    $scope.save = function () {
                        $host({
                            method: 'PUT',
                            url: 'api/admin/rooms',
                            params: {
                                id: roomId
                            },
                            data: $scope.item
                        })(function (res) {
                            $location.path('/room/details/' + roomId);
                            $location.replace();
                        }, function (res) {
                            $scope.msg = res.data.ModelState;
                        });
                    }

                    $scope.delete = function () {
                        if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                            $host({
                                method: 'DELETE',
                                url: 'api/admin/rooms',
                                params: {
                                    id: roomId
                                }
                            })(function (res) {
                                $location.path('/room/index');
                                $location.replace();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                };
            })();
        }
    ]);