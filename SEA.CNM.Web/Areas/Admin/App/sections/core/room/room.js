﻿'use strict';

angular
    .module('app-admin.sections.room', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
            $routeProvider
                .when('/room/index', {
                    templateUrl: '/Areas/Admin/App/sections/core/room/index/room.index.html',
                    controller: 'RoomIndex'
                }).when('/room/create', {
                    templateUrl: '/Areas/Admin/App/sections/core/room/item/room.item.html',
                    controller: 'RoomItem'
                }).when('/room/details/:id', {
                    templateUrl: '/Areas/Admin/App/sections/core/room/item/room.item.html',
                    controller: 'RoomItem'
                }).when('/room/edit/:id', {
                    templateUrl: '/Areas/Admin/App/sections/core/room/item/room.item.html',
                    controller: 'RoomItem'
                });
        }
    ]);