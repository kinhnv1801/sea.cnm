﻿'use strict';

angular.module('app-admin.sections.time').controller('TimeIndex',
[
    '$scope', '$host', 'UcInfo',
    function ($scope, $host, ucInfo) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý thời gian chiếu';
            document.title = 'Quản lý thời gian chiếu';
            $scope.action = 'Danh sách thời gian chiếu';
            $scope.actionIcon = 'fa fa-list';
        })();

        (function main() {
            $scope.filter = {
                IsDeleted: false,
                Page: 1,
                Take: 10
            }

            $scope.loadPage = function (page) {
                if (page == undefined) {
                    $scope.filter.Page = 1;
                } else {
                    $scope.filter.Page = page;
                }

                $host({
                    method: 'GET',
                    params: $scope.filter,
                    url: 'api/admin/times'
                })(function (res) {
                    $scope.items = res.data.Items;
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].BeginAt = new Date($scope.items[i].BeginAt);
                        $scope.items[i].BeginAt.setHours($scope.items[i].BeginAt.getHours() + 7);
                        $scope.items[i].EndAt = new Date($scope.items[i].EndAt);
                        $scope.items[i].EndAt.setHours($scope.items[i].EndAt.getHours() + 7);
                    }
                    $scope.pageInfo = res.data.PageInfo;
                });
            }

            $scope.loadPage();

            $scope.changeStatus = function (id) {
                $host({
                    method: 'GET',
                    url: 'api/admin/times/changed-status',
                    params: {
                        id: id
                    }
                })(function (res) {
                    if (res.data === false) {
                        alert('Bạn không thể đổi trạng thái bảng ghi');
                    } else {
                        if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                            $host({
                                method: 'PUT',
                                params: {
                                    id: id
                                },
                                url: 'api/admin/times/status'
                            })(function (res) {
                                $scope.loadPage();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                }, function (res, exec1) {
                    exec1(res);
                });
            }
        })();
    }
]);