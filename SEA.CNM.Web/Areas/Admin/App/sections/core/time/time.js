﻿'use strict';

angular.module('app-admin.sections.time', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/time/index', {
                templateUrl: '/Areas/Admin/App/sections/core/time/index/time.index.html',
                controller: 'TimeIndex'
            }).when('/time/create', {
                templateUrl: '/Areas/Admin/App/sections/core/time/item/time.item.html',
                controller: 'TimeItem'
            }).when('/time/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/time/item/time.item.html',
                controller: 'TimeItem'
            }).when('/time/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/time/item/time.item.html',
                controller: 'TimeItem'
            });
    }
    ]);