﻿angular.module('app-admin.sections.time').controller('TimeItem',
[
    '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
    function ($scope, $http, ucInfo, $host, $routeParams, $location) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý thời gian chiếu';
            document.title = 'Quản lý thời gian chiếu';

            if (window.location.href.indexOf('create') !== -1) {
                $scope.createPage = true;
                $scope.action = 'Thêm thời gian chiếu';
                $scope.actionIcon = 'fa fa-plus';
            } else if (window.location.href.indexOf('edit') !== -1) {
                $scope.editPage = true;
                $scope.action = 'Sửa thời gian chiếu';
                $scope.actionIcon = 'fa fa-edit';
            } else if (window.location.href.indexOf('details') !== -1) {
                $scope.detailsPage = true;
                $scope.action = 'Xem chi tiết thời gian chiếu';
                $scope.actionIcon = 'fa fa-eye';
            }
        })();

        (function main() {
            if ($scope.detailsPage || $scope.editPage) {
                var timeId = $routeParams.id;
                $host({
                    method: 'GET',
                    url: 'api/admin/times',
                    params: {
                        id: timeId
                    }
                })(function (res) {
                    $scope.item = res.data;
                    $scope.item.BeginAt = new Date($scope.item.BeginAt);
                    $scope.item.BeginAt.setHours($scope.item.BeginAt.getHours() + 7);
                    $scope.item.EndAt = new Date($scope.item.EndAt);
                    $scope.item.EndAt.setHours($scope.item.EndAt.getHours() + 7);
                }, function (res) {

                });

                $scope.save = function () {
                    $host({
                        method: 'PUT',
                        url: 'api/admin/times',
                        params: {
                            id: timeId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/time/details/' + timeId);
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }

                $scope.delete = function () {
                    if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                        $host({
                            method: 'DELETE',
                            url: 'api/admin/times',
                            params: {
                                id: timeId
                            }
                        })(function (res) {
                            $location.path('/time/index');
                            $location.replace();
                        }, function (res, exec1) {
                            exec1(res);
                        });
                    }
                }
            } else if ($scope.createPage) {
                $scope.item = {};

                $scope.save = function () {
                    $host({
                        method: 'POST',
                        url: 'api/admin/times',
                        params: {
                            id: timeId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/time/index');
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }
            }
        })();
    }
]);