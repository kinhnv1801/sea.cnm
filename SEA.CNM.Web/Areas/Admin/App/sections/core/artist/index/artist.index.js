﻿'use strict';

angular.module('app-admin.sections.artist').controller('ArtistIndex',
[
    '$scope', '$host', 'UcInfo',
    function ($scope, $host, ucInfo) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý nghệ sĩ';
            document.title = 'Quản lý nghệ sĩ';
            $scope.filter = {
                SearchText: '',
                IsDeleted: false,
                Page: 1,
                Take: 10
            };
            ucInfo.UcName = 'Quản lý nghệ sĩ';
            document.title = 'Quản lý nghệ sĩ';
        })();

        (function makeSrouce() {
            $scope.genderSrouce = {
                Items: [
                        { Value: true, Title: 'Nam' },
                        { Value: false, Title: 'Nữ' }
                ],
                Value: 'Value',
                Title: 'Title',
                Placeholder: 'Chọn giới tính'
            }
        })();

        (function main() {
            $scope.filter = {
                SearchText: '',
                IsDeleted: false,
                Page: 1,
                Take: 10
            };

            $scope.loadPage = function (page) {
                if (page == undefined) {
                    $scope.filter.Page = 1;
                } else {
                    $scope.filter.Page = page;
                }

                $host({
                    method: 'GET',
                    params: $scope.filter,
                    url: 'api/admin/artists'
                })(function (res) {
                    $scope.items = res.data.Items;
                    $scope.pageInfo = res.data.PageInfo;
                });
            }

            $scope.loadPage();
            $scope.changeStatus = function (id) {
                $host({
                    method: 'GET',
                    url: 'api/admin/artists/changed-status',
                    params: {
                        id: id
                    }
                })(function (res) {
                    if (res.data === false) {
                        alert('Bạn không thể đổi trạng thái bảng ghi');
                    } else {
                        if (confirm('Bạn có muốn đổi trạng thái bảng ghi không ?')) {
                            $host({
                                method: 'PUT',
                                params: {
                                    id: id
                                },
                                url: 'api/admin/artists/status'
                            })(function (res) {
                                $scope.loadPage();
                            }, function (res, exec1) {
                                exec1(res);
                            });
                        }
                    }
                }, function (res, exec1) {
                    exec1(res);
                });
            }
        })();
    }
]);