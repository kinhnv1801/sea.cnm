﻿'use strict';

angular.module('app-admin.sections.artist', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/artist/index', {
                templateUrl: '/Areas/Admin/App/sections/core/artist/index/artist.index.html',
                controller: 'ArtistIndex'
            }).when('/artist/create', {
                templateUrl: '/Areas/Admin/App/sections/core/artist/item/artist.item.html',
                controller: 'ArtistItem'
            }).when('/artist/details/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/artist/item/artist.item.html',
                controller: 'ArtistItem'
            }).when('/artist/edit/:id', {
                templateUrl: '/Areas/Admin/App/sections/core/artist/item/artist.item.html',
                controller: 'ArtistItem'
            });
    }
    ]);