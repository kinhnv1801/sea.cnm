﻿angular.module('app-admin.sections.artist').controller('ArtistItem',
[
    '$scope', '$host', '$routeParams','$location', 'UcInfo',
    function ($scope, $host, $routeParams, $location, ucInfo) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý nghệ sĩ';
            document.title = 'Quản lý nghệ sĩ';

            if (window.location.href.indexOf('create') !== -1) {
                $scope.createPage = true;
                $scope.action = 'Thêm nghệ sĩ';
                $scope.actionIcon = 'fa fa-plus';
            } else if (window.location.href.indexOf('edit') !== -1) {
                $scope.editPage = true;
                $scope.action = 'Sửa nghệ sĩ';
                $scope.actionIcon = 'fa fa-edit';
            } else if (window.location.href.indexOf('details') !== -1) {
                $scope.detailsPage = true;
                $scope.action = 'Xem chi tiết nghệ sĩ';
                $scope.actionIcon = 'fa fa-eye';
            }
        })();

        (function makeSrouce() {
            $scope.genderSrouce = {
                Items: [
                        { Value: true, Title: 'Nam' },
                        { Value: false, Title: 'Nữ' }
                ],
                Value: 'Value',
                Title: 'Title',
                Placeholder: 'Chọn giới tính'
            }
        })();
        
        (function main() {
            if ($scope.detailsPage || $scope.editPage) {
                var artistId = $routeParams.id;
                $host({
                    method: 'GET',
                    url: 'api/admin/artists',
                    params: {
                        id: artistId
                    }
                })(function(res) {
                    $scope.item = res.data;
                    $scope.item.DateOfBirth = new Date($scope.item.DateOfBirth);
                });

                $scope.save = function() {
                    $host({
                        method: 'PUT',
                        url: 'api/admin/artists',
                        params: {
                            id: artistId
                        },
                        data: $scope.item
                    })(function(res) {
                        $location.path('/artist/details/' + artistId);
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }

                $scope.delete = function () {
                    if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                        $host({
                            method: 'DELETE',
                            url: 'api/admin/artists',
                            params: {
                                id: artistId
                            }
                        })(function (res) {
                            $location.path('/artist/index');
                            $location.replace();
                        }, function (res, exec1) {
                            exec1(res);
                        });
                    }
                }
            } else if ($scope.createPage) {
                $scope.item = {};

                $scope.save = function () {
                    $host({
                        method: 'POST',
                        url: 'api/admin/artists',
                        params: {
                            id: artistId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/artist/index');
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }
            }
        })();
    }
]);