﻿angular.module('app-admin.sections.field').controller('FieldItem',
[
    '$scope', '$http', 'UcInfo', '$host', '$routeParams', '$location',
    function ($scope, $http, ucInfo, $host, $routeParams, $location) {
        (function makeLayout() {
            ucInfo.UcName = 'Quản lý lĩnh vực';
            document.title = 'Quản lý lĩnh vực';

            if (window.location.href.indexOf('create') !== -1) {
                $scope.createPage = true;
                $scope.action = 'Thêm lĩnh vực';
                $scope.actionIcon = 'fa fa-plus';
            } else if (window.location.href.indexOf('edit') !== -1) {
                $scope.editPage = true;
                $scope.action = 'Sửa lĩnh vực';
                $scope.actionIcon = 'fa fa-edit';
            } else if (window.location.href.indexOf('details') !== -1) {
                $scope.detailsPage = true;
                $scope.action = 'Xem chi tiết lĩnh vực';
                $scope.actionIcon = 'fa fa-eye';
            }
        })();

        (function makeSrouce() {
            $scope.fieldTypeSrouce = {
                Items: [
                        { value: 1, title: 'Thể loại' },
                        { value: 2, title: 'Nguồn gốc' }
                ],
                Value: 'value',
                Title: 'title',
                Placeholder: 'Chọn kiểu lĩnh vực'
            }
        })();

        (function main() {
            if ($scope.detailsPage || $scope.editPage) {
                var fieldId = $routeParams.id;
                $host({
                    method: 'GET',
                    url: 'api/admin/fields',
                    params: {
                        id: fieldId
                    }
                })(function (res) {
                    $scope.item = res.data;
                });

                $scope.save = function () {
                    $host({
                        method: 'PUT',
                        url: 'api/admin/fields',
                        params: {
                            id: fieldId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/field/details/' + fieldId);
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }

                $scope.delete = function () {
                    if (confirm('Bạn có muốn xóa hoàn toàn bảng ghi ?')) {
                        $host({
                            method: 'DELETE',
                            url: 'api/admin/fields',
                            params: {
                                id: fieldId
                            }
                        })(function (res) {
                            $location.path('/field/index');
                            $location.replace();
                        }, function (res, exec1) {
                            exec1(res);
                        });
                    }
                }
            } else if ($scope.createPage) {
                $scope.item = {};

                $scope.save = function () {
                    $host({
                        method: 'POST',
                        url: 'api/admin/fields',
                        params: {
                            id: fieldId
                        },
                        data: $scope.item
                    })(function (res) {
                        $location.path('/field/index');
                        $location.replace();
                    }, function (res) {
                        $scope.msg = res.data.ModelState;
                    });
                }
            }
        })();
    }
]);