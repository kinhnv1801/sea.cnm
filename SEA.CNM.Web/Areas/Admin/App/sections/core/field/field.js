﻿'use strict';

angular.module('app-admin.sections.field', ['ngRoute'])
    .config([
        '$routeProvider', function($routeProvider) {
            $routeProvider
                .when('/field/index', {
                    templateUrl: '/Areas/Admin/App/sections/core/field/index/field.index.html',
                    controller: 'FieldIndex'
                }).when('/field/create', {
                    templateUrl: '/Areas/Admin/App/sections/core/field/item/field.item.html',
                    controller: 'FieldItem'
                }).when('/field/details/:id', {
                    templateUrl: '/Areas/Admin/App/sections/core/field/item/field.item.html',
                    controller: 'FieldItem'
                }).when('/field/edit/:id', {
                    templateUrl: '/Areas/Admin/App/sections/core/field/item/field.item.html',
                    controller: 'FieldItem'
                });
        }
    ]);