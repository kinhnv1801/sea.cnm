﻿angular
    .module('app-admin.filters')
    .filter('getTitle', [
        function () {
            return function (value, srouce) {
                var selectItem = "Không xác định";
                if (srouce != undefined) {
                    if (!srouce.Multiple) {
                        if (value != undefined && value !== {}) {
                            if (srouce.Items == undefined ||
                                srouce.Items.length == undefined ||
                                srouce.Items.length === 0)
                                return selectItem;
                            return srouce.Items.filter(function(e) {
                                return e[srouce.Value] === value;
                            })[0][srouce.Title];
                        } else {
                            return selectItem;
                        }
                    } else {
                        if (value != undefined && value !== {} && value.length !== 0) {
                            return srouce.Items.filter(function(e) {
                                return value.indexOf(e[srouce.Value]) !== -1;
                            }).map(function(e) {
                                return e[srouce.Title];
                            }).join(', ');
                        } else {
                            return selectItem;
                        }
                    }
                } else {
                    return selectItem;
                }
            }
        }
    ]);