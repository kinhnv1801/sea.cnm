﻿using System.Data.Entity.Migrations;
using SEA.CNM.Domain.Migrations;

namespace SEA.CNM.Domain
{
    public class RenameKey : DbMigration
    {
        public override void Up()
        {
            
        }
    }

    public class EfConfig
    {
        public static void Initialize()
        {
            RunMigrations();
        }

        private static void RunMigrations()
        {
            var efMigrationSettings = new Configuration();
            var efMigrator = new DbMigrator(efMigrationSettings);
            efMigrator.Update();
        }
    }
}