﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Emun
{
    public enum ArtistType
    {
        Director,
        Actor
    }
}
