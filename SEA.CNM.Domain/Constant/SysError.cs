﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Constant
{
    public static class SysError
    {
        public const string SERVICE_ERROR = "System error";

        public const string NOT_FOUND = "Not found";

        public const string REQUIRED = "Thông tin này bắt buộc";
    }
}
