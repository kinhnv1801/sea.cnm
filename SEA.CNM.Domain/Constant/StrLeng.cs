﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Constant
{
    public static class StrLeng
    {
        public const int USER_ID = 128;

        public const int USER_NAME = 128;

        public const int PASSWORD = 128;

        public const int ROLE_ID = 128;

        public const int NAME = 64;

        public const int DESCRIPTION = 8192;

        public const int LINK = 256;

        public const int INTRODUCTION = 2048;

        public const int CODE = 128;

        public const int PHONE = 32;

        public const int EMAIL = 64;

        public const int ADDRESS = 2048;

    }
}
