﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.AccountEntities
{
    public class UserRole
    {
        [Key, Column(Order = 0)]
        [StringLength(StrLeng.USER_ID)]
        public string UserId { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(StrLeng.ROLE_ID)]
        public string RoleId { get; set; }
    }
}
