﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.AccountEntities
{
    public class User
    {
        [Key]
        [StringLength(StrLeng.USER_ID)]
        public string UserId { get; set; }

        [StringLength(StrLeng.USER_NAME)]
        public string UserName { get; set; }

        [StringLength(StrLeng.PASSWORD)]
        public string Password { get; set; }

        [StringLength(StrLeng.NAME)]
        public string FullName { get; set; }

        public bool? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(StrLeng.PHONE)]
        public string Phone { get; set; }

        [StringLength(StrLeng.EMAIL)]
        public string Email { get; set; }

        [StringLength(StrLeng.LINK)]
        public string Avatar { get; set; }

        [StringLength(StrLeng.ADDRESS)]
        public string Address { get; set; }

        public bool IsDeleted { get; set; }
    }
}
