﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.AccountEntities
{
    public class Role
    {
        [Key]
        [StringLength(StrLeng.ROLE_ID)]
        public string RoleId { get; set; }

        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        public bool RoleType { get; set; }

        [StringLength(StrLeng.DESCRIPTION)]
        public string Description { get; set; }

        public string Apis { get; set; }

        public bool IsDeleted { get; set; }
    }
}
