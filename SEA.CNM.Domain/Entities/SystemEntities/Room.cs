﻿using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.SystemEntities
{
    public class Room
    {
        [Key]
        public int RoomId { get; set; }

        [StringLength(StrLeng.CODE)]
        public string Code { get; set; }

        public float Large { get; set; }

        public int Capacity { get; set; }

        [StringLength(StrLeng.DESCRIPTION)]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }
    }
}
