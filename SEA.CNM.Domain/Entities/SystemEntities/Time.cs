﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.SystemEntities
{
    public class Time
    {
        public int TimeId { get; set; }

        public DateTime BeginAt { get; set; }

        public DateTime EndAt { get; set; }

        public bool IsDeleted { get; set; }
    }
}
