﻿using System;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.SystemEntities
{

    public class Artist
    {
        [Key]
        public int ArtistId { get; set; }

        [StringLength(StrLeng.NAME)]
        public string FullName { get; set; }

        public bool Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        [StringLength(StrLeng.LINK)]
        public string Avatar { get; set; }

        [StringLength(StrLeng.INTRODUCTION)]
        public string Introduction { get; set; }

        public bool IsDeleted { get; set; }
    }
}
