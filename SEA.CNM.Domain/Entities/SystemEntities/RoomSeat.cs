﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEA.CNM.Domain.Entities.SystemEntities
{
    public class RoomSeat
    {
        [Key, Column(Order = 0)]
        public int RoomId { get; set; }

        [Key, Column(Order = 1)]
        public int SeatTypeId { get; set; }

        public int Quantity { get; set; }
    }
}
