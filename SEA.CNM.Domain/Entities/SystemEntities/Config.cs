﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.SystemEntities
{
    public class Config
    {
        [Key]
        public int ConfigId { get; set; }

        public int ConfigType { get; set; }

        public string Content { get; set; }
    }
}
