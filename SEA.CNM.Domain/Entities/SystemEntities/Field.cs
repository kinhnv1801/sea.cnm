﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.SystemEntities
{
    public class Field
    {
        [Key]
        public int FieldId { get; set; }

        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        public int FieldType { get; set; }

        public bool IsDeleted { get; set; }
    }
}
