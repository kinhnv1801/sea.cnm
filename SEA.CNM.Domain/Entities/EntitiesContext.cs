using System.Data.Entity;
using SEA.CNM.Domain.Entities.AccountEntities;
using SEA.CNM.Domain.Entities.CinemaEntities;
using SEA.CNM.Domain.Entities.SystemEntities;

namespace SEA.CNM.Domain.Entities
{
    public partial class EntitiesContext : DbContext
    {
        public EntitiesContext()
            : base("name=EntitiesContext")
        {
        }
        //Account
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        //Cinema
        public DbSet<Film> Films { get; set; }
        public DbSet<FilmArtist> FilmArtists { get; set; }
        public DbSet<FilmField> FilmFields { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomSeat> RoomSeats { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        
        //Sale
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillTicket> BillTickets { get; set; }

        //System
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<SeatType> SeatTypes { get; set; }
        public DbSet<Time> Times { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
