﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class Schedule
    {
        [Key]
        public int ScheduleId { get; set; }

        public int FilmId { get; set; }

        public int RoomId { get; set; }

        public DateTime Date { get; set; }

        public int TimeId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
