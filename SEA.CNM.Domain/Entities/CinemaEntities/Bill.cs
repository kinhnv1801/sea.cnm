﻿using System;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class Bill
    {
        [Key]
        public int BillId { get; set; }

        [StringLength(StrLeng.CODE)]
        public string Code { get; set; }

        public string UserId { get; set; }

        public decimal Price { get; set; }

        public DateTime CreatedAt { get;set; }

        public bool IsDeleted { get; set; }
    }
}
