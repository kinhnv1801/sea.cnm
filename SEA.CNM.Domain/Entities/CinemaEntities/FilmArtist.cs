﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class FilmArtist
    {
        [Key, Column(Order = 0)]
        public int FilmId { get; set; }

        [Key, Column(Order = 1)]
        public int ArtistId { get; set; }

        [Key, Column(Order = 2)]
        public int ArtistType { get; set; }
    }
}
