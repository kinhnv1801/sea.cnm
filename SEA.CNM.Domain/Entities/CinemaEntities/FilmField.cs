﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class FilmField
    {
        [Key, Column(Order = 0)]
        public int FilmId { get; set; }

        [Key, Column(Order = 1)]
        public int FieldId { get; set; }
    }
}
