﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class BillTicket
    {
        [Key, Column(Order = 0)]
        public int? BillId { get; set; }

        [Key, Column(Order = 1)]
        public int? TicketId { get; set; }

        public int? Quantity { get; set; }
    }
}
