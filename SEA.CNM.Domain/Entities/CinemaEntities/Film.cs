﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class Film
    {
        [Key]
        public int FilmId { get; set; }

        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        [StringLength(StrLeng.LINK)]
        public string Avatar { get; set; }

        public int Hour { get; set; }

        public int Minute { get; set; }

        public DateTime CreatedAt { get; set; }

        [StringLength(StrLeng.DESCRIPTION)]
        public string Review { get; set; }

        [StringLength(StrLeng.LINK)]
        public string LinkReview { get; set; }

        public bool IsDeleted { get; set; }
    }
}
