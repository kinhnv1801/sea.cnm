﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Entities.CinemaEntities
{
    public class Ticket
    {
        [Key]
        public int TicketId { get; set; }

        public int? ScheduleId { get; set; }

        public int? SeatTypeId { get; set; }

        public decimal? Price { get; set; }

        public int? Quantity { get; set; }

        public DateTime? PublishedAt { get; set; }

        public bool IsDeleted { get; set; }
    }
}
