﻿using System.Collections.Generic;
using System.Linq;

namespace SEA.CNM.Domain.Entities.Extensions
{
    public static class QueryableExtensions
    {
        public static List<T> ToPaginatedList<T>(
            this IQueryable<T> query, int pageIndex, int pageSize)
        {
            return query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
