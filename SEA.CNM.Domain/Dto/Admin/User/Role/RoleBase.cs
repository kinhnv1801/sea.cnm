﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.User.Role
{
    public class RoleBase
    {
        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        public bool RoleType { get; set; }

        [StringLength(StrLeng.DESCRIPTION)]
        public string Description { get; set; }

        public List<String> Apis { get; set; }
    }
}
