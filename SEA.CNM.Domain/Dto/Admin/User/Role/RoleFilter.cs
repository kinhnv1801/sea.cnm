﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.User.Role
{
    public class RoleFilter : PageFilter
    {
        public string SearchText { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
