﻿using System.Collections.Generic;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.User.User
{
    public class UserFilter : PageFilter
    {
        public string SearchText { get; set; }

        public bool IsDeleted { get; set; }

        public List<string> RoleIds { get; set; }
    }
}
