﻿namespace SEA.CNM.Domain.Dto.Admin.User.User
{
    public class UserPost : UserBase
    {
        public string Password { get; set; }

        public string Confirm { get; set; }
    }
}
