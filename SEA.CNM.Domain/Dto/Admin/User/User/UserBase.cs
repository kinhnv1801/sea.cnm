﻿using System;
using System.Collections.Generic;

namespace SEA.CNM.Domain.Dto.Admin.User.User
{
    public class UserBase
    {
        public string UserName { get; set; }

        public string FullName { get; set; }

        public bool Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Avatar { get; set; }

        public string Address { get; set; }

        public List<string> RoleIds { get; set; } 
    }
}
