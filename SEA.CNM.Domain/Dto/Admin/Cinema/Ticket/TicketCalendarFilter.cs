﻿using System;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Ticket
{
    public class TicketCalendarFilter
    {
        public DateTime? BeginDate { get; set; }

        public int? RoomId { get; set; }
    }
}
