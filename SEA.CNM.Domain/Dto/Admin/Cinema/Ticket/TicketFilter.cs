﻿using System;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Ticket
{
    public class TicketFilter : PageFilter
    {
        public int? RoomId { get; set; }

        public int? TimeId { get; set; }

        public DateTime? Date { get; set; }

        public int? FilmId { get; set; }

        public DateTime? PublishedAt { get; set; }

        public int? SeatTypeId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
