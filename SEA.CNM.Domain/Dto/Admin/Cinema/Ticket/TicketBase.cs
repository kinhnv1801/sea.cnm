﻿using System;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Ticket
{
    public class TicketBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? FilmId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? RoomId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? Date { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? TimeId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? SeatTypeId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public decimal? Price { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? PublishedAt { get; set; }
    }
}
