﻿using System.Collections.Generic;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Film
{
    public class FilmFilter : PageFilter
    {
        public string SearchText { get; set; }

        public List<int> FieldIds { get; set; }

        public List<int> DirectorIds { get; set; }

        public List<int> ActorIds { get; set; }

        public bool IsDeleted { get; set; }
    }
}
