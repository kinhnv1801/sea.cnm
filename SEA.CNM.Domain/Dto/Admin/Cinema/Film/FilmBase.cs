﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Film
{
    public class FilmBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.LINK)]
        public string Avatar { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? Hour { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? Minute { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? CreatedAt { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.DESCRIPTION)]
        public string Review { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.LINK)]
        public string LinkReview { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public List<int> FieldIds { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public List<int> DirectorIds { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public List<int> ActorIds { get; set; }
    }
}
