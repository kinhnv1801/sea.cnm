﻿using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Bill
{
    public class BillTicketBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? TicketId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? Quantity { get; set; }
    }
}
