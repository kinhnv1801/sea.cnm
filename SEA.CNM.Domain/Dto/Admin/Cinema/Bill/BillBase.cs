﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;
using SEA.CNM.Domain.Entities.CinemaEntities;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Bill
{
    public class BillBase
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public List<BillTicketBase> Tickets { get; set; } 
    }
}
