﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Bill
{
    public class BillFilter : PageFilter
    {
        public string SearchText { get; set; }

        public List<int> TicketIds { get; set; }

        public bool IsDeleted { get; set; }
    }
}
