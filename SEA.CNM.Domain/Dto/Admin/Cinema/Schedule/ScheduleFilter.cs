﻿using System;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Schedule
{
    public class ScheduleFilter : PageFilter
    {
        public int? RoomId { get; set; }

        public int? TimeId { get; set; }

        public DateTime? Date { get; set; }

        public int? FilmId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
