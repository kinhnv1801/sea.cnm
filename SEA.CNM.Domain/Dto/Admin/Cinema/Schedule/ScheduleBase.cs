﻿using System;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Schedule
{
    public class ScheduleBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? FilmId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? RoomId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? Date { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? TimeId { get; set; }
    }
}
