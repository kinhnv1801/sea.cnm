﻿using System;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.Cinema.Schedule
{
    public class ScheduleCalendarFilter
    {
        public DateTime? BeginDate { get; set; }

        public int? RoomId { get; set; }
    }
}
