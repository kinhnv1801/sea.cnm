﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.System.SeatType
{
    public class SeatTypeFilter : PageFilter
    {
        public string SearchText { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
