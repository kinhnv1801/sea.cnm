﻿using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.System.SeatType
{
    public class SeatTypeBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }
    }
}
