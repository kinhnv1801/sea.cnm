﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.System.Room
{
    public class RoomFilter : PageFilter
    {
        public string SearchText { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
