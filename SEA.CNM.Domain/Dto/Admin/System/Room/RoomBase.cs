﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.System.Room
{
    public class RoomPostSeat
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        public int SeatTypeId { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int Quantity { get; set; }
    }

    public class RoomBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.CODE)]
        public string Code { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public float? Large { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? Capacity { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.DESCRIPTION)]
        public string Description { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public List<RoomPostSeat> Seats { get; set; }
    }
}
