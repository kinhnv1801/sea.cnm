﻿using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.System.Field
{
    public class FieldBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.NAME)]
        public string Name { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public int? FieldType { get; set; }
    }
}
