﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.System.Field
{
    public class FieldFilter : PageFilter
    {
        public string SearchText { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
