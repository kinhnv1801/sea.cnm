﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.System.Artist
{
    public class ArtistFilter : PageFilter
    {
        public string SearchText { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
