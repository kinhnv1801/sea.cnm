﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.System.Artist
{
    public class ArtistBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.NAME)]
        public string FullName { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public bool? Gender { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? DateOfBirth { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.LINK)]
        public string Avatar { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        [StringLength(StrLeng.INTRODUCTION)]
        public string Introduction { get; set; }
    }
}
