﻿using System;
using System.ComponentModel.DataAnnotations;
using SEA.CNM.Domain.Constant;

namespace SEA.CNM.Domain.Dto.Admin.System.Time
{
    public class TimeBase
    {
        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? BeginAt { get; set; }

        [Required(ErrorMessage = SysError.REQUIRED)]
        public DateTime? EndAt { get; set; }
    }
}
