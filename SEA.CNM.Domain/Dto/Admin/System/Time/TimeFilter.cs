﻿using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Admin.System.Time
{
    public class TimeFilter : PageFilter
    {
        public bool IsDeleted { get; set; }
    }
}
