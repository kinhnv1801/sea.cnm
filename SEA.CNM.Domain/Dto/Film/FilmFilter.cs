﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Film
{
    public class FilmFilter : PageFilter
    {
        public string SearchText { get; set; }

        public List<int> TimeIds { get; set; }

        public List<int> FieldIds { get; set; }

        public List<int> DirectorIds { get; set; }

        public List<int> ActorIds { get; set; }

        public DateTime? ScheduleDate { get; set; }
    }
}
