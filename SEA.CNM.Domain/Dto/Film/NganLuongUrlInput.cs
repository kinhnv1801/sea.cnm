﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Dto.Film
{
    public class NganLuongUrlInput
    {
        public List<TicketInfo> Tickets { get; set; }

        public string ReturnUrl { get; set; }
    }
}
