﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Dto.Film
{
    public class TicketInfo
    {
        public int TicketId { get; set; }

        public int Quantity { get; set; }
    }
}
