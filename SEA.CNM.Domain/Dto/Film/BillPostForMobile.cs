﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEA.CNM.Domain.Dto.NganLuong;

namespace SEA.CNM.Domain.Dto.Film
{
    public class BillPostForMobile
    {
        public string Tickets { get; set; }

        public decimal Price { get; set; }
    }
}
