﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Dto.Common
{
    public class PageInfo
    {
        public PageInfo(int pageIndex, int pageSize,
            int totalCount)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPageCount = 
                (int)Math.Ceiling(totalCount / (double)pageSize);
        }

        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public int TotalPageCount { get; private set; }
        public int FirstDisplayPage
        {
            get { return PageIndex - 2 > 0 ? PageIndex - 2 : 1; }
        }
        public int LastDisplayPage
        {
            get { return PageIndex + 2 < TotalPageCount ? PageIndex + 2 : TotalPageCount; }
        }
        public int NextPage
        {
            get { return PageIndex + 1 < TotalPageCount ? PageIndex + 1 : PageIndex; }
        }
        public int BeforePage
        {
            get { return PageIndex - 1 > 0 ? PageIndex - 1 : PageIndex; }
        }
    }
}
