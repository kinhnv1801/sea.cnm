﻿namespace SEA.CNM.Domain.Dto.Common
{
    public class PageFilter
    {
        public int Page { get; set; }

        public int Take { get; set; }
    }
}
