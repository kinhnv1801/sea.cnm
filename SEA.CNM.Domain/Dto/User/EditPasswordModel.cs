﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA.CNM.Domain.Dto.User
{
    public class EditPasswordModel
    {
        public string OldPassword { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
