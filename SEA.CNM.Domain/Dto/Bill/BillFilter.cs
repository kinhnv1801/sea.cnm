﻿using System;
using System.Collections.Generic;
using SEA.CNM.Domain.Dto.Common;

namespace SEA.CNM.Domain.Dto.Bill
{
    public class BillFilter : PageFilter
    {
        public List<int> FilmIds { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
