﻿namespace SEA.CNM.Domain.Dto.NganLuong
{
    public class VerifyPaymentInput
    {
        public string TransactionInfo { get; set; }

        public string OrderCode { get; set; }

        public string Price { get; set; }

        public string PaymentId { get; set; }

        public string PaymentType { get; set; }

        public string ErrorText { get; set; }

        public string SecureCode { get; set; }
    }
}
