﻿namespace SEA.CNM.Domain.Dto.NganLuong
{
    public class BuildCheckoutUrlInput
    {
        public string ReturnUrl { get; set; }

        public string TransactionInfo { get; set; }

        public string OrderCode { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public float Quantity { get; set; }

        public float Tax { get; set; }

        public float Discount { get; set; }

        public float FeeCal { get; set; }

        public float FeeShipping { get; set; }

        public string OrderDescription { get; set; }

        public string BuyerInfo { get; set; }

        public string AffiliateCode { get; set; }
    }
}
