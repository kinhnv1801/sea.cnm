/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {  
    AppRegistry,
    Text,
    View,
    Button,
    TextInput
} from 'react-native';

import App from './app/App'

AppRegistry.registerComponent('AndApp', () => App);
