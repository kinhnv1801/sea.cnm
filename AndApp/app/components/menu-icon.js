import React, {Component} from 'react';
import {
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

export default class MenuIcon extends Component {
    constructor(props){
        super(props);
    }

    render(){
        let icon = 'http://www.iconarchive.com/download/i90447/icons8/windows-8/Data-List.ico'

        return (
            <TouchableOpacity onPress={ ()=> { this.props.navigation.navigate('DrawerOpen') }}> 
                <Image 
                    style={{ marginLeft: 20, width: 25, height: 25 }} 
                    source={require("./../../images/list-icon.png")} />
            </TouchableOpacity>
        )
    }
}