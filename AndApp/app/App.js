
import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  DrawerItems
} from 'react-native';

import { 
    StackNavigator,
    DrawerNavigator
} from 'react-navigation';
import ListOfFilm from './screens/Film/ListOfFilm';
import DetailsOfFilm from './screens/Film/DetailsOfFilm';
import ListOfBill from './screens/Bill/ListOfBill';
import DetailsOfBill from './screens/Bill/DetailsOfBill';
import DetailsOfProfile from './screens/Profile/DetailsOfProfile';
import EditProfile from './screens/Profile/EditProfile';
import EditPassword from './screens/Profile/EditPassword';
import Login from './screens/Login';
import Register from './screens/Register';

export default DrawerNavigator ({
    Film: { 
        screen: StackNavigator({
            ListOfFilm: { 
                screen: ListOfFilm
            },
            DetailsOfFilm: { 
                screen : DetailsOfFilm
            }
        })
    },
    Bill: { 
        screen: StackNavigator({
            ListOfFilm: { 
                screen: ListOfBill
            },
            DetailsOfFilm: { 
                screen : DetailsOfBill
            }
        })
    }, 
    Profile: {
        screen: StackNavigator({
            DetailsOfProfile: { 
                screen: DetailsOfProfile
            },
            EditProfile: { 
                screen : EditProfile
            },
            EditPassword: {
                screen: EditPassword
            }
        })
    }, 
    Register: {
        screen: StackNavigator({
            Register: {
                screen: Register
            }                
        })
    }, 
    Login: {
        screen: StackNavigator({
            Login: {
                screen: Login
            }
        })
    }
});
