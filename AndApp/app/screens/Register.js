import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Button
} from 'react-native';
import MenuIcon from './../components/menu-icon'

export default class Register extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title: 'Đăng ký',
        headerLeft: <MenuIcon navigation={ navigation } />
    });

    render(){
        return (
            <View style={{flex: 1}}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 20 }}>Đăng ký tài khoản</Text>
                </View>
                <View style={{flex: 5, paddingLeft: 40, paddingRight: 40 }}>
                    <TextInput placeholder="Tên tài khoản" />
                    <TextInput placeholder="Mật khẩu" />
                    <TextInput placeholder="Xác nhân mật khẩu" />
                    <TextInput placeholder="Tên đầy đủ" />
                    <TextInput placeholder="Số điện thoại" />
                    <TextInput placeholder="Địa chỉ Email" />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Button onPress={()=>{}} style={{ width: 100 }} title="Đăng ký"/>
                </View>
            </View>
        )
    }
}