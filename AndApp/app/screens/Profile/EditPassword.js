import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import { TabNavigator } from 'react-navigation';

export default class EditProfile extends Component {
    constructor(props){
        super(props);
    }
    static navigationOptions = {
        title: 'Sửa mật khẩu',
    };

    render(){
        return (
            <View style={{flex: 1}}>
                <View style={{ flex: 1 }}>
                </View>
                <View style={{flex: 1, paddingLeft: 40, paddingRight: 40 }}>
                    <Text style={{ fontSize: 15 }}>Tài khoản: kinhnv1801@gmail.com</Text>
                    <TextInput placeholder="Mật khẩu cũ" />
                    <TextInput placeholder="Mật khẩu mới" />
                    <TextInput placeholder="Xác nhân mật khẩu" />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Button onPress={()=>{}} style={{ width: 100 }} title="Lưu"/>
                </View>
            </View>
        )
    }
}