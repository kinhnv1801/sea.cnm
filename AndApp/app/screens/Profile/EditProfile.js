import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Button,
  Image,
  Picker,
  ScrollView
} from 'react-native';

import { TabNavigator } from 'react-navigation';

export default class EditProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            gender: true
        }
    }
    static navigationOptions = {
        title: 'Sửa tài khoản'
    };

    render(){
        return (
            <ScrollView>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                        <Image 
                            style={{ height: 120, width: 120}}
                            source={{ uri: 'https://cdn3.iconfinder.com/data/icons/user-with-laptop/100/user-laptop-512.png' }}/>
                    </View>
                    <View style={{flex: 5, paddingLeft: 40, paddingRight: 40 }}>
                        <Text style={{ fontSize: 15, marginBottom: 8 }}>Tài khoản: kinhnv1801@gmail.com</Text>
                        <TextInput placeholder="Tên đầy đủ" />
                        <TextInput placeholder="Số điện thoại" />
                        <TextInput placeholder="Địa chỉ Email" />
                        <Picker
                            selectedValue={this.state.gender}
                            onValueChange={(itemValue, itemIndex) => this.setState({gender: itemValue})}>
                            <Picker.Item label="Nam" value="true" />
                            <Picker.Item label="Nữ" value="false" />
                        </Picker>
                        <TextInput placeholder="Ngày sinh" />
                        <TextInput placeholder="Đia chỉ" />
                        <TextInput placeholder="Số điện thoại" />
                        <TextInput placeholder="Địa chỉ Email" />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Button onPress={() => this.props.navigation.navigate('EditProfile')} style={{ width: 100 }} title="Lưu"/>                    
                    </View>
                </View>
            </ScrollView>
            
        )
    }
}