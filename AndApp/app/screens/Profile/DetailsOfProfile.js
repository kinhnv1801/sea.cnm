import React, { Component } from 'react';
import {
  Text,
  View,
  Button,
  Image
} from 'react-native';
import MenuIcon from './../../components/menu-icon'

export default class DetailsOfProfile extends Component {
    constructor(props){
        super(props);
    }
    
    static navigationOptions = ({navigation}) => ({
        title: 'Thông tin tài khoản',
        headerLeft: <MenuIcon navigation={ navigation } />
    });

    render(){
        let Avatar = 'https://cdn3.iconfinder.com/data/icons/user-with-laptop/100/user-laptop-512.png'

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                    <Image 
                        style={{ height: 120, width: 120}}
                        source={{ uri: 'https://cdn3.iconfinder.com/data/icons/user-with-laptop/100/user-laptop-512.png' }}/>
                </View>
                <View style={{flex: 3, paddingLeft: 40, paddingRight: 40 }}>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Tài khoản: kinhnv1801@gmail.com</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Tên: Nguyễn Văn Kính</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Giới tính: Nam</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Ngày sinh: 18/01/1996</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Địa chỉ: Số 4 - ngắch 6/7 - Phú Lãm - Hà Đông - Hà Nội</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Số điện thoại: 0934517691</Text>
                    <Text style={{ fontSize: 15, marginBottom: 8 }}>Email: kinhnv1801@gmail.com</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Button onPress={() => this.props.navigation.navigate('EditProfile')} style={{ width: 100 }} title="Sửa thông tin"/>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Button onPress={ () => this.props.navigation.navigate('EditPassword') } style={{ width: 100 }} title="Sửa mật khẩu"/>                    
                    </View>
                </View>
            </View>
        )
    }
}