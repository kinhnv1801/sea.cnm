import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Button
} from 'react-native';
import MenuIcon from './../components/menu-icon'

export default class Login extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title: 'Đăng nhập',
        headerLeft: <MenuIcon navigation={ navigation } />
    });

    render(){
        return (
            <View style={{flex: 1}}>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 30 }}>Cinema XXII</Text>

                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 20 }}>Đăng nhập</Text>
                    </View>
                </View>
                <View style={{flex: 1, paddingLeft: 40, paddingRight: 40 }}>
                    <TextInput placeholder="Tài khoản" />
                    <TextInput placeholder="Mật khẩu" />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Button onPress={()=>{}} style={{ width: 100 }} title="Đăng nhập"/>
                </View>
            </View>
        )
    }
}